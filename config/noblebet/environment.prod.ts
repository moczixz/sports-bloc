export const environment = {
  production: true,
  api2Enabled: false,
  sportsBookApi1: 'https://app.lvbet.pl/_api/v1/offer/',
  sportsBookWs1: 'wss://ws.lvbet.pl/_v3/ws/update/',
  sportsBookApi2: 'https://offer.noblebet.pl/client-api/v1/',
  sportsBookWs2: 'wss://offer-ws.noblebet.pl/_v3/ws/update/'
};