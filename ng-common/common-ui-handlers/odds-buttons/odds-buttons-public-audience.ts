import { tap } from 'rxjs/operators';
import { MonoTypeOperatorFunction } from 'rxjs';
import { Market } from '@logic/shared/models/match-offer/market';


export class OddsButtonsPublicAudience {

  public hasSelections: boolean;

  public tapMarket(): MonoTypeOperatorFunction<Market> {
    return tap((market: Market) => {
      this.hasSelections = market && market.selections && market.selections.length > 0;
    });
  }
}
