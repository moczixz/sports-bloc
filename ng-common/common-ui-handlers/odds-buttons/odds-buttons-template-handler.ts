import { tap } from 'rxjs/operators';
import { MonoTypeOperatorFunction } from 'rxjs';
import { PrimaryColumnConfig } from '@logic/shared/models/match-offer/primary-column-config';
import { Market } from '@logic/shared/models/match-offer/market';

export class OddsButtonsTemplateHandler {

  private primaryColumnConfig: PrimaryColumnConfig;
  private primaryColumns: string[];

  public setPrimaryColumnConfig(config: PrimaryColumnConfig): void {
    this.primaryColumnConfig = config;
    this.primaryColumns = config ? config.columns : [];
  }

  public getSelectionsLength(market: Market): number {
    
    if (market && this.primaryColumnConfig && this.primaryColumnConfig.hasLine) {
      return market.selections.length + 1;
    }
    return market ? market.selections.length : 0;
  }

  public getPrimaryColumnAtIndex(selectionIndex: number): string {
    const addIndex: number = this.primaryColumnConfig && this.primaryColumnConfig.hasLine ? 1 : 0;
    return this.primaryColumns[selectionIndex + addIndex];
  }

  public tapAndUpdateColumns(): MonoTypeOperatorFunction<string[]> {
    return tap((columns: string[]) => this.primaryColumns = columns)
  }
}
