import { Injectable, ElementRef, EventEmitter } from '@angular/core';
import { SportsGroup } from '@logic/shared/models/match-offer/sports-group';

@Injectable()
export class DisciplinesSelectUiHandler {
  private topTenRef: ElementRef;
  public activeDisciplineChange: EventEmitter<SportsGroup> = new EventEmitter<SportsGroup>();

  public disciplines: SportsGroup[];
  public activeDisciplineId: number;
  public activeDiscipline: SportsGroup;

  public setActiveDisciplineChange(emitter: EventEmitter<SportsGroup>): void {
    this.activeDisciplineChange = emitter;
  }

  public setTopTenRef(element: ElementRef): void {
    this.topTenRef = element;
  }

  public setActiveDisciplineId(discipline: SportsGroup): void {
    if (discipline) {
      this.activeDisciplineId = discipline.id;
      this.activeDiscipline = discipline;
    }
  }

  public emitActiveDiscipline(disciplineId: number): void {
    const discipline: SportsGroup = this.disciplines.find((discipline: SportsGroup) => discipline.id === disciplineId);
    this.activeDiscipline = discipline;
    this.activeDisciplineChange.emit(discipline);
  }

  public get disciplinesWithTop(): SportsGroup[] {
    if (this.disciplines[0].id === -1 && this.topTenRef) {
      return [
        {
          id: -1,
          name: this.topTenRef.nativeElement.textContent
        } as SportsGroup,
        ...this.disciplines.filter((discipline: SportsGroup) => discipline.id !== -1)
      ];
    }
    return this.disciplines;
  }

}
