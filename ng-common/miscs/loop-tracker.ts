import { SportsGroup } from "@logic/shared/models/match-offer/sports-group";
import { Match } from "@logic/shared/models/match-offer/match";

export class LoopTracker {
  public trackByMatchId(index: number, match: Match): string | number {
    return match.id;
  }

  public trackByIndex(index: number): number {
    return index;
  }

  public trackByDisciplineId(index: number, discipline: SportsGroup): number {
    return discipline.id;
  }

  public trackByDisciplineIndex(index: number, discipline: SportsGroup): number {
    return index;
  }

  public trackBySportsGroupId(index: number, sportsGroup: SportsGroup): number {
    return sportsGroup.id;
  }
}
