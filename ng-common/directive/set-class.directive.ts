import { Directive, Input, ElementRef } from '@angular/core';

/*
  why we need this if we have ngClass:
  consider this case:
        <div 
        [setClass]="{
          'col-d-2 col-md-3 col-sd-2 col-t-3 col-st-6 col-sm-12': colPrimaryColumnConfigs.length === 3 && i === 0,
          'col-d-2 col-md-3 col-sd-2 col-t-3 col-st-6 col-sm-hidden': colPrimaryColumnConfigs.length === 3 && i === 1,
          'col-d-2 col-md-hidden col-sd-show col-t-3 col-st-hidden': colPrimaryColumnConfigs.length === 3 && i === 2,
          'col-d-6 col-t-9 col-st-12': colPrimaryColumnConfigs.length === 1
        }"></div>
  in this example, if the first condition pass, the ngClass unfortunatelly set only class "col-sm-12" and thats the problem
*/

interface SetClassInput {
  [classString: string]: boolean;
}

@Directive({selector: '[setClass]'})
export class SetClassDirective {

  @Input() set setClass(classObject: SetClassInput) {
    if (classObject) {
      this.setClasses(classObject);
    }
  }

  constructor(private elementRef: ElementRef) {
  }

  /*
  we look for first classes which pass the condition and set it up
  */
  private setClasses(classObject: SetClassInput): void {
    this.removeAllListedClasses(classObject);
    const classForTrueCondition: string = Object.keys(classObject).find((key: string) => classObject[key] === true);
    if (classForTrueCondition) {
      classForTrueCondition.split(' ').forEach((className: string) =>
      this.elementRef.nativeElement.classList.add(className)
      );
    }
  }
  /*
  we remove all class which we set in input object, 
  this can be improve to remove only classes which didnt pass the condition
  */
  private removeAllListedClasses(classObject: SetClassInput): void {
    const allClasses: string[] = Object.keys(classObject)
      .reduce((accumulator: string[], classString: string) => [...accumulator, ...classString.split(' ')], []);

    new Set(allClasses).forEach((className: string) => {
      this.elementRef.nativeElement.classList.remove(className);
    });
  }

}
