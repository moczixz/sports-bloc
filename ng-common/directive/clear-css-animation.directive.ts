import { Directive, ElementRef, AfterViewInit, Input, OnDestroy } from '@angular/core';
import { fromEvent, Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';

@Directive({
  selector: '[clearCssAnimation]'
})
export class ClearCssAnimationDirective implements AfterViewInit, OnDestroy {

  @Input() clearCssAnimation: string | string[];
  private onDestroy$: Subject<void> = new Subject<void>();

  constructor(private el: ElementRef) {
 }

 public ngAfterViewInit(): void {
   fromEvent(this.el.nativeElement, 'animationend')
    .pipe(
      filter(() => Boolean(this.clearCssAnimation)),
      takeUntil(this.onDestroy$)
    )
    .subscribe(() => {
      if (Array.isArray(this.clearCssAnimation)) {
        this.clearCssAnimation.forEach((cssClass: string) =>
          this.el.nativeElement.classList.remove(cssClass)
        );
      }
      else {
        this.el.nativeElement.classList.remove(this.clearCssAnimation);
      }
    });
 }

 public ngOnDestroy(): void {
   this.onDestroy$.next(null);
   this.onDestroy$.complete();
 }

}
