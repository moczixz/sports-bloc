import { NgModule } from "@angular/core";
import { SetClassDirective } from "./set-class.directive";
import { ClearCssAnimationDirective } from "./clear-css-animation.directive";

@NgModule({
  declarations: [
    SetClassDirective,
    ClearCssAnimationDirective
  ],
  exports: [
    SetClassDirective,
    ClearCssAnimationDirective
  ]
})
export class CommonDirectivesModule {
  
}