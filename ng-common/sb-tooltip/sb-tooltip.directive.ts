import { Directive, Input, AfterViewInit, ElementRef, OnDestroy, Renderer2 } from '@angular/core';
import { fromEvent, timer, Subject } from 'rxjs';
import { takeUntil, switchMapTo, filter } from 'rxjs/operators';

@Directive({
  selector: '[sbTooltip]'
})
export class SbTooltipDirective implements AfterViewInit, OnDestroy {
  private readonly INTENTIONAL_MOUSE_OVER_DELAY: number = 100;
  @Input() sbTooltip: HTMLElement;
  @Input() tooltipActiveClass: string = 'is-active';

  private isActive: boolean = false;

  private onDestroy$: Subject<void> = new Subject<void>();

  constructor(private elementRef: ElementRef, private renderer: Renderer2) {
  }

  public ngAfterViewInit(): void {
    fromEvent(this.elementRef.nativeElement, 'mouseover')
      .pipe(
        switchMapTo(
          timer(this.INTENTIONAL_MOUSE_OVER_DELAY)
            .pipe(
              takeUntil(fromEvent(this.elementRef.nativeElement, 'mouseout'))
            )
        ),
        takeUntil(this.onDestroy$)
      )
    .subscribe(() => {
      this.renderer.addClass(this.sbTooltip, this.tooltipActiveClass);
      this.isActive = true;
    });

    fromEvent(this.elementRef.nativeElement, 'mouseout')
      .pipe(
        filter(() => this.isActive === true),
        takeUntil(this.onDestroy$)
      )
      .subscribe(() => {
        this.renderer.removeClass(this.sbTooltip, this.tooltipActiveClass);
        this.isActive = false;
      });
  }

  public ngOnDestroy(): void {
    this.onDestroy$.next(null);
    this.onDestroy$.complete();
  }

}
