import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SbTooltipDirective } from './sb-tooltip.directive';

@NgModule({
  declarations: [SbTooltipDirective],
  imports: [
    CommonModule
  ],
  exports: [SbTooltipDirective]
})
export class SbTooltipModule {

}
