import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SbSelectComponent } from './sb-select.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    SbSelectComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    SbSelectComponent
  ]
})
export class SbControlsModule {}
