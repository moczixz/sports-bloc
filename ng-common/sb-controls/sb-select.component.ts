import { Component, forwardRef, ElementRef, Input, Output, EventEmitter, ChangeDetectorRef, ChangeDetectionStrategy, OnInit, OnDestroy } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { Subject, fromEvent } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'sb-select',
  templateUrl: 'sb-select.component.html',
  providers: [
    { 
      provide: NG_VALUE_ACCESSOR,
      // tslint:disable-next-line:no-forward-ref
      useExisting: forwardRef(() => SbSelectComponent),
      multi: true
    }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SbSelectComponent implements OnInit, OnDestroy {

  public isActive: boolean;
  public selectedIndex: number = -1;
  public selectedValue: any;
  public selectedOption: any;

  @Input() options: any[];
  @Input() modelPropertyKey: string = 'key';
  @Input() selectedOptionDisplayKey: string = 'value';
  @Input() placeholder: string;
  @Input() singleOptionAsPlaceholder: boolean = false;
  @Input() onlyIconMode: boolean = false;
  @Input() externalDisplayValue: boolean = false;
  @Input() disabled: boolean = false;
  @Input() assetsSrc: string = null;
  // tslint:disable-next-line:no-output-named-after-standard-event
  @Output() change: EventEmitter<any> = new EventEmitter<any>();

  private onDestroy$: Subject<void> = new Subject<void>();

  public get displayValue(): string {
    if (this.selectedOptionDisplayKey) {
      return this.options[this.selectedIndex] ? this.options[this.selectedIndex][this.selectedOptionDisplayKey] : '';
    }
    return this.options[this.selectedIndex];
  }

  constructor(protected elementRef: ElementRef, private changeDetector: ChangeDetectorRef) {
  }

  public writeValue(value: any, findIndex: boolean = true): void {
    if (value) {
      this.selectedValue = value;
      if (findIndex) {
        this.selectedIndex = this.options.findIndex((option: any) => option[this.modelPropertyKey] === value);
        this.selectedOption = this.options[this.selectedIndex];
        this.change.emit(this.selectedValue);
      }
      this.triggerDetector(value);
    }
  }

  public clearSelect(): void {
    this.selectedIndex = -1;
    this.selectedValue = undefined;
    this.selectedOption = undefined;
    this.propagateChange(this.selectedValue);
    this.change.emit(this.selectedValue);
  }

  public toggleSelect(): void {
    if (!this.disabled && (!this.singleOptionAsPlaceholder || this.options.length > 1)) {
      this.isActive = !this.isActive;
    }
  }

  public ngOnInit(): void {
    // DO NOT rewrite it to HostListener, because its trigger change detector
    fromEvent(document, 'click')
      .pipe(
        filter(() => this.isActive === true),
        takeUntil(this.onDestroy$)
      )
      .subscribe((event: MouseEvent) => {
        const targetElement: HTMLElement = event.target as HTMLElement;
        if (targetElement && !this.elementRef.nativeElement.contains(targetElement)) {
          this.touchAndClearSearch();
          this.isActive = false;
          this.changeDetector.markForCheck();
        }
      });
  }

  public selectOption(selectedOptionValue: string | number, index: number): void {
    this.selectedIndex = index;
    this.selectedOption = this.options[this.selectedIndex];
    this.writeValue(selectedOptionValue, false);
    this.propagateChange(this.selectedValue);
    this.change.emit(this.selectedValue);
  }

  private touchAndClearSearch(): void {
    if (this.isActive) {
      this.propagateTouch(null);
    } 
  }

  private triggerDetector(value: any): void {
    // if you input model as primitive in onpush component, it wont trigger changes
    if (value !== Object(value)) {
      this.changeDetector.markForCheck();
    }
  }

  public registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  public registerOnTouched(fn: any): void {
    this.propagateTouch = fn;
  }
  
  public setDisabledState?(isDisabled: boolean): void {

  }

  public propagateChange = (_: any) => {};
  public propagateTouch = (_: any) => {};

  public ngOnDestroy(): void {
    this.onDestroy$.next(null);
    this.onDestroy$.complete();
  }
}