import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { TopLiveModule } from './src/top-live.module';
import { registerEnvironment } from '@logic/bloc-logic';
import { environment } from '../../../../config/noblebet/environment';

registerEnvironment(environment);
platformBrowserDynamic().bootstrapModule(TopLiveModule)
  .catch(err => console.error(err));
