import {enableProdMode} from '@angular/core';
import {platformBrowser} from '@angular/platform-browser';
import { TopLiveModule } from './src/top-live.module';
import { registerEnvironment } from '@logic/bloc-logic';
import { environment } from '@config/noblebet/environment.prod';

registerEnvironment(environment);
enableProdMode();
platformBrowser().bootstrapModule(TopLiveModule);
