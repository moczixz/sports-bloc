import {enableProdMode} from '@angular/core';
import {platformBrowser} from '@angular/platform-browser';
// @ts-ignore
import {TopLiveModuleNgFactory} from './src/top-live.module.ngfactory';
import { registerEnvironment } from '@logic/bloc-logic';
import { environment } from '@config/noblebet/environment.prod';

registerEnvironment(environment);
enableProdMode();
platformBrowser().bootstrapModuleFactory(TopLiveModuleNgFactory);
