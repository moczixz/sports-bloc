import { Component, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { TopLiveBloc } from '@logic/bloc/top-live/top-live.bloc';
import { getTopLiveBloc } from '@logic/bloc-logic';
import { Observable, Subject } from 'rxjs';
import { ContainerToken } from '@logic/match-offer/enums/container-token.enum';
import { SportsGroup } from '@logic/shared/models/match-offer/sports-group';
import { Match } from '@logic/shared/models/match-offer/match';
import { Market } from '@logic/shared/models/match-offer/market';
import { ObjectAsId } from '@logic/shared/interfaces/object-as-id.interface';
import { PrimaryColumnConfig } from '@logic/shared/models/match-offer/primary-column-config';
import { MarketsBelongsToMatch } from '@logic/match-offer/matches-data-selectors';
import { LoopTracker } from '@ngCommon/miscs/loop-tracker';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'top-live',
  templateUrl: './top-live.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TopLiveComponent implements OnDestroy {
  private topLiveBloc: TopLiveBloc = getTopLiveBloc();
  public tracker: LoopTracker = new LoopTracker();

  public token$: Observable<ContainerToken> = this.topLiveBloc.getToken();
  public isDataLoaded$: Observable<boolean> = this.topLiveBloc.isDataLoaded();
  public disciplines$: Observable<SportsGroup[]> = this.topLiveBloc.getDisciplines();
  public activeDiscipline$: Subject<SportsGroup> = this.topLiveBloc.getActiveDiscipline();
  public matchesInActiveDiscipline$: Observable<Match[]> = this.topLiveBloc.getMatchesInActiveDiscipline();
  public primaryMarkets$: Observable<Market[]> = this.topLiveBloc.getPrimaryMarkets();
  public selectedPrimaryColumns$: Observable<ObjectAsId<PrimaryColumnConfig>> = this.topLiveBloc.getSelectedPrimaryColumn();
  public primaryMarketsGroupedByMatch$: Observable<MarketsBelongsToMatch> = this.topLiveBloc.getPrimaryMarketsGroupedByMatch();
  public defaultPrimaryColumnConfig$: Observable<PrimaryColumnConfig> = this.topLiveBloc.getDefaultPrimaryColumnConfig();


  detector() {
    console.log('app component detector');
  }

  public ngOnDestroy(): void {
    this.topLiveBloc.dispose();
  }
}
