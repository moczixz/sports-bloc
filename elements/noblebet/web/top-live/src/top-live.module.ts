import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { TopLiveComponent } from './top-live.component';
import { CommonModule } from '@angular/common';
import { DisciplinesSelectModule } from '@projects/common/web/match-offer-widgets/disciplines-select/disciplines-select.module';
import { MatchRowModule } from '@projects/common/web/match-row/match-row.module';
import { OddsButtonsModule } from '@projects/common/web/match-offer-widgets/odds-buttons/odds-buttons.module';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [TopLiveComponent],
  imports: [
    BrowserModule,
    CommonModule,
    DisciplinesSelectModule,
    MatchRowModule,
    OddsButtonsModule,
    RouterModule.forRoot([{ path: "", component: TopLiveComponent}])
  ],
  providers: [

  ],
  bootstrap: [TopLiveComponent]
})
export class TopLiveModule {}