import { WebSocketSubject } from 'rxjs/webSocket';
import { Observable, Observer } from 'rxjs';

export class WebSocketSubjectExtended<T> extends WebSocketSubject<T> {

  private multiplexCount: {[channelName: string]: number} = {};

  public multiplexShared(channelName: string, subMsg: () => any, unsubMsg: () => any, messageFilter: (value: T) => boolean) {
    const self = this;
    return new Observable((observer: Observer<any>) => {
      try {
        if (!this.multiplexCount[channelName] || this.multiplexCount[channelName] === 0) {
          this.multiplexCount[channelName] = 0;
          self.next(subMsg());
        }
        this.multiplexCount[channelName]++;
      } catch (err) {
        observer.error(err);
      }

      const subscription = self.subscribe(x => {
        try {
          if (messageFilter(x)) {
            observer.next(x);
          }
        } catch (err) {
          observer.error(err);
        }
      },
        err => observer.error(err),
        () => observer.complete());

      return () => {
        try {
          if (this.multiplexCount[channelName] === 1) {
            self.next(unsubMsg());
          }
          this.multiplexCount[channelName]--;
        } catch (err) {
          observer.error(err);
        }
        subscription.unsubscribe();
      };
    });
  }

}
