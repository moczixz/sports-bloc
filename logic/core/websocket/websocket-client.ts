import { delay, takeUntil } from 'rxjs/operators';
import { Subject, Observable } from 'rxjs';
import { WebSocketSubjectExtended } from './websocket-subject-extended';
import { LazySingleton } from '@logic/di-resolver/di-resolver';
import { Environment } from '@logic/shared/environment';


@LazySingleton([Environment])
export class WebsocketClient {

  private socketUrl: string = this.env.sportsBookWs2 + `?language=en`;
  private socket$: WebSocketSubjectExtended<any>;

  private onOpenSocket$: Subject<Event> = new Subject<Event>();
  private onCloseSocket$: Subject<CloseEvent> = new Subject<CloseEvent>();
  private onDestroy$: Subject<void> = new Subject<void>();

  constructor(private env: Environment) {
    this.socket$ = new WebSocketSubjectExtended<any>({
      url: this.socketUrl,
      openObserver: this.onOpenSocket$,
      closeObserver: this.onCloseSocket$
    });
    this.startConnection();
    this.listenForCloseEvent();
  }

  private startConnection(): void {
    this.socket$
      .pipe(
        takeUntil(this.onDestroy$)
      )
      .subscribe();
  }

  private listenForCloseEvent(): void {
    this.onCloseSocket$
      .pipe(
        delay(500),
        takeUntil(this.onDestroy$)
      )
      .subscribe(() => this.startConnection());
  }

  public getSocket(): WebSocketSubjectExtended<any> {
    return this.socket$;
  }
  
  public onSocketOpen(): Observable<Event> {
    return this.onOpenSocket$.asObservable();
  }

  public ngOnDestroy(): void {
    this.onDestroy$.next(null);
    this.onDestroy$.complete();
  }

}