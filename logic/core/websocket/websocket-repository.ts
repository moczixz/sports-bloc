import { Observable, merge } from 'rxjs';
import { WebsocketApi } from './websocket-api';
import { LazySingleton } from '@logic/di-resolver/di-resolver';
import { Match } from '@logic/shared/models/match-offer/match';
import { Market } from '@logic/shared/models/match-offer/market';
import { MatchStats } from '@logic/shared/models/match-offer/match-stats';

@LazySingleton([WebsocketApi])
export class WebsocketRepository {

  constructor(private websocketApi: WebsocketApi) {
  }

  public matchesInSportsGroup(sportsGroupId: number): Observable<Match> {
    return this.websocketApi.matchesChannel(sportsGroupId);
  }

  public matchesInDisciplines(disciplines: number[]): Observable<Match> {
    return merge(
      ...disciplines.map((disciplineId: number) => this.matchesInSportsGroup(disciplineId))
    );
  }

  public marketsForMatch(matchId: string): Observable<any> {
    return this.websocketApi.marketsChannel(matchId);
  }

  public matchStatsForMatch(matchId: string): Observable<MatchStats> {
    return this.websocketApi.matchesStatsChannel(matchId);
  }

  public matchStatsForMatches(matchesIds: string[]): Observable<MatchStats> {
    return merge(
      ...matchesIds.map((matchId: string) => this.matchStatsForMatch(matchId))
    );
  }

  public marketsForMatches(matchesIds: string[]): Observable<Market> {
    return merge(
      ...matchesIds.map((matchId: string) => this.marketsForMatch(matchId))
    );
  }

}