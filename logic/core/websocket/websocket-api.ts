import { Observable } from 'rxjs';
import { retryWhen, delay, map } from 'rxjs/operators';
import { WebsocketClient } from './websocket-client';
import { LazySingleton } from '@logic/di-resolver/di-resolver';
import { Match } from '@logic/shared/models/match-offer/match';
import { MatchStats } from '@logic/shared/models/match-offer/match-stats';
import { Market } from '@logic/shared/models/match-offer/market';


@LazySingleton([WebsocketClient])
export class WebsocketApi {
  constructor(private websocketClient: WebsocketClient) {
  }

  public matchesChannel(sportsGroupId: number): Observable<Match> {
    const params = {
      filter: {
        sports_group_id: sportsGroupId
      }
    };
    return this.websocketClient.getSocket().multiplexShared(
      'matches_' + sportsGroupId,
      () => ({ action: 'subscribe', channel: 'matches', params }),
      () => ({ action: 'unsubscribe', channel: 'matches', params }),
      (message: any) => this.isMatchesChannel(message, sportsGroupId)
    )
    .pipe(
      map(Match.fromJsonApi2),
      this.resubscribePipe()
    );
  }

  public matchesStatsChannel(matchId: string): Observable<MatchStats> {
    const params = {
      filter: {
        match_id: matchId
      }
    };
    return this.websocketClient.getSocket().multiplexShared(
      'match-statistics_' + matchId,
      () => ({action: 'subscribe', channel: 'match-statistics', params }),
      () => ({action: 'unsubscribe', channel: 'match-statistics', params }),
      (message: any) => this.isMatchStatsChannel(message, matchId)
    )
    .pipe(
      map(MatchStats.fromJsonApi2),
      this.resubscribePipe() 
    );
  }

  public marketsChannel(matchId: string): Observable<Market> {
    const params = {
      filter: {
        match_id: matchId
      }
    };
    return this.websocketClient.getSocket().multiplexShared(
      'markets_' + matchId,
      () => ({action: 'subscribe', channel: 'markets', params }),
      () => ({action: 'unsubscribe', channel: 'markets', params }),
      (message: any) => this.isMarketsChannel(message, matchId)
    )
    .pipe(
      map(Market.fromJsonApi2),
      this.resubscribePipe() 
    );
  }

  private resubscribePipe(): any {
    return retryWhen(() => 
      this.websocketClient.onSocketOpen()
        .pipe(
          delay(100)
        )
    );
  }

  private isMatchesChannel(socketMessage: any, sportsGroupId: number): boolean {
    return socketMessage['channel'] === `matches_${sportsGroupId}`;
  }

  private isMarketsChannel(socketMessage: any, matchId: string): boolean {
    return socketMessage['channel'] === `markets_${matchId}`;
  }

  private isMatchStatsChannel(socketMessage: any, matchId: string): boolean {
    return socketMessage.hasOwnProperty('score') && socketMessage['match_id'] === matchId;
  }
}
