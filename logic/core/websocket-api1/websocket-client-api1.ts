import { delay, takeUntil, tap, filter, switchMap, mergeMap } from 'rxjs/operators';
import { Subject, Observable, timer } from 'rxjs';
import { LazySingleton } from '@logic/di-resolver/di-resolver';
import { Environment } from '@logic/shared/environment';
import { WebSocketSubjectExtended } from '../websocket/websocket-subject-extended';


@LazySingleton([Environment])
export class WebsocketClientApi1 {

  private socketUrl: string = this.env.sportsBookWs1 + `?language=en`;
  private socket$: WebSocketSubjectExtended<any>;

  private onOpenSocket$: Subject<Event> = new Subject<Event>();
  private onCloseSocket$: Subject<CloseEvent> = new Subject<CloseEvent>();
  private onDestroy$: Subject<void> = new Subject<void>();

  constructor(private env: Environment) {
    this.socket$ = new WebSocketSubjectExtended<any>({
      url: this.socketUrl,
      openObserver: this.onOpenSocket$,
      closeObserver: this.onCloseSocket$
    });
    this.startConnection();
    this.startPingPong();
    this.listenForCloseEvent();
  }

  private startConnection(): void {
    this.socket$
      .pipe(
        takeUntil(this.onDestroy$)
      )
      .subscribe();
  }

  private listenForCloseEvent(): void {
    this.onCloseSocket$
      .pipe(
        delay(500),
        takeUntil(this.onDestroy$)
      )
      .subscribe(() => this.startConnection());
  }

  private onPong(): Observable<void> {
    return this.socket$
      .pipe(
        filter((msg: any) => msg.action === 'pong')
      )
  }

  private startPingPong(): void {
    this.onOpenSocket$
      .pipe(
        tap(() => this.socket$.next({'action': 'ping'})),
        switchMap(() => 
          this.onPong()
            .pipe(
              mergeMap(() => timer(2000)),
              tap(() => this.socket$.next({'action': 'ping'}))
            )
        )
      )
      .subscribe();
  }

  public getSocket(): WebSocketSubjectExtended<any> {
    return this.socket$;
  }
  
  public onSocketOpen(): Observable<Event> {
    return this.onOpenSocket$.asObservable();
  }

  public ngOnDestroy(): void {
    this.onDestroy$.next(null);
    this.onDestroy$.complete();
  }

}