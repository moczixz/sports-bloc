import { Observable, merge } from 'rxjs';
import { LazySingleton } from '@logic/di-resolver/di-resolver';
import { Match } from '@logic/shared/models/match-offer/match';
import { WebsocketApi1 } from './websocket-api1';

@LazySingleton([WebsocketApi1])
export class WebsocketRepositoryApi1 {

  constructor(private websocketApi: WebsocketApi1) {
  }

  public matchesInSportsGroup(sportsGroupId: number): Observable<MatchApi1Raw> {
    return this.websocketApi.matchesChannel(sportsGroupId);
  }

  public matchesInDisciplines(disciplines: number[]): Observable<MatchApi1Raw> {
    return merge(
      ...disciplines.map((disciplineId: number) => this.matchesInSportsGroup(disciplineId))
    );
  }

}