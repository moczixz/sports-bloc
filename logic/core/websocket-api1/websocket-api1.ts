import { Observable } from 'rxjs';
import { retryWhen, delay, map, tap } from 'rxjs/operators';
import { WebsocketClientApi1 } from './websocket-client-api1';
import { LazySingleton } from '@logic/di-resolver/di-resolver';
import { Match } from '@logic/shared/models/match-offer/match';


@LazySingleton([WebsocketClientApi1])
export class WebsocketApi1 {
  constructor(private websocketClient: WebsocketClientApi1) {
  }

  public matchesChannel(sportsGroupId: number): Observable<MatchApi1Raw> {
    const params = {
      filter: {
        _sports_group: sportsGroupId
      }
    };
    return this.websocketClient.getSocket().multiplexShared(
      'matches_' + sportsGroupId,
      () => ({ action: 'subscribe', channel: 'matches', params }),
      () => ({ action: 'unsubscribe', channel: 'matches', params }),
      (message: any) => this.isMatchesChannel(message, sportsGroupId)
    )
    .pipe(
      this.resubscribePipe()
    );
  }

  private resubscribePipe(): any {
    return retryWhen(() => 
      this.websocketClient.onSocketOpen()
        .pipe(
          delay(100)
        )
    );
  }

  private isMatchesChannel(socketMessage: any, sportsGroupId: number): boolean {
    return socketMessage['channel'] === `matches_${sportsGroupId}`;
  }
}
