import { HttpClient } from '../http-client';
import { LazySingleton } from '@logic/di-resolver/di-resolver';
import { Environment } from '@logic/shared/environment';
import { Observable, of, EMPTY } from 'rxjs';
import { map, expand, tap } from 'rxjs/operators';
import { PrimaryColumnConfig } from '@logic/shared/models/match-offer/primary-column-config';
import { filterDuplicate } from '@logic/shared/helpers/pure-utils';


@LazySingleton([HttpClient, Environment])
export class PrimaryColumns1Api {

  private apiUrl: string = this.env.sportsBookApi1 + 'sports-groups/';

  constructor(private http: HttpClient, private env: Environment) {
    
  }

  public getAllPrimaryColumnConfigs(): Observable<PrimaryColumnConfig[]> {
    return this.http.get(`${this.apiUrl}tree/`)
      .pipe(
        //expand((data: any) => data.children ? of(data.children) : EMPTY),
        //tap(d => console.log('expanded', d)),
        map((data: any[]) => 
          data
          .filter(data => Boolean(data.primaryMarketTypes))
          .reduce((acc: PrimaryColumnConfig[], data: any) => {
            return [...acc, ...PrimaryColumnConfig.fromJsonApi1Array(data.primaryMarketTypes)]
          }, [])
        ),
        map((data: PrimaryColumnConfig[]) => data.filter(filterDuplicate('id')))
      )
  }

}
