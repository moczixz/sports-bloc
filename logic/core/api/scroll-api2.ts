import { Observable, EMPTY } from 'rxjs';
import { expand, reduce } from 'rxjs/operators';
import { Provide } from '@logic/di-resolver/di-resolver';
import { HttpClient } from '@logic/core/http-client';

interface ScrollIdApi<T> {
  hits: T;
  scroll_id: string;
}

@Provide([HttpClient])
export class ScrollApi {

  constructor(private http: HttpClient) {
  }

  public httpGetScroll<T>(url: string, params: any = {}): Observable<T[]> {
    return this.scrollIdApiGet(url, params)
      .pipe(
        reduce((acc: T[], curr: ScrollIdApi<T[]>) => [...acc, ...curr.hits], [])
      );
  }

  public httpPostScroll<T>(url: string,  body: any): Observable<T[]> {
    return this.scrollIdApiPost(url, body)
      .pipe(
        reduce((acc: T[], curr: ScrollIdApi<T[]>) => [...acc, ...curr.hits], [])
      );
  }

  private scrollIdApiGet<T>(url: string, params: any = {}): Observable<ScrollIdApi<T[]>> {
    return this.http.get(url, { params })
      .pipe(
        expand((response: ScrollIdApi<T[]>) => {
          return this.canExpandByScroll(response) ?
            this.http.get<ScrollIdApi<T[]>>(url, {params: this.getParamsWithScroll(params, response)})
            :
            EMPTY;
        })
      );
  }

  private scrollIdApiPost<T>(url: string, body: any): Observable<ScrollIdApi<T[]>> {
    return this.http.post(url, body)
      .pipe(
        expand((response: ScrollIdApi<T[]>) => {
          return this.canExpandByScroll(response) ?
            this.http.post<ScrollIdApi<T[]>>(url, this.getBodyWithScroll(body, response))
            :
            EMPTY;
        })
      );
  }

  private canExpandByScroll<T>(response: ScrollIdApi<T[]>): boolean {
    return response.hits && response.hits.length > 0 && response.scroll_id && response.scroll_id.length > 0;
  }

  private getParamsWithScroll<T>(params: any, response: ScrollIdApi<T[]>): any {
    return response.scroll_id ? 
      params.append('scroll_id', response.scroll_id) 
      : 
      params;
  }

  private getBodyWithScroll<T>(body: object, response: ScrollIdApi<T[]>): object {
    return response.scroll_id ?
      {...body, ...{ scroll_id: response.scroll_id } }
      :
      body;
  }

}
