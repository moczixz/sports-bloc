import { HttpClient } from '../http-client';
import { LazySingleton } from '@logic/di-resolver/di-resolver';
import { Environment } from '@logic/shared/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { PrimaryColumnConfig } from '@logic/shared/models/match-offer/primary-column-config';


@LazySingleton([HttpClient, Environment])
export class PrimaryColumns2Api {

  private apiUrl: string = this.env.sportsBookApi2 + 'primary-columns/';

  constructor(private http: HttpClient, private env: Environment) {
    
  }

  public getPrimaryColumnConfig(primaryColumnIds: number[]): Observable<PrimaryColumnConfig[]> {
    let columnsJoined: string = primaryColumnIds.filter(Boolean).join(',');
    if (columnsJoined[columnsJoined.length - 1] === ',') {
      columnsJoined = columnsJoined.substring(0, columnsJoined.length - 1);
    }
    return this.http.get(`${this.apiUrl}?primary_columns_ids=${columnsJoined}`)
      .pipe(
        map(PrimaryColumnConfig.fromJsonApi2Array)
      );
  }

}
