import { HttpClient } from '../http-client';
import { LazySingleton } from '@logic/di-resolver/di-resolver';
import { Environment } from '@logic/shared/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ScrollApi } from './scroll-api2';
import * as moment from 'moment';
import { Match } from '@logic/shared/models/match-offer/match';
import { Market } from '@logic/shared/models/match-offer/market';
import { MatchStats } from '@logic/shared/models/match-offer/match-stats';

export interface MatchesApiConfig {
  isLive?: boolean;
  isPre?: boolean;
  sportsGroupsIds?: number[];
  dateFrom?: moment.Moment;
  dateTo?: moment.Moment;
  matchesIds?: (string | number)[];
  limit?: number;
}

@LazySingleton([HttpClient, Environment])
export class Matches2Api {
  
  private apiUrl: string = this.env.sportsBookApi2 + 'matches/';
  private scrollApi: ScrollApi;

  constructor(private http: HttpClient, private env: Environment) {
    this.scrollApi = new ScrollApi(http);
  }

  public getTopMatches(config: MatchesApiConfig): Observable<Match[]> {
    const matchType: string = config.isLive ? 'live' : 'pre';
    delete config.isLive;
    delete config.isPre;
    return this.http.get(`${this.apiUrl}top/${matchType}/`, { params: this.prepareParams(config) })
      .pipe(
        map(Match.fromJsonApi2Array)
      );
  }

  public getPrimaryMarkets(config: MatchesApiConfig): Observable<Market[]> {
    return this.scrollApi.httpPostScroll(`${this.apiUrl}primary-markets/search`, { matches_ids: config.matchesIds })
      .pipe(
        map(Market.fromJsonApi2Array)
      );
}

  public getPrimaryMarket(matchId: string): Observable<Market> {
    return this.scrollApi.httpGetScroll(`${this.apiUrl}primary-markets/?matches_ids=${matchId}`)
      .pipe(
        map(Market.fromJsonApi2Array),
        map((markets: Market[]) => markets[0])
      );
  }

  public getMatches(config: MatchesApiConfig): Observable<Match[]> {
    return this.scrollApi.httpGetScroll(this.apiUrl, this.prepareParams(config))
      .pipe(map(Match.fromJsonApi2Array));
  }

  public getMatch(matchId: string): Observable<Match> {
    return this.http.get(this.apiUrl + matchId)
      .pipe(
        map(Match.fromJsonApi2)
      );
  }

  public getMatchesStatistics(config: MatchesApiConfig): Observable<MatchStats[]> {
    return this.http.post(`${this.apiUrl}statistics/search`, { matches_ids: config.matchesIds })
      .pipe(
        map(MatchStats.fromJsonApi2Array)
      );
}

  private prepareParams(config: MatchesApiConfig): any {
    const httpParamsObject: any = {};
    if (config.hasOwnProperty('isLive')) {
      httpParamsObject['is_live'] = config.isLive;
    }
    if (config.hasOwnProperty('isPre')) {
      httpParamsObject['is_pre'] = config.isPre;
    }
    if (config.hasOwnProperty('sportsGroupsIds')) {
      httpParamsObject['sports_groups_ids'] = config.sportsGroupsIds.join(',');
    }
    if (config.hasOwnProperty('dateFrom')) {
      httpParamsObject['date_from'] = config.dateFrom.format('YYYY-MM-DD HH:mm:ss');
    }
    if (config.hasOwnProperty('dateTo')) {
      httpParamsObject['date_to'] = config.dateTo.format('YYYY-MM-DD HH:mm:ss');
    }
    if (config.hasOwnProperty('matchesIds')) {
      httpParamsObject['matches_ids'] = config.matchesIds.join(',');
    }
    if (config.hasOwnProperty('limit')) {
      httpParamsObject['limit'] = config.limit;
    }
    return httpParamsObject;
  }
}
