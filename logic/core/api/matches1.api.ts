import { HttpClient } from "../http-client";
import { Environment } from "@logic/shared/environment";
import { LazySingleton } from "@logic/di-resolver/di-resolver";
import { MatchesApiConfig } from "./matches2.api";
import { Observable, VirtualTimeScheduler } from "rxjs";
import { Match } from "@logic/shared/models/match-offer/match";
import { map } from "rxjs/operators";

@LazySingleton([HttpClient, Environment])
export class Matches1Api {
  
  private apiUrl: string = this.env.sportsBookApi1 + 'matches/';

  constructor(private http: HttpClient, private env: Environment) {
  }

  public getTopMatches(config: MatchesApiConfig): Observable<Match[]> {
    const matchType: string = config.isLive ? 'live' : 'pre';
    delete config.isLive;
    delete config.isPre;
    return this.http.get(`${this.apiUrl}top/${matchType}/`, { params: this.prepareParams(config) })
      .pipe(
        map(Match.fromJsonApi1Array)
      );
  }


  private prepareParams(config: MatchesApiConfig): any {
    const httpParamsObject: any = {};
    if (config.hasOwnProperty('limit')) {
      httpParamsObject['limit'] = config.limit;
    }
    return httpParamsObject;
  }

}