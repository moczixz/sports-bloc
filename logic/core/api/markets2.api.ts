import { HttpClient } from '../http-client';
import { LazySingleton } from '@logic/di-resolver/di-resolver';
import { Environment } from '@logic/shared/environment';
import { Observable, zip } from 'rxjs';

import { map } from 'rxjs/operators';
import { ScrollApi } from './scroll-api2';
import { chunk } from '../../shared/helpers/pure-utils';
import { Market } from '@logic/shared/models/match-offer/market';

@LazySingleton([HttpClient, Environment])
export class Markets2Api {
  
  private apiUrl: string = this.env.sportsBookApi2  + 'markets/';
  private scrollApi: ScrollApi;

  constructor(private http: HttpClient, private env: Environment) {
    this.scrollApi = new ScrollApi(http);
  }

  public getMarketsByMatchId(matchId: string): Observable<Market[]> {
    return this.scrollApi.httpGetScroll(`${this.apiUrl}match/${matchId}`)
      .pipe(map(Market.fromJsonApi2Array));
  }

  public getMarketsByMatchesIds(matchesIds: string[]): Observable<Market[]> {
    return this.scrollApi.httpPostScroll<any>(`${ this.apiUrl }search`, { matches_ids: matchesIds })
      .pipe(
        map(Market.fromJsonApi2Array)
      );
  }

  public getMarketsByIds(marketsIds: string[]): Observable<Market[]> {
    const paramsLimit: number = 1000;
    const chunkSize: number = paramsLimit / marketsIds[0].length;

    return zip(
      ...chunk(marketsIds, chunkSize).map((marketsIds: string[]) => 
      this.scrollApi.httpGetScroll(`${this.apiUrl}?markets_ids=${marketsIds.join(',')}`)
      )
    )
    .pipe(
      map((nestedMarketsData: any[][]) => 
        nestedMarketsData.reduce((array: any[], current: any) => array.concat(current), [])
      ),
      map(Market.fromJsonApi2Array)
    );
  }

  public getById(marketId: string): Observable<Market> {
    return this.http.get(this.apiUrl + marketId)
      .pipe(map((Market.fromJsonApi2)));
  }

}
