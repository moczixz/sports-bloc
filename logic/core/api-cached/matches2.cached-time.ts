import { LazySingleton } from '@logic/di-resolver/di-resolver';
import { Matches2Api, MatchesApiConfig } from '@logic/core/api/matches2.api';
import { Observable } from 'rxjs';
import { cacheTime } from '@logic/shared/helpers/rxjs-helpers';
import { MatchStats } from '@logic/shared/models/match-offer/match-stats';
import { Market } from '@logic/shared/models/match-offer/market';
import { Match } from '@logic/shared/models/match-offer/match';

export interface CachedTimeMatches {
  isLive?: boolean;
  isPre?: boolean;
  limit: number;
  matches$: Observable<Match[]>;
}

interface CachedTimeMatchesContainerConfig {
  limit: number;
  isLive: boolean;
  isPre: boolean;
  container: CachedTimeMatches[];
  matchCacheSource$: Observable<Match[]>;
}

interface CachedTimeMatch {
  matchId: string;
  match$: Observable<Match>;
}

interface SimpleCachedTimeContainer<T> {
  [key: string]: Observable<T>;
}

@LazySingleton([Matches2Api])
export class Matches2CachedTime {
  private readonly TEMP_CACHE_TIME: number = 5000;
  private cachedTimeTopMatches: CachedTimeMatches[] = [];
  private cachedTimeMatches: CachedTimeMatches[] = [];
  private cachedSingleMatches: CachedTimeMatch[] = [];
  private cachedPrimaryMarkets: SimpleCachedTimeContainer<Market[]> = {};
  private cachedMatchesStatistics: SimpleCachedTimeContainer<MatchStats[]> = {};

  constructor(private matchesApi: Matches2Api) {
  }

  public getMatchesTopCachedTime(isLive: boolean, isPre: boolean, limit: number): Observable<Match[]> {
    return this.findOrCreateCacheTimeMatches({
      isLive, 
      isPre, 
      limit,
      container: this.cachedTimeTopMatches,
      matchCacheSource$: this.matchesApi.getTopMatches({ isPre, isLive, limit }).pipe(cacheTime(this.TEMP_CACHE_TIME))
    }).matches$;
  }

  public getMatchesCachedTime(isLive: boolean, isPre: boolean): Observable<Match[]> {
    return this.findOrCreateCacheTimeMatches({
      isLive, 
      isPre, 
      limit: null,
      container: this.cachedTimeMatches,
      matchCacheSource$: this.matchesApi.getMatches({ isPre, isLive }).pipe(cacheTime(this.TEMP_CACHE_TIME))
    }).matches$; 
  }

  public getMatchByIdCachedTime(matchId: string): Observable<Match> {
    return this.findOrCreateCacheTimeMatch(matchId).match$;
  }

  public getPrimaryMarketsCachedTime(config: MatchesApiConfig): Observable<Market[]> {
    const matchesIdsKey: string = config.matchesIds.join('');
    if (!this.cachedPrimaryMarkets[matchesIdsKey]) {
      this.cachedPrimaryMarkets[matchesIdsKey] = this.matchesApi.getPrimaryMarkets(config)
        .pipe(cacheTime(this.TEMP_CACHE_TIME));
    }
    return this.cachedPrimaryMarkets[matchesIdsKey];
  }

  public getMatchesStatisticsCachedTime(config: MatchesApiConfig): Observable<MatchStats[]> {
    const matchesIdsKey: string = config.matchesIds.join('');
    if (!this.cachedMatchesStatistics[matchesIdsKey]) {
      this.cachedMatchesStatistics[matchesIdsKey] = this.matchesApi.getMatchesStatistics(config)
        .pipe(cacheTime(this.TEMP_CACHE_TIME));
    }
    return this.cachedMatchesStatistics[matchesIdsKey];
  }

  private findOrCreateCacheTimeMatches(config: CachedTimeMatchesContainerConfig): CachedTimeMatches {
    const foundCacheMatches: CachedTimeMatches = config.container.find((cachedMatch: CachedTimeMatches) => 
      cachedMatch.isLive === config.isLive && cachedMatch.isPre === config.isPre && cachedMatch.limit === config.limit
    );
    if (foundCacheMatches) {
      return foundCacheMatches;
    }
    else {
      const newCacheMatch: CachedTimeMatches = {
        isLive: config.isLive, 
        isPre: config.isPre, 
        limit: config.limit,
        matches$: config.matchCacheSource$
      };
      config.container.push(newCacheMatch);
      return newCacheMatch;
    }
  }

  private findOrCreateCacheTimeMatch(matchId: string): CachedTimeMatch {
    const foundCacheMarket: CachedTimeMatch = this.cachedSingleMatches.find((cachedMatch: CachedTimeMatch) => 
      cachedMatch.matchId === matchId
    );

    if (foundCacheMarket) {
      return foundCacheMarket;
    }
    else {
      const newCacheMatch: CachedTimeMatch = {
        matchId,
        match$: this.matchesApi.getMatch(matchId).pipe(cacheTime(this.TEMP_CACHE_TIME))
      };
      this.cachedSingleMatches.push(newCacheMatch);
      return newCacheMatch;
    }
  }

}
