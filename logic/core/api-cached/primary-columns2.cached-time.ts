import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { PrimaryColumns2Api } from '../api/primary-columns2.api';
import { cacheTime } from '@logic/shared/helpers/rxjs-helpers';
import { LazySingleton } from '@logic/di-resolver/di-resolver';
import { PrimaryColumnConfig } from '@logic/shared/models/match-offer/primary-column-config';

interface PrimaryColumnsCachedContainer {
  [key: string]: Observable<PrimaryColumnConfig[]>;
}

interface PrimaryColumnsSingleCachedContainer {
  [key: string]: Observable<PrimaryColumnConfig>;
}

@LazySingleton([PrimaryColumns2Api])
export class PrimaryColumns2CachedTime {
  private readonly CACHED_TIME: number = 60 * 60 * 1000; // 1 hour
  private primaryColumnConfigByIdsCached: PrimaryColumnsCachedContainer = {};
  private primaryColumnConfigCached: PrimaryColumnsSingleCachedContainer = {};

  constructor(private primaryColumnsApi: PrimaryColumns2Api) {
  }

  public getPrimaryColumnsConfigsByIdsCachedTime(primaryColumnIds: number[]): Observable<PrimaryColumnConfig[]> {
    // sort to always have the same string with diffrent incoming array
    const configIdsString: string = primaryColumnIds.sort((idA: number, idB: number) => idA - idB).join('');
    if (!this.primaryColumnConfigByIdsCached[configIdsString]) {
      this.primaryColumnConfigByIdsCached[configIdsString] = this.primaryColumnsApi.getPrimaryColumnConfig(primaryColumnIds)
        .pipe(cacheTime(this.CACHED_TIME));
    }
    return this.primaryColumnConfigByIdsCached[configIdsString];
  }

  public getPrimaryColumnConfigCachedTime(primaryColumnId: number): Observable<PrimaryColumnConfig> {
    if (!this.primaryColumnConfigCached[primaryColumnId]) {
      this.primaryColumnConfigCached[primaryColumnId] = this.primaryColumnsApi.getPrimaryColumnConfig([primaryColumnId])
        .pipe(
          map((primaryColumn: PrimaryColumnConfig[]) => primaryColumn[0]),
          cacheTime(this.CACHED_TIME)
        );
    }
    return this.primaryColumnConfigCached[primaryColumnId];
  }
}
