import { LazySingleton } from '@logic/di-resolver/di-resolver';
import { Matches2Api } from '@logic/core/api/matches2.api';
import { Observable } from 'rxjs';
import { Markets2Api } from '../api/markets2.api';
import { cacheTime } from '@logic/shared/helpers/rxjs-helpers';
import { Market } from '@logic/shared/models/match-offer/market';


interface CachedTimeMarket {
  marketId: string;
  market$: Observable<Market>;
}

@LazySingleton([Matches2Api])
export class MarketsCachedTimeService {
  private readonly TEMP_CACHE_TIME: number = 5000;
  private cachedSingleMarkets: CachedTimeMarket[] = [];

  constructor(private marketsApiProvider: Markets2Api) {
  }

  public getMarketByIdCachedTime(marketId: string): Observable<Market> {
    return this.findOrCreateCacheTimeMarket(marketId).market$;
  }

  private findOrCreateCacheTimeMarket(marketId: string): CachedTimeMarket {
    const foundCacheMarket: CachedTimeMarket = this.cachedSingleMarkets.find((cachedMarket: CachedTimeMarket) => 
      cachedMarket.marketId === marketId
    );

    if (foundCacheMarket) {
      return foundCacheMarket;
    }
    else {
      const newCacheMarket: CachedTimeMarket = {
        marketId,
        market$: this.marketsApiProvider.getById(marketId).pipe(cacheTime(this.TEMP_CACHE_TIME))
      };
      this.cachedSingleMarkets.push(newCacheMarket);
      return newCacheMarket;
    }
  }

}
