import { LazySingleton } from "@logic/di-resolver/di-resolver";
import { Matches1Api } from "../api/matches1.api";
import { Observable } from "rxjs";
import { Match } from "@logic/shared/models/match-offer/match";

@LazySingleton([Matches1Api])
export class Matches1Repository {

  constructor(private matchesApi: Matches1Api) {
    
  }

  public getTopLive(limit?: number): Observable<Match[]> {
    return this.matchesApi.getTopMatches({ isLive: true, limit });
  }

}