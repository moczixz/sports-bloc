import { LazySingleton } from '@logic/di-resolver/di-resolver';
import { PrimaryColumns1Api } from '../api/primary-columns1.api';
import { shareReplay, tap } from 'rxjs/operators';
import { PrimaryColumnConfig } from '@logic/shared/models/match-offer/primary-column-config';
import { Observable } from 'rxjs';


@LazySingleton([PrimaryColumns1Api])
export class PrimaryColumns1Repository {
  
  constructor(private primaryColumnsApi: PrimaryColumns1Api) {
  }

  public allPrimaryColumnsConfig$: Observable<PrimaryColumnConfig[]> = this.primaryColumnsApi.getAllPrimaryColumnConfigs()
    .pipe(
      shareReplay(1)
    );
}
