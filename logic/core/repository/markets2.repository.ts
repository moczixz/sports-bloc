import { LazySingleton } from '@logic/di-resolver/di-resolver';
import { Observable } from 'rxjs';
import { Markets2Api } from '../api/markets2.api';
import { MarketsCachedTimeService } from '../api-cached/markets2.cached-time';
import { extract } from '@logic/shared/helpers/pure-utils';
import { Market } from '@logic/shared/models/match-offer/market';
import { Match } from '@logic/shared/models/match-offer/match';


@LazySingleton([Markets2Api])
export class Markets2Repository {

  constructor(private marketsApiProvider: Markets2Api, private marketsCachedTime: MarketsCachedTimeService) {
  }

  public getMarketsByMatchId(matchId: string): Observable<Market[]> {
    return this.marketsApiProvider.getMarketsByMatchId(matchId);
  }

  public getMarketsForMatches(matches: Match[]): Observable<Market[]> {
    return this.marketsApiProvider.getMarketsByMatchesIds(extract(matches, 'id'));
  }

  public getMarketById(marketId: string): Observable<Market> {
    return this.marketsCachedTime.getMarketByIdCachedTime(marketId);
  }

  public getMarketsByIds(marketsIds: string[]): Observable<Market[]> {
    return this.marketsApiProvider.getMarketsByIds(marketsIds);
  }

}
