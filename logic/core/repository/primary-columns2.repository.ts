import { Observable } from 'rxjs';
import { PrimaryColumns2Api } from '../api/primary-columns2.api';
import { LazySingleton } from '@logic/di-resolver/di-resolver';
import { PrimaryColumns2CachedTime } from '../api-cached/primary-columns2.cached-time';
import { PrimaryColumnConfig } from '@logic/shared/models/match-offer/primary-column-config';


@LazySingleton([PrimaryColumns2Api, PrimaryColumns2CachedTime])
export class PrimaryColumns2Repository {
  
  constructor(private primaryColumnsApi: PrimaryColumns2Api, private primaryColumnsCached: PrimaryColumns2CachedTime) {
  }

  public getPrimaryColumnsConfigsByIds(primaryColumnIds: number[]): Observable<PrimaryColumnConfig[]> {
    return this.primaryColumnsCached.getPrimaryColumnsConfigsByIdsCachedTime(primaryColumnIds);
  }

  public getPrimaryColumnConfig(primaryColumnId: number): Observable<PrimaryColumnConfig> {
    return this.primaryColumnsCached.getPrimaryColumnConfigCachedTime(primaryColumnId);
  }
}
