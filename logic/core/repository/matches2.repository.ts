import { LazySingleton } from '@logic/di-resolver/di-resolver';
import { Matches2Api } from '@logic/core/api/matches2.api';
import { zip, of, Observable } from 'rxjs';
import { mergeMap, map } from 'rxjs/operators';
import * as moment from 'moment';
import { extract } from '@logic/shared/helpers/pure-utils';
import { filterEmptyArray } from '@logic/shared/helpers/rxjs-helpers';
import { Market } from '@logic/shared/models/match-offer/market';
import { Matches2CachedTime } from '../api-cached/matches2.cached-time';
import { Match } from '@logic/shared/models/match-offer/match';
import { MatchStats } from '@logic/shared/models/match-offer/match-stats';
;

export type MatchesWithPrimaryMarkets = [Match, Market[]];


@LazySingleton([Matches2Api, Matches2CachedTime])
export class Matches2Repository {

  constructor(private matchesApi: Matches2Api, private matchesCachedTime: Matches2CachedTime) {
  }

  public getPrimaryMarketsForMatches(matches: Match[]): Observable<Market[]> {
    if (!matches || matches.length === 0) {
      return of([]);
    }
    return this.matchesCachedTime.getPrimaryMarketsCachedTime({
      matchesIds: extract(matches, 'id')
    });
  }

  public getPrimaryMarketsForMatchesIds(matchesIds: string[]): Observable<Market[]> {
    if (!matchesIds || matchesIds.length === 0) {
      return of([]);
    }
    return this.matchesCachedTime.getPrimaryMarketsCachedTime({ matchesIds }); 
  }

  public getMatchesStatistics(matches: Match[]): Observable<MatchStats[]> {
    if (!matches || matches.length === 0) {
      return of([]);
    }
    return this.matchesCachedTime.getMatchesStatisticsCachedTime({
      matchesIds: extract(matches, 'id')
    });
  }

  public getMatchesStatisticsByMatchesIds(matchesIds: string[]): Observable<MatchStats[]> {
    if (!matchesIds || matchesIds.length === 0) {
      return of([]);
    }
    return this.matchesCachedTime.getMatchesStatisticsCachedTime({ matchesIds });
  }

  public getMatchStatistic(match: Match): Observable<MatchStats> {
    return this.matchesApi.getMatchesStatistics({
      matchesIds: [match.id]
    })
      .pipe(
        filterEmptyArray(),
        map((stats: MatchStats[]) => stats[0])
      );
  }

  public getPrimaryMarketForMatch(matchId: string): Observable<Market> {
    return this.matchesApi.getPrimaryMarket(matchId);
  }

  public getPrimaryMarketsForMatch(matchId: string): Observable<Market[]> {
    return this.matchesApi.getPrimaryMarkets({
      matchesIds: [matchId]
    });
  }

  public getMatchById(matchId: string): Observable<Match> {
    return this.matchesCachedTime.getMatchByIdCachedTime(matchId);
  }

  public getMatchesPreByIds(matchesIds: string[]): Observable<Match[]> {
    return this.matchesApi.getMatches({
      matchesIds,
      isLive: false
    });
  }

  public getMatchesByIds(matchesIds: string[]): Observable<Match[]> {
    return this.matchesApi.getMatches({
      matchesIds
    });
  }

  public getMatchInArray(matchId: string): Observable<Match[]> {
    return this.matchesApi.getMatch(matchId)
      .pipe(
        map((match: Match) => [match])
      );
  }

  public getTopLive(limit?: number): Observable<Match[]> {
    return limit ?
      this.matchesCachedTime.getMatchesTopCachedTime(true, false, limit)
      :
      this.matchesApi.getTopMatches({ isLive: true });
  }

  public getTopPre(limit?: number): Observable<Match[]> {
    return limit ?
      this.matchesCachedTime.getMatchesTopCachedTime(false, true, limit)
      :
      this.matchesApi.getTopMatches({ isPre: true });
  }

  // cacheTime, because few component may used exactly the same resource in the same time
  // example: live conference / sports sidebar
  public getMatchesLive(): Observable<Match[]> {
    return this.matchesCachedTime.getMatchesCachedTime(true, false);
  }

  public getMatchesPreForLeagues(leaguesIds: number[]): Observable<Match[]> {
    return this.matchesApi.getMatches({
      sportsGroupsIds: leaguesIds,
      isLive: false
    });
  }

  public getMatchesPreForSportsGroupsFilterByToDate(sportsGroupsIds: number[], dateTo: moment.Moment): Observable<Match[]> {
    return this.matchesApi.getMatches({
      isPre: true,
      sportsGroupsIds,
      dateTo
    });
  }

  public getMatchesForDisciplinesLive(disciplineIds: number[]): Observable<Match[]> {
    return this.matchesApi.getMatches({
      sportsGroupsIds: disciplineIds,
      isLive: true
    });
  }

  public getSelectedMatchesLive(matchesIds: string[]): Observable<Match[]> {
    return this.matchesApi.getMatches({
      matchesIds, isLive: true
    });
  }

  public getTopLiveWithPrimaryMarkets(limit?: number): Observable<MatchesWithPrimaryMarkets> {
    return this.getTopLive(limit)
      .pipe(mergeMap(this.lazyLoadPrimaryMarket.bind(this)))
  }

  public getTopPreWithPrimaryMarkets(limit?: number): Observable<MatchesWithPrimaryMarkets> {
    return this.getTopPre(limit)
      .pipe(mergeMap(this.lazyLoadPrimaryMarket.bind(this)))
  }

  public getMatchesLiveWithPrimaryMarkets(): Observable<MatchesWithPrimaryMarkets> {
    return this.matchesApi.getMatches({ isLive: true  })
      .pipe(mergeMap(this.lazyLoadPrimaryMarket.bind(this)));
  }

  public getSpecificMatchesLiveWithPrimaryMarkets(matchesIds: string[]): Observable<MatchesWithPrimaryMarkets> {
    return this.matchesApi.getMatches({ isLive: true, matchesIds })
      .pipe(mergeMap(this.lazyLoadPrimaryMarket.bind(this)));
  }

  public getMatchesPreForLeaguesWithPrimaryMarkets(leaguesIds: number[]): Observable<MatchesWithPrimaryMarkets> {
    return this.matchesApi.getMatches({ sportsGroupsIds: leaguesIds })
      .pipe(mergeMap(this.lazyLoadPrimaryMarket.bind(this)));
  }

  private lazyLoadPrimaryMarket(matches: Match[]): any {
    return zip(
      of(matches),
      this.getPrimaryMarketsForMatches(matches)
    );
  }
}
