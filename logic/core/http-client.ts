import { ajax, AjaxError } from "rxjs/ajax";
//const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
import { Observable, of, throwError, OperatorFunction } from "rxjs";
import { map, catchError } from "rxjs/operators";
import { UnauthorizedException } from '../shared/exceptions/unauthorized.exception';
import { BadRequestException } from '../shared/exceptions/bad-request.exception';
import { Singleton } from "../di-resolver/di-resolver";

@Singleton([])
export class HttpClient {
  constructor() {}

  public get<T>(url: string, options: { params?: any, headers?: any } = {} as any): Observable<any> {
    return ajax({
      //createXHR: () => new XMLHttpRequest(),
      url: this.prepareUrl(url, options.params),
      headers: this.prepareHeaders(options.headers),
      method: "GET"
    }).pipe(map(res => res.response));
  }

  public post<T>(url: string, body: any): Observable<T> {
    return ajax({
      //createXHR: () => new XMLHttpRequest(),
      url: this.prepareUrl(url),
      method: "POST",
      headers: this.prepareHeaders(),
      body
    }).pipe(
      map(res => res.response as T),
      this.interceptError() as any
    );
  }

  private interceptError(): OperatorFunction<AjaxError, any> {
    return catchError((err: AjaxError) => {
      if (err.status === 400) {
        return throwError(new BadRequestException(err.response))
      } 
      else if(err.status === 401) {
        return throwError(new UnauthorizedException(err.response.detail))
      }
      else {
        console.log('http error', err);
        return of();
      }
    }) 
  }

  private prepareUrl(url: string, params: any = {}): string {
    const paramsString = params && Object.keys(params).length > 0 ? 
      Object.keys(params).reduce((paramString: string, key: string) => paramString + `${key}=${params[key]}`, '?') : '';

    return url + paramsString;
    //return url.includes("http") ? url : `${process.env.API_URL}${url}` || url;
  }

  protected prepareHeaders(headers: any = {}): Object {
    const defaultHeaders = {
      'Content-Type': 'application/json'
    };
    return {
      ...defaultHeaders,
      ...headers
    };
  }
}
