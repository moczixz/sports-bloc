import { Provide } from '@logic/di-resolver/di-resolver';
import { BehaviorSubject, Observable, Subject, OperatorFunction, throwError, merge, MonoTypeOperatorFunction, of, from, pipe, UnaryFunction } from 'rxjs';
import { WebsocketRepository } from '@logic/core/websocket/websocket-repository';
import { catchError, filter, switchMap, map, scan, auditTime, takeUntil, tap, distinctUntilChanged, withLatestFrom, startWith } from 'rxjs/operators';
import { transformObjectToArray, extract } from '@logic/shared/helpers/pure-utils';
import { MatchesCommon } from '../matches-common';
import { WsPool } from '../interfaces/ws-pool.interface';
import { Match } from '@logic/shared/models/match-offer/match';
import { DataObjectContainer } from '../interfaces/data-object-container.interface';
import { DataContainerChanged } from '../interfaces/data-container-changed.interface';
import { Disposable } from '@logic/shared/interfaces/disposable.interface';
import * as moment from 'moment';
import { attachSource } from './matches-helpers';
import { SourceType } from '../enums/source-type.enum';
import { WithSource } from '../interfaces/with-source.interface';
import { MatchesSharedService } from '../matches-shared.service';

@Provide([MatchesSharedService, WebsocketRepository])
export class MatchesApi2Loader implements Disposable {
  private httpSource: Observable<Match[]>;
  private common: MatchesCommon;
  
  private matchesWhiteList: Match[] = [];
  private destroy$: Subject<void> = new Subject<void>();
  
  constructor(
    private matchesSharedService: MatchesSharedService,
    private websocketRepository: WebsocketRepository) {
  }

  private interceptLoadMatchError(): OperatorFunction<Match[], Match[]> {
    return catchError((err: any) => {
      this.matchesHttpLoadError$.next(null);
      return throwError(err);
    });
  }

  private get matches$(): BehaviorSubject<Match[]> {
    return this.matchesSharedService.getContainer(this.common.getToken()).matches$
  }

  private get matchesHttpLoadError$(): Subject<void> {
    return this.matchesSharedService.getContainer(this.common.getToken()).matchesHttpLoadError$
  }

  private get rawMatchesHttp$(): BehaviorSubject<Match[]> {
    return this.matchesSharedService.getContainer(this.common.getToken()).rawMatchesHttp$;
  }

  private get matchWsPool(): WsPool<Match> {
    return this.matchesSharedService.getContainer(this.common.getToken()).matchWsPool;
  } 

  private set matchWsPool(newPool: WsPool<Match>) {
    this.matchesSharedService.getContainer(this.common.getToken()).matchWsPool = newPool;
  }

  public setCommon(common: MatchesCommon): void {
    this.common = common;
  }

  public setHttpSource(httpSource: Observable<Match[]>): void {
    this.httpSource = httpSource;
  }

  private chooseWebsocketRepositorySource(matches: Match[]): Observable<Match> {
    if (this.common.getConfig().selectedLeaguesIds && this.common.getConfig().selectedLeaguesIds.length > 0) {
      return merge(
        ...this.common.getConfig().selectedLeaguesIds.map((leagueId: number) => 
          this.websocketRepository.matchesInSportsGroup(leagueId)
        )
      );
    }
    return this.websocketRepository.matchesInDisciplines(extract(extract(matches, 'discipline'), 'id'));
  }


  private canAddMatch(container: DataContainerChanged<Match>, data: WithSource<Match>): boolean {
    if (data.source === SourceType.http) {
      return true;
    }
    if (!this.common.getConfig()) {
      return true;
    }
    const match: Match = data.payload;

    const passMatchState: boolean = this.common.getConfig().bypassMatchStateLiveFilter 
      || typeof this.common.getConfig().isLive === 'undefined' 
      || match.state.isLive === this.common.getConfig().isLive;
/*
    const isMatchDateIsBeforeConfigDateTo: boolean = 
      (!this.common.getConfig().matchesDateTo || 
        (this.common.getConfig().matchesDateTo 
          && moment(match.date).isBefore(moment(this.common.getConfig().matchesDateTo))
        )
      );
*/
    const isMatchesContainerLengthIsBellowLimit: boolean = 
      (!this.common.getConfig().limit || 
        (!container[match.id] && Object.keys(container).length < this.common.getConfig().limit)
      );

    // should we add new match for top five ???

    //return passMatchState || isMatchDateIsBeforeConfigDateTo || isMatchesContainerLengthIsBellowLimit;
    return true;
  }

  public loadMatches(): void {
    this.common.onRestart()
      .pipe(
        switchMap(() => this.httpSource),
        this.interceptLoadMatchError(),
        tap((matches: Match[]) => this.matchesWhiteList = matches.slice()),
        tap((matches: Match[]) => this.rawMatchesHttp$.next(matches)),
        switchMap((matches: Match[]) => 
          merge(
            of(null),
            from(matches).pipe(attachSource(SourceType.http)),
            this.chooseWebsocketRepositorySource(matches).pipe(attachSource(SourceType.socket))
          )
        ),
        scan((container: DataContainerChanged<Match>, data: WithSource<Match>) => {
          container.changed = false;
          if (data === null) {
            this.matchWsPool = {};
            return { changed: true, data: {} };
          }
          const match: Match = data.payload;
          if (container.data[match.id]) {
            // match exist so just push update
            this.matchWsPool[match.id].next(match);
          }
          else if(this.canAddMatch(container, data)) {
            // match dont exist and can add
            container.data[match.id] = match;
            this.matchWsPool[match.id] = new BehaviorSubject(match);
            container.changed = true;
            this.common.newMatch$.next(match);
          }
          return container;
        }, { changed: false, data: {} }),
        filter((container: DataContainerChanged<Match>) => container.changed),
        map((container: DataContainerChanged<Match>) => container.data),
        auditTime(10),
        startWith({}),
        takeUntil(this.destroy$),
      )
      .subscribe((container: DataObjectContainer<Match>) => 
        this.matches$.next(transformObjectToArray(container))
      )
  }

  public dispose(): void {
    this.destroy$.next(null);
    this.destroy$.complete();
  }

}