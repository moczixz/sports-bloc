import { Provide } from "@logic/di-resolver/di-resolver";
import { MatchesCommon } from "../matches-common";
import { Subject, merge, of, BehaviorSubject, iif, defer, from, Observable } from "rxjs";
import { MatchStats } from "@logic/shared/models/match-offer/match-stats";
import { switchMap, tap, filter, mergeMap, map, scan, auditTime, takeUntil, bufferTime, startWith } from "rxjs/operators";
import { Match } from "@logic/shared/models/match-offer/match";
import { WsPool } from "../interfaces/ws-pool.interface";
import { Matches2Repository } from "@logic/core/repository/matches2.repository";
import { dontCompleteStreamOnError, filterEmptyArray } from "@logic/shared/helpers/rxjs-helpers";
import { transformObjectToArray, extract } from "@logic/shared/helpers/pure-utils";
import { DataContainerChanged } from "../interfaces/data-container-changed.interface";
import { DataObjectContainer } from "../interfaces/data-object-container.interface";
import { WebsocketRepository } from "@logic/core/websocket/websocket-repository";
import { Disposable } from "@logic/shared/interfaces/disposable.interface";
import { MatchesSharedService } from "../matches-shared.service";

@Provide([MatchesSharedService, Matches2Repository, WebsocketRepository])
export class MatchesStatsApi2Loader implements Disposable {
  private common: MatchesCommon;

  private destroy$: Subject<void> = new Subject<void>();

  constructor(
    private matchesSharedService: MatchesSharedService,
    private matchesRepository: Matches2Repository,
    private websocketRepository: WebsocketRepository) {
  }

  public setCommon(common: MatchesCommon): void {
    this.common = common;
  }

  private get matchStatsWsPool(): WsPool<MatchStats> {
    return this.matchesSharedService.getContainer(this.common.getToken()).matchStatsWsPool;
  }

  private set matchStatsWsPool(newPool: WsPool<MatchStats>) {
    this.matchesSharedService.getContainer(this.common.getToken()).matchStatsWsPool = newPool;
  }

  private get matchesStats$(): BehaviorSubject<MatchStats[]> {
    return this.matchesSharedService.getContainer(this.common.getToken()).matchesStats$
  }


  private createWsPoolForMatch(match: Match): void {
    if (!this.matchStatsWsPool[match.id]) {
      this.matchStatsWsPool[match.id] = new BehaviorSubject(null);
    }
  }


  public loadMatchStats(): void {
    const matchStatsSocket$: Subject<MatchStats> = new Subject<MatchStats>();
    
    this.common.onRestart()
      .pipe(
        switchMap(() =>
          merge(
            of(null),
            this.common.newMatch$
              .pipe(
                tap((match: Match) => this.createWsPoolForMatch(match)),
                bufferTime(10),
                filterEmptyArray(),
                mergeMap((matches: Match[]) =>
                  this.matchesRepository.getMatchesStatistics(matches)
                    .pipe(
                      dontCompleteStreamOnError(), // need, not catched error complete the stream :(
                      filterEmptyArray(),
                      mergeMap((stats: MatchStats[])  => from(stats))
                    )
                )
              ),
            matchStatsSocket$
          )
        ),
        scan((container: DataContainerChanged<MatchStats>, matchStats: MatchStats) => {
          container.changed = false;
          if (matchStats === null) {
            this.matchStatsWsPool = {};
            return { changed: true, data: {} };
          }
          if (!container.data[matchStats.matchId]) {
            container.data[matchStats.matchId] = matchStats;
            this.matchStatsWsPool[matchStats.matchId] = new BehaviorSubject(matchStats);
            this.matchStatsWsPool[matchStats.matchId].next(matchStats);
            container.changed = true;
          }
          else {
            this.matchStatsWsPool[matchStats.matchId].next(matchStats);
          }
          return container;
        }, { changed: false, data: {}}),
        filter((container: DataContainerChanged<MatchStats>) => container.changed),
        map((container: DataContainerChanged<MatchStats>) => container.data),
        auditTime(10),
        startWith({}),
        takeUntil(this.destroy$)
      )
      .subscribe((container: DataObjectContainer<MatchStats>) => 
        this.matchesStats$.next(transformObjectToArray(container))
      )
      
    this.common.onRestart()
      .pipe(
        switchMap(() => this.common.newMatch$),
        switchMap((match: Match) => this.websocketRepository.matchStatsForMatch(match.id.toString())),
        takeUntil(this.destroy$)
      ).subscribe((stats: MatchStats) => matchStatsSocket$.next(stats));
  }

  public dispose(): void {
    this.destroy$.next(null);
    this.destroy$.next(null);
  }
}