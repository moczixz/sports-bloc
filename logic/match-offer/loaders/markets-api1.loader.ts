import { Provide } from "@logic/di-resolver/di-resolver";
import { MatchesSharedService } from "../matches-shared.service";
import { Disposable } from "@logic/shared/interfaces/disposable.interface";
import { MatchesCommon } from "../matches-common";
import { Subject, BehaviorSubject, merge, from, Observable, of } from "rxjs";
import { WsPool } from "../interfaces/ws-pool.interface";
import { Market } from "@logic/shared/models/match-offer/market";
import { filter, switchMap, map, mergeScan, auditTime, takeUntil, tap, } from "rxjs/operators";
import { attachSource } from "./matches-helpers";
import { SourceType } from "../enums/source-type.enum";
import { WithSource } from "../interfaces/with-source.interface";
import { ObjectAsId } from "@logic/shared/interfaces/object-as-id.interface";
import { transformObjectToArray } from "@logic/shared/helpers/pure-utils";
import { PrimaryColumns1Repository } from "@logic/core/repository/primary-columns1.repository";
import { PrimaryColumnConfig } from "@logic/shared/models/match-offer/primary-column-config";

@Provide([MatchesSharedService, PrimaryColumns1Repository])
export class MarketsApi1Loader implements Disposable {
  private common: MatchesCommon;


  private destroy$: Subject<void> = new Subject<void>();

  constructor(
    private matchesSharedService: MatchesSharedService, private primaryColumnsRepository: PrimaryColumns1Repository) {
  }

  public setCommon(common: MatchesCommon): void {
    this.common = common;
  }

  private get marketWsPool(): WsPool<Market> {
    return this.matchesSharedService.getContainer(this.common.getToken()).marketWsPool;
  }

  private set marketWsPool(newPool: WsPool<Market>) {
    this.matchesSharedService.getContainer(this.common.getToken()).marketWsPool = newPool;
  }

  private get markets$(): BehaviorSubject<Market[]> {
    return this.matchesSharedService.getContainer(this.common.getToken()).markets$;
  }

  private get rawMarketsHttp$(): BehaviorSubject<Market[]> {
    return this.matchesSharedService.getContainer(this.common.getToken()).rawMarketsHttp$
  }

  private dealWithIncomingMarket(container: ObjectAsId<Market>, curr: WithSource<PrimaryMarket>): any {
    if (curr.source === SourceType.http) {
      const market: Market = Market.fromJsonApi1(curr.payload);
      container[market.id] = market;
      this.marketWsPool[market.id] = new BehaviorSubject(market);
      return of(container);
    }
    else {
      const market: PrimaryMarket = curr.payload as PrimaryMarket;
      if (container[market.id]) {
        const existinMarketAsJson: PrimaryMarket = JSON.parse(JSON.stringify(container[curr.payload.id]));

        const fff = market.selections.find(s => s.id === 23103794572) 

        const selections: Selection[] = existinMarketAsJson.selections.map((selection: Selection) => {
          const found = market.selections.find(s => s.id === selection.id);
          if (found) {
            return {
              ...selection,
              ...found
            }
          }
          return selection;
        })
        curr.payload.selections = selections;

        const mergedMarket = {
          ...existinMarketAsJson,
          ...curr.payload
        }
        this.marketWsPool[market.id].next(Market.fromJsonApi1(mergedMarket));
      }
      else {
        // we dont have this market, what now??
      }
      return of(null);
    }
  }

  public loadMarkets(): void {
    this.loadPrimaryColumns();
    this.common.onRestart()
      .pipe(
        filter(() => !this.common.getConfig().disableLoadPrimaryMarkets),
        switchMap(() =>
          merge(
            this.common.newMarketApi1$.pipe(attachSource(SourceType.http)),
            this.common.updateMarketApi1$.pipe(attachSource(SourceType.socket))
          )
        ),
        mergeScan((acc: ObjectAsId<Market>, curr: WithSource<Market | any>) => {
          return curr !== null ?
            this.dealWithIncomingMarket(acc, curr)
              .pipe(
                filter(Boolean),
                map((item: ObjectAsId<Market>) => item ? ({...acc, ...item}) : acc = {})
              )
              :
            of({});
        }, {}, 1),
        auditTime(0),
        map((market: ObjectAsId<Market>) => transformObjectToArray(market)),
        takeUntil(this.destroy$)
      )
      .subscribe((markets: Market[]) => this.markets$.next(markets));
  }

  private loadPrimaryColumns(): void {
    this.primaryColumnsRepository.allPrimaryColumnsConfig$
      .subscribe((configs: PrimaryColumnConfig[]) => 
        this.matchesSharedService.getContainer(this.common.getToken()).primaryColumnsConfig$.next(configs)
      );
  }

  public dispose(): void {
    this.destroy$.next(null);
    this.destroy$.complete();
  }

}