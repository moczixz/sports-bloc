import { Provide } from "@logic/di-resolver/di-resolver";
import { MatchesSharedService } from "../matches-shared.service";
import { Disposable } from "@logic/shared/interfaces/disposable.interface";
import { MatchesCommon } from "../matches-common";
import { Subject, BehaviorSubject, merge, from, Observable, of } from "rxjs";
import { WsPool } from "../interfaces/ws-pool.interface";
import { Market } from "@logic/shared/models/match-offer/market";
import { filter, switchMap, map, mergeScan, auditTime, takeUntil, tap, } from "rxjs/operators";
import { attachSource } from "./matches-helpers";
import { SourceType } from "../enums/source-type.enum";
import { WithSource } from "../interfaces/with-source.interface";
import { ObjectAsId } from "@logic/shared/interfaces/object-as-id.interface";
import { transformObjectToArray } from "@logic/shared/helpers/pure-utils";
import { MatchStats } from "@logic/shared/models/match-offer/match-stats";

@Provide([MatchesSharedService])
export class MatchesStatsApi1Loader implements Disposable {
  private common: MatchesCommon;


  private destroy$: Subject<void> = new Subject<void>();

  constructor(
    private matchesSharedService: MatchesSharedService) {
  }

  public setCommon(common: MatchesCommon): void {
    this.common = common;
  }

  private get matchStatsWsPool(): WsPool<MatchStats> {
    return this.matchesSharedService.getContainer(this.common.getToken()).matchStatsWsPool;
  }

  private set matchStatsWsPool(newPool: WsPool<MatchStats>) {
    this.matchesSharedService.getContainer(this.common.getToken()).matchStatsWsPool = newPool;
  }

  private get matchesStats$(): BehaviorSubject<MatchStats[]> {
    return this.matchesSharedService.getContainer(this.common.getToken()).matchesStats$
  }

  private dealWithIncomingStat(container: ObjectAsId<MatchStats>, curr: WithSource<any>): any {
    if (curr.source === SourceType.http) {
      const matchStats: MatchStats = MatchStats.fromJsonApi1(curr.payload);
      container[matchStats.id] = matchStats;
      this.matchStatsWsPool[matchStats.matchId] = new BehaviorSubject(matchStats);
      return of(container);
    }
    else {
      const matchStats: any = curr.payload;
      if (container[matchStats.id]) {
        const existinStatAsJson: MatchStats = JSON.parse(JSON.stringify(container[curr.payload.id]));
        const mergedStat = {
          ...existinStatAsJson,
          ...curr.payload
        }
        this.matchStatsWsPool[mergedStat.matchId].next(MatchStats.fromJsonApi1(mergedStat));
      }
      else {
        // we dont have this market, what now??
      }
      return of(null);
    }
  }

  public loadMatchesStats(): void {
    this.common.onRestart()
      .pipe(
        switchMap(() =>
          merge(
            this.common.newStatApi1$.pipe(attachSource(SourceType.http)),
            this.common.updateStatApi1$.pipe(attachSource(SourceType.socket))
          )
        ),
        mergeScan((acc: ObjectAsId<MatchStats>, curr: WithSource<any>) => {
          return curr !== null ?
            this.dealWithIncomingStat(acc, curr)
              .pipe(
                filter(Boolean),
                map((item: ObjectAsId<MatchStats>) => item ? ({...acc, ...item}) : acc = {})
              )
              :
            of({});
        }, {}, 1),
        auditTime(0),
        map((market: ObjectAsId<Market>) => transformObjectToArray(market)),
        takeUntil(this.destroy$)
      )
      .subscribe((stats: MatchStats[]) => this.matchesStats$.next(stats));
  }

  public dispose(): void {
    this.destroy$.next(null);
    this.destroy$.complete();
  }

}