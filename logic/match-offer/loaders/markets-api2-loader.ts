import { MatchesCommon } from "../matches-common";
import { Subject, of, merge, Observable, iif, defer, from, BehaviorSubject } from "rxjs";
import { filter, switchMap, bufferTime, tap, mergeMap, scan, map, auditTime, takeUntil, distinctUntilChanged, startWith } from "rxjs/operators";
import { Provide } from "@logic/di-resolver/di-resolver";
import { Matches2Repository } from "@logic/core/repository/matches2.repository";
import { Markets2Repository } from "@logic/core/repository/markets2.repository";
import { WebsocketRepository } from "@logic/core/websocket/websocket-repository";
import { MarketsHttpSourceType } from "../enums/markets-http-source-type.enum";
import { dontCompleteStreamOnError, filterEmptyArray } from "@logic/shared/helpers/rxjs-helpers";
import { transformObjectToArray, extract } from "@logic/shared/helpers/pure-utils";
import { Market } from "@logic/shared/models/match-offer/market";
import { Match } from "@logic/shared/models/match-offer/match";
import { DataObjectContainer } from "../interfaces/data-object-container.interface";
import { WsPool } from "../interfaces/ws-pool.interface";
import { DataContainerChanged } from "../interfaces/data-container-changed.interface";
import { Disposable } from "@logic/shared/interfaces/disposable.interface";
import { attachSource } from "./matches-helpers";
import { SourceType } from "../enums/source-type.enum";
import { WithSource } from "../interfaces/with-source.interface";
import { MatchesSharedService } from "../matches-shared.service";

@Provide([MatchesSharedService, Matches2Repository, Markets2Repository, WebsocketRepository])
export class MarketsApi2Loader implements Disposable {
  private common: MatchesCommon;


  private destroy$: Subject<void> = new Subject<void>();

  constructor(
    private matchesSharedService: MatchesSharedService,
    private matchesRepository: Matches2Repository,
    private marketsRepository: Markets2Repository,
    private websocketRepository: WebsocketRepository) {
  }

  public setCommon(common: MatchesCommon): void {
    this.common = common;
  }

  private get marketWsPool(): WsPool<Market> {
    return this.matchesSharedService.getContainer(this.common.getToken()).marketWsPool;
  }

  private set marketWsPool(newPool: WsPool<Market>) {
    this.matchesSharedService.getContainer(this.common.getToken()).marketWsPool = newPool;
  }

  private get markets$(): BehaviorSubject<Market[]> {
    return this.matchesSharedService.getContainer(this.common.getToken()).markets$;
  }

  private get rawMarketsHttp$(): BehaviorSubject<Market[]> {
    return this.matchesSharedService.getContainer(this.common.getToken()).rawMarketsHttp$
  }

  private chooseMarketsHttpSource(matches: Match[]): Observable<Market[]> {
    if (this.common.getConfig().marketsHttpSourceType === MarketsHttpSourceType.markets) {
      return this.marketsRepository.getMarketsForMatches(matches);
    }
    return this.matchesRepository.getPrimaryMarketsForMatches(matches);
  }

  private chooseMarketHttpSource(matchId: string): Observable<Market[]> {
    if (this.common.getConfig().marketsHttpSourceType === MarketsHttpSourceType.markets) {
      return this.marketsRepository.getMarketsByMatchId(matchId);
    }
    return this.matchesRepository.getPrimaryMarketsForMatch(matchId);
  }

  private canAddMarket(container: DataContainerChanged<Market>, data: WithSource<Market>): boolean {
    return true;
  }

    /*
    in loadMatches we push match to two stream multiMatch$ and singleMatch$
    here we merge it into one and fetch the primaryMarkets depends if we have array or single match
    then we use mergeScan and accumulate markets data over time.
    in first merge, we send "null" to reset the accumulated data
*/
  
public loadMarkets(): void {
  const marketSocket$: Subject<Market> = new Subject<Market>();
  this.common.onRestart()
    .pipe(
      filter(() => !this.common.getConfig().disableLoadPrimaryMarkets),
      switchMap(() => 
        merge(
          of(null),
          this.common.newMatch$
            .pipe(
              bufferTime(10),
              filterEmptyArray(),
              mergeMap((matches: Match[]) => 
                this.chooseMarketsHttpSource(matches)
                  .pipe(
                    tap((markets: Market[]) => this.rawMarketsHttp$.next(markets)),
                    dontCompleteStreamOnError(), // need, not catched error complete the stream :(
                    filterEmptyArray(),
                    //tap((markets: Market[]) => this.common.multiMarket$.next(markets)),
                    mergeMap((markets: Market[])  => from(markets).pipe(attachSource(SourceType.http)))
                  )
              )
            ),
          marketSocket$.pipe(attachSource(SourceType.socket))
        )
      ),
      scan((container: DataContainerChanged<Market>, data: WithSource<Market>) => {
        container.changed = false;
        if (data === null) {
          container.changed = true;
          return { changed: false, data: {} };
        }
        const market: Market = data.payload;
        if (container.data[market.id]) {
          this.marketWsPool[market.id].next(market);
        }
        else if(this.canAddMarket(container, data)) {
          container.data[market.id] = market;
          this.marketWsPool[market.id] = new BehaviorSubject(market);
          container.changed = true;
          this.common.newMarket$.next(market);
        }
        return container;
      }, { changed: false, data: {} }),
      filter((container: DataContainerChanged<Market>) => container.changed),
      map((container: DataContainerChanged<Market>) => container.data),
      auditTime(10),
      startWith({}),
      takeUntil(this.destroy$)
    )
    .subscribe((container: DataObjectContainer<Market>) => 
      this.markets$.next(transformObjectToArray(container))
    )

  this.common.onRestart()
    .pipe(
      switchMap(() => this.common.newMatch$),
      switchMap((match: Match) => this.websocketRepository.marketsForMatch(match.id.toString())),
      takeUntil(this.destroy$)
    ).subscribe((marketSocket: Market) => marketSocket$.next(marketSocket));
  }

  public dispose(): void {
    this.destroy$.next(null);
    this.destroy$.next(null);
  }

}