import { SourceType } from "../enums/source-type.enum";
import { map } from "rxjs/operators";
import { WithSource } from "../interfaces/with-source.interface";
import { OperatorFunction } from "rxjs";

export function attachSource<T>(source: SourceType): OperatorFunction<T, WithSource<T>> {
  return map((data: T) => ({ payload: data, source: source }));
}

