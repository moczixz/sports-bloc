import { Provide } from "@logic/di-resolver/di-resolver";
import { MatchesCommon } from "../matches-common";
import { filter, switchMap, mergeMap, map, scan, auditTime, takeUntil, bufferTime } from "rxjs/operators";
import { from, Subject, BehaviorSubject, Observable, pipe, UnaryFunction } from "rxjs";
import { filterEmptyArray, dontCompleteStreamOnError } from "@logic/shared/helpers/rxjs-helpers";
import { Market } from "@logic/shared/models/match-offer/market";
import { PrimaryColumns2Repository } from "@logic/core/repository/primary-columns2.repository";
import { extract, filterDuplicate, transformObjectToArray } from "@logic/shared/helpers/pure-utils";
import { PrimaryColumnConfig } from "@logic/shared/models/match-offer/primary-column-config";
import { ObjectAsId } from "@logic/shared/interfaces/object-as-id.interface";
import { Disposable } from "@logic/shared/interfaces/disposable.interface";
import { MatchesSharedService } from "../matches-shared.service";

@Provide([MatchesSharedService, PrimaryColumns2Repository])
export class PrimaryColumnConfigApi2Loader implements Disposable {
  private common: MatchesCommon;


  private primaryColumnsConfig$: BehaviorSubject<PrimaryColumnConfig[]>;
  private destroy$: Subject<void> = new Subject<void>();

  constructor(
    private matchesSharedService: MatchesSharedService,
    private primaryColumnRepository: PrimaryColumns2Repository) {
  }

  public setCommon(common: MatchesCommon): void {
    this.common = common;
    this.primaryColumnsConfig$ = this.matchesSharedService.getContainer(common.getToken()).primaryColumnsConfig$;
  }

  private filterUndefinedPrimaryColumn(): UnaryFunction<Observable<Market[]>, Observable<Market[]>> {
    return pipe(
      map((markets: Market[]) => markets.filter((market: Market) => market.primaryColumn)),
      filterEmptyArray()
    )
  }

  public loadPrimaryColumns(): void {
    this.common.onRestart()
      .pipe(
        filter(() => !this.common.getConfig().disableLoadPrimaryMarkets && !this.common.getConfig().disableLoadPrimaryColumns),
        switchMap(() => 
          this.common.newMarket$
            .pipe(
              bufferTime(10),
              filterEmptyArray(),
              this.filterUndefinedPrimaryColumn(),
              mergeMap((markets: Market[]) => 
                this.primaryColumnRepository.getPrimaryColumnsConfigsByIds(
                  this.getUniqueMarketsPrimaryColumnIds(markets)
                )
                .pipe(
                  dontCompleteStreamOnError(), // need, not catched error complete the stream :(
                  filterEmptyArray(),
                  mergeMap((primaryColumns: PrimaryColumnConfig[]) => from(primaryColumns))
                )
              )
            )
        ),
        map((config: PrimaryColumnConfig) => ({ [config.id]: config })),
        scan((acc: ObjectAsId<PrimaryColumnConfig>, curr: ObjectAsId<PrimaryColumnConfig>) => ({...acc, ...curr}), {}),
        auditTime(0),
        map((configKeeper: ObjectAsId<PrimaryColumnConfig>) => transformObjectToArray(configKeeper)),
        takeUntil(this.destroy$)
      )
      .subscribe((primaryColumns: PrimaryColumnConfig[]) => this.primaryColumnsConfig$.next(primaryColumns))
  }

  private getUniqueMarketsPrimaryColumnIds(markets: Market[]): number[] {
    return extract(markets.filter(filterDuplicate('primaryColumn')), 'primaryColumn');
  }

  public dispose(): void {
    this.destroy$.next(null);
    this.destroy$.next(null);
  }
}