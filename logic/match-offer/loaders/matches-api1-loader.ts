import { Provide } from '@logic/di-resolver/di-resolver';
import { BehaviorSubject, Observable, Subject, OperatorFunction, throwError, merge, of, from } from 'rxjs';

import { MatchesCommon } from '../matches-common';
import { WsPool } from '../interfaces/ws-pool.interface';
import { Match } from '@logic/shared/models/match-offer/match';
import { Disposable } from '@logic/shared/interfaces/disposable.interface';
import { MatchesSharedService } from '../matches-shared.service';
import { switchMap, catchError, tap, mergeScan, filter, map, auditTime, takeUntil } from 'rxjs/operators';
import { WebsocketRepositoryApi1 } from '@logic/core/websocket-api1/websocket-repository-api1';
import { attachSource } from './matches-helpers';
import { extract, transformObjectToArray } from '@logic/shared/helpers/pure-utils';
import { SourceType } from '../enums/source-type.enum';
import { ObjectAsId } from '@logic/shared/interfaces/object-as-id.interface';
import { WithSource } from '../interfaces/with-source.interface';
import { Market } from '@logic/shared/models/match-offer/market';

@Provide([MatchesSharedService, WebsocketRepositoryApi1])
export class MatchesApi1Loader implements Disposable {
  private httpSource: Observable<Match[]>;
  private common: MatchesCommon;
  
  private matchesWhiteList: Match[] = [];
  private destroy$: Subject<void> = new Subject<void>();
  
  constructor(
    private matchesSharedService: MatchesSharedService,
    private websocketRepository: WebsocketRepositoryApi1) {
  }

  private interceptLoadMatchError(): OperatorFunction<Match[], Match[]> {
    return catchError((err: any) => {
      this.matchesHttpLoadError$.next(null);
      return throwError(err);
    });
  }

  private get matches$(): BehaviorSubject<Match[]> {
    return this.matchesSharedService.getContainer(this.common.getToken()).matches$
  }

  private get matchesHttpLoadError$(): Subject<void> {
    return this.matchesSharedService.getContainer(this.common.getToken()).matchesHttpLoadError$
  }

  private get rawMatchesHttp$(): BehaviorSubject<Match[]> {
    return this.matchesSharedService.getContainer(this.common.getToken()).rawMatchesHttp$;
  }

  private get rawMarketsHttp$(): BehaviorSubject<Market[]> {
    return this.matchesSharedService.getContainer(this.common.getToken()).rawMarketsHttp$;
  }

  private get matchWsPool(): WsPool<Match> {
    return this.matchesSharedService.getContainer(this.common.getToken()).matchWsPool;
  } 

  private set matchWsPool(newPool: WsPool<Match>) {
    this.matchesSharedService.getContainer(this.common.getToken()).matchWsPool = newPool;
  }

  public setCommon(common: MatchesCommon): void {
    this.common = common;
  }

  public setHttpSource(httpSource: Observable<Match[]>): void {
    this.httpSource = httpSource;
  }

  private chooseWebsocketRepositorySource(matches: Match[]): Observable<MatchApi1Raw> {
    if (this.common.getConfig().selectedLeaguesIds && this.common.getConfig().selectedLeaguesIds.length > 0) {
      return merge(
        ...this.common.getConfig().selectedLeaguesIds.map((leagueId: number) => 
          this.websocketRepository.matchesInSportsGroup(leagueId)
        )
      );
    }
    return this.websocketRepository.matchesInDisciplines(extract(extract(matches, 'discipline'), 'id'));
  }

  private dealWithIncomingMatch(container: ObjectAsId<Match>, curr: WithSource<Match | MatchApi1Raw>): any {
    
    if (curr.source === SourceType.http) {
      const match: Match = curr.payload as Match;
      container[match.id] = match as any;
      this.matchWsPool[match.id] = new BehaviorSubject(match as Match);
      match.rawData.primaryMarkets.forEach(el => this.common.newMarketApi1$.next(el) );

      this.common.newStatApi1$.next({...match.rawData.stats, matchId: match.id});
      return of(container);
    }
    else {
      const match: MatchApi1Raw= curr.payload as MatchApi1Raw;
      if (container[curr.payload.id]) {
        const existinMatchAsJson: MatchApi1Raw = JSON.parse(JSON.stringify(container[curr.payload.id].rawData));

        const mergedMatch = {
          ...existinMatchAsJson,
          ...curr.payload
        }
        this.matchWsPool[match.id].next(Match.fromJsonApi1(mergedMatch));
        if (match.primaryMarkets) {
          match.primaryMarkets.forEach(el => this.common.updateMarketApi1$.next(el));
        }
        if (match.stats) {
          this.common.updateStatApi1$.next({...match.stats, matchId: match.id});
        }
      }
      else {
        // got match we dont have! what now???
      }
      return of(null);
    }
  }

  private pushMarketsFromMatches(matches: Match[]): void {
    const markets: any[] = matches.reduce((acc: any[], match: Match) => [...acc, ...match.rawData.primaryMarkets], []);
    this.rawMarketsHttp$.next(Market.fromJsonApi1Array(markets));
  }

  public loadMatches(): void {
    this.common.onRestart()
    .pipe(
      switchMap(() => this.httpSource),
      this.interceptLoadMatchError(),
      tap((matches: Match[]) => this.matchesWhiteList = matches.slice()),
      tap((matches: Match[]) => {
        this.rawMatchesHttp$.next(matches); 
        this.pushMarketsFromMatches(matches);
      }),
      switchMap((matches: Match[]) => 
        merge(
          of(null),
          from(matches).pipe(attachSource(SourceType.http)),
          this.chooseWebsocketRepositorySource(matches).pipe(attachSource(SourceType.socket))
        )
      ),
      mergeScan((acc: ObjectAsId<Match>, curr: WithSource<Match | MatchApi1Raw>) => { // accumulate data over time so we can fetch new match over http and push forward
        return curr !== null ?
          this.dealWithIncomingMatch(acc, curr)
            .pipe(
              filter(Boolean),
              map((item: ObjectAsId<Match>) => item ? ({...acc, ...item}) : acc = {})
            )
            :
          of({});
      }, {}, 1),
      auditTime(0),
      map((match: ObjectAsId<Match>) => transformObjectToArray(match)),
      takeUntil(this.destroy$)
    )
    .subscribe((matches: Match[]) => this.matches$.next(matches));
  }


  public dispose(): void {
    this.destroy$.next(null);
    this.destroy$.complete();
  }

}