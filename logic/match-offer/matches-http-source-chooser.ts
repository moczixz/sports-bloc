import { Provide } from "@logic/di-resolver/di-resolver";
import { Matches2Repository } from "@logic/core/repository/matches2.repository";
import { MatchesContainerConfig } from "./interfaces/matches-container-config.interface";
import { Observable, forkJoin } from "rxjs";
import { mapFlatArray } from "@logic/shared/helpers/rxjs-helpers";
import { Match } from "@logic/shared/models/match-offer/match";

@Provide([Matches2Repository])
export class MatchesHttpSourceChooser {

  constructor(
    private matchesRepository: Matches2Repository) {
  }

  public chooseHttpMatchesSource(config: MatchesContainerConfig): Observable<Match[]> {
    if (config.isTopFive) {
      return config.isLive ? 
        this.matchesRepository.getTopLive(config.limit) 
        : 
        this.matchesRepository.getTopPre(config.limit);
    }
    else if (config.selectedLeaguesIds && config.selectedLeaguesIds.length > 0) {
      return this.matchesRepository.getMatchesPreForLeagues(config.selectedLeaguesIds);
    }
    else if (config.selectedMatchesIds && config.selectedMatchesIds.length > 0) {
      return this.matchesRepository.getSelectedMatchesLive(config.selectedMatchesIds);
    }
    else if (config.singleMatchId && config.singleMatchId.length > 0) {
      return this.matchesRepository.getMatchInArray(config.singleMatchId);
    }
    else if (config.isTopPreLiveCarousel) {
      return forkJoin(
        this.matchesRepository.getTopLive(config.topPreLiveCarouselLiveLimit),
        this.matchesRepository.getTopPre(config.topPreLiveCarouselPreLimit)
      ).pipe(mapFlatArray());
    } 
    else {
      return this.matchesRepository.getMatchesLive();
    }
  }
}