import { LazySingleton } from "@logic/di-resolver/di-resolver";
import { Observable, BehaviorSubject, Subject } from "rxjs";
import { Match } from "@logic/shared/models/match-offer/match";
import { DataContainerToken } from "./interfaces/data-container-token.interface";
import { ContainerToken } from "./enums/container-token.enum";
import { WsPool } from "./interfaces/ws-pool.interface";
import { DataContainer } from "./interfaces/data-container.interface";
import { Market } from "@logic/shared/models/match-offer/market";
import { MatchStats } from "@logic/shared/models/match-offer/match-stats";
import { PrimaryColumnConfig } from "@logic/shared/models/match-offer/primary-column-config";

@LazySingleton()
export class MatchesSharedService {

  private container: DataContainerToken = {}

  constructor() {
    this.initializeContainers();
  }

  private initializeContainers() {
    Object.keys(ContainerToken).forEach(key => 
      this.container[ContainerToken[key]] = {
        matchesHttpLoadError$: new Subject<void>(),
        matchWsPool: {},
        matches$:  new BehaviorSubject<Match[]>(null),
        marketWsPool: {},
        markets$: new BehaviorSubject<Market[]>(null),
        matchStatsWsPool: {},
        matchesStats$: new BehaviorSubject<MatchStats[]>(null),
        primaryColumnsConfig$: new BehaviorSubject<PrimaryColumnConfig[]>(null),
        rawMatchesHttp$: new BehaviorSubject<Match[]>(null),
        rawMarketsHttp$:  new BehaviorSubject<Market[]>(null),
      } 
    )
  }

  public cleanContainer(token: ContainerToken): void {
    this.container[token].matchWsPool = {};
    this.container[token].matches$.next(null);
    this.container[token].marketWsPool = {};
    this.container[token].markets$.next(null);
    this.container[token].matchStatsWsPool = {};
    this.container[token].matchesStats$.next(null);
    this.container[token].primaryColumnsConfig$.next(null);
    this.container[token].rawMarketsHttp$.next(null);
    this.container[token].rawMatchesHttp$.next(null);
  }

  public getContainer(token: ContainerToken): DataContainer {
    return this.container[token];
  }

  public getMatches(token: ContainerToken): Observable<Match[]> {
    return this.container[token].matches$.asObservable();
  }

  public getMatchWsPool(token: ContainerToken): WsPool<Match> {
    return this.container[token].matchWsPool;
  }

  public getMarkets(token: ContainerToken): Observable<Market[]> {
    return this.container[token].markets$.asObservable();
  }

  public getMarketWsPool(token: ContainerToken): WsPool<Market> {
    return this.container[token].marketWsPool;
  }

  public getMatchesStats(token: ContainerToken): Observable<MatchStats[]> {
    return this.container[token].matchesStats$.asObservable();
  }

  public getMatchStatsWsPool(token: ContainerToken): WsPool<MatchStats> {
    return this.container[token].matchStatsWsPool;
  }

  public getPrimaryColumnsConfigs(token: ContainerToken): Observable<PrimaryColumnConfig[]> {
    return this.container[token].primaryColumnsConfig$.asObservable();
  }

  public getRawMatchesHttp(token: ContainerToken): Observable<Match[]> {
    return this.container[token].rawMatchesHttp$.asObservable();
  }

  public getRawMarketsHttp(token: ContainerToken): Observable<Market[]> {
    return this.container[token].rawMarketsHttp$.asObservable();
  }
}