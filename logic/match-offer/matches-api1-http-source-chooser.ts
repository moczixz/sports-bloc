import { Provide } from "@logic/di-resolver/di-resolver";
import { MatchesContainerConfig } from "./interfaces/matches-container-config.interface";
import { Observable, forkJoin, of } from "rxjs";
import { Match } from "@logic/shared/models/match-offer/match";
import { Matches1Repository } from "@logic/core/repository/matches1.repository";

@Provide([Matches1Repository])
export class MatchesApi1HttpSourceChooser {

  constructor(
    private matchesRepository: Matches1Repository) {
  }

  public chooseHttpMatchesSource(config: MatchesContainerConfig): Observable<Match[]> {
    if (config.isTopFive) {
      return config.isLive ? 
        this.matchesRepository.getTopLive(config.limit) 
        : 
        of([])
    }
    return of([]);
  }
}