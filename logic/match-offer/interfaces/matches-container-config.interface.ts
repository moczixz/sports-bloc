import { MarketsHttpSourceType } from '../enums/markets-http-source-type.enum';
import * as moment from 'moment';

export interface MatchesContainerConfig {
  disableLoadPrimaryMarkets?: boolean;
  disableLoadPrimaryColumns?: boolean; // we dont need primary columns on single match view
  isLive?: boolean;
  isTopFive?: boolean;
  limit?: number; // limit for standard http request and to websocket, to limit new matches
  singleViewDisciplineFromUrl?: boolean;
  isTopPreLiveCarousel?: boolean; // set for top pre live carousel to get matches top pre and matches top live
  topPreLiveCarouselPreLimit?: number; // limit fetch for topPreLiveCarouse pre matches
  topPreLiveCarouselLiveLimit?: number;// limit fetch for topPreLiveCarousel live matches
  bypassMatchStateLiveFilter?: boolean; // if you want matches either live and pre, for top pre carousel
  promoMatchesLimit?: number;
  selectedLeaguesIds?: number[];
  selectedMatchesIds?: string[];
  singleMatchId?: string; // set this if you want to fetch one match, for example single match view
  marketsHttpSourceType?: MarketsHttpSourceType; // by default we fetch primaryMarkets
  matchesDateTo?: moment.Moment; // use it prematches-list, with timefilters
  allowMarketsWithoutPrimaryColumnId?: boolean; // match details view should only show markets without PC
}

export interface LiveCalendarContainerConfig extends MatchesContainerConfig {
  paginationLimit: number;
  daysForward: number;
  dateFrom: moment.Moment;
}
