import { DataContainer } from "./data-container.interface";

export interface DataContainerToken {
  [token: string]: DataContainer;
}
