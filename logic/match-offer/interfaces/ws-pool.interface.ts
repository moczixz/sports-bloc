import { BehaviorSubject } from "rxjs";

export interface WsPool<T> {
  [objectId: string]: BehaviorSubject<T>;
}