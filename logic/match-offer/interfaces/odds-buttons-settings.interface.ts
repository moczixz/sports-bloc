import { OddsButtonsType } from "../enums/odds-buttons-type.enum";

export interface OddsButtonsSettings {
  isOnlyRate?: boolean; // show only rate inside button
  isPrimaryColumnConfigInside?: boolean; // show primary column marker inside button
  isSelectionNameInside?: boolean; // show selection name inside button
  type?: OddsButtonsType;
  selectFirstMarket?: boolean;
}
