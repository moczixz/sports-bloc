import { WsPool } from "./ws-pool.interface";
import { BehaviorSubject, Subject } from "rxjs";
import { Match } from "@logic/shared/models/match-offer/match";
import { Market } from "@logic/shared/models/match-offer/market";
import { MatchStats } from "@logic/shared/models/match-offer/match-stats";
import { PrimaryColumnConfig } from "@logic/shared/models/match-offer/primary-column-config";

export interface DataContainer {
  matchesHttpLoadError$: Subject<void>,
  matchWsPool: WsPool<Match>,
  matches$: BehaviorSubject<Match[]>,
  marketWsPool: WsPool<Market>,
  markets$: BehaviorSubject<Market[]>,
  matchStatsWsPool: WsPool<MatchStats>,
  matchesStats$: BehaviorSubject<MatchStats[]>,
  primaryColumnsConfig$: BehaviorSubject<PrimaryColumnConfig[]>,
  rawMatchesHttp$: BehaviorSubject<Match[]>,
  rawMarketsHttp$: BehaviorSubject<Market[]>,
}