export interface DataObjectContainer<T> {
  [objectId: string]: T;
}