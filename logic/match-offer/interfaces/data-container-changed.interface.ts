import { DataObjectContainer } from "./data-object-container.interface";

export interface DataContainerChanged<T> {
  changed: boolean;
  data: DataObjectContainer<T>;
}