import { SourceType } from "../enums/source-type.enum";

export interface WithSource<T> {
  payload: T,
  source: SourceType;
}