import { Provide } from "@logic/di-resolver/di-resolver";
import { Market } from "@logic/shared/models/match-offer/market";
import { MatchesSharedService } from "./matches-shared.service";
import { ContainerToken } from "./enums/container-token.enum";
import { BehaviorSubject, of, Observable, UnaryFunction, pipe, merge, from, OperatorFunction, GroupedObservable, iif, combineLatest } from "rxjs";
import { PrimaryColumnConfig } from "@logic/shared/models/match-offer/primary-column-config";
import { ObjectAsId } from "@logic/shared/interfaces/object-as-id.interface";
import { SportsGroup } from "@logic/shared/models/match-offer/sports-group";
import { Match } from "@logic/shared/models/match-offer/match";
import { distinctUntilChanged, switchMap, scan, startWith, auditTime, filter, tap, mergeMap, map, reduce, take, groupBy, toArray, takeWhile } from "rxjs/operators";
import { MatchStats } from "@logic/shared/models/match-offer/match-stats";
import { filterEmptyArray, mappedArrayExtractProperty, mappedArrayOnlyUniqueByKey, mappedArraySort, mappedArrayFilter, simpleSortByAsc } from "@logic/shared/helpers/rxjs-helpers";
import { filterDuplicate, createArrayByLength } from "@logic/shared/helpers/pure-utils";

export interface MarketsBelongsToMatch {
  [matchId: string]: Market[];
}

@Provide([MatchesSharedService])
export class MatchesDataSelector {
  private token: ContainerToken;
  
  public activeDiscipline$:  BehaviorSubject<SportsGroup> = new BehaviorSubject<SportsGroup>(null);
  private primaryColumns$: BehaviorSubject<ObjectAsId<PrimaryColumnConfig>> = new BehaviorSubject<ObjectAsId<PrimaryColumnConfig>>(null);

  constructor(private matchesSharedService: MatchesSharedService) {
  }

  public setToken(token: ContainerToken): void {
    this.token = token;
  }

  public isDataLoaded(): Observable<boolean> {
    return combineLatest(
      this.matchesSharedService.getRawMatchesHttp(this.token), 
      this.matchesSharedService.getRawMarketsHttp(this.token)
    )
      .pipe(
        map(([matches, markets]: [Match[], Market[]]) =>
          Array.isArray(matches) && Array.isArray(markets)
        ),
        takeWhile((state: boolean) => state === false, true),
      );
  }

  public isMatchesNotFound(): Observable<void> {
    return this.matchesSharedService.getContainer(this.token).matchesHttpLoadError$.asObservable();
  }

  public getMatchSocket(match: Match): Observable<Match> {
    return this.matchesSharedService.getMatchWsPool(this.token)[match.id]
      .pipe(
        distinctUntilChanged((previous: Match, current: Match) => 
          previous.state.isSuspended === current.state.isSuspended
            && previous.state.isVisible === current.state.isVisible
        )
      );
  }

  public getMarketSocket(market: Market): Observable<Market> {
    return this.matchesSharedService.getMarketWsPool(this.token)[market.id];
  }

  public getMarketSocketWatchedState(market: Market): Observable<Market> {
    return this.getMarketSocket(market)
    .pipe(
      distinctUntilChanged((previous: Market, current: Market) => 
        previous.state.isVisible === current.state.isVisible
          && previous.state.isSuspended === current.state.isSuspended
      )
    );
  }

  public selectMarketSocketForMarkets(): UnaryFunction<Observable<Market[]>, Observable<ObjectAsId<Market>>> {
    return pipe(
      switchMap((markets: Market[]) => 
        merge(...markets.map((market: Market) => this.getMarketSocket(market)))
      ),
      scan((marketsContainer: ObjectAsId<Market>, market: Market) => ({
        ...marketsContainer,
        [market.id]: market
      }), {}),
      startWith({}),
      auditTime(0)
    );
  }

  public getMatchStatsSocket(match: Match): Observable<MatchStats> {
    return this.matchesSharedService.getMatchStatsWsPool(this.token)[match.id]
      .pipe(
        filter(Boolean),
        tap((matchStats: MatchStats) => 
          matchStats.setDisciplineLabel(match.discipline.label)
        )
      );
  }

  public getFirstMatchSocket(): Observable<Match> {
    return this.getFirstMatch()
      .pipe(
        switchMap((match: Match) => this.getMatchSocket(match))
      );
  }

  public getFirstMatchStatsSocket(): Observable<MatchStats> {
    return this.getFirstMatch()
      .pipe(
        switchMap((match: Match) => this.getMatchStatsSocket(match))
      );
  }

  public getMatchLink(match: Match): Observable<string> {
    return of('');
  }

  public getMatchesLinks(): Observable<any> {
    return this.getMatches()
      .pipe(
        filterEmptyArray(),
        switchMap((matches: Match[]) => 
          from(matches)
            .pipe(
              mergeMap((match: Match) => 
                this.getMatchLink(match)
                  .pipe(
                    map((link: string) => ({[match.id]: link}))
                  )
              ),
              reduce((acc: any, curr: any) => ({...acc, ...curr}), {})
            )
        ),
        startWith({})
      );
  }

  public getPrimaryMarkets(): Observable<Market[]> {
    // alias method for getMarkets;
    return this.matchesSharedService.getMarkets(this.token);
  }

  public getPrimaryMarketsOnlyWithSelections(): Observable<Market[]> {
    return this.matchesSharedService.getMarkets(this.token)
      .pipe(
        map((markets: Market[]) => 
          markets.filter((market: Market) => market.selections.length > 0)
        )
      );
  }

  public getMarkets(): Observable<Market[]> {
    // this return all markets, remember we subscribe to sports_groups to get new matches
    // it may happen then we add matches we dont want
    return this.matchesSharedService.getMarkets(this.token)
      .pipe(filter(Boolean))
  }

  public selectMarketsForMatch(): OperatorFunction<Match, Market[]> {
    return switchMap((match: Match) => 
      this.getMarkets()
        .pipe(
          map((markets: Market[]) => 
            markets.filter((market: Market) => market.matchId === match.id)
          )
        )
    )
  }

  public getUniqueDisciplines(setDefaultActiveDiscipline: boolean = false): Observable<SportsGroup[]> {
    return this.matchesSharedService.getMatches(this.token)
      .pipe(
        mappedArrayExtractProperty('discipline'),
        mappedArrayOnlyUniqueByKey<any>('id'),
        tap((disciplines: SportsGroup[]) => 
          setDefaultActiveDiscipline ? 
            this.setDefaultActiveDiscipline(disciplines) 
            : 
            null
        )
      );
  }

  public getUniqueDisciplinesSortedByOrder(setDefaultActiveDiscipline: boolean = false): Observable<SportsGroup[]> {
    return this.matchesSharedService.getMatches(this.token)
    .pipe(
      filterEmptyArray(),
      mappedArrayExtractProperty('discipline'),
      mappedArrayOnlyUniqueByKey<any>('id'),
      mappedArraySort((disciplineA: SportsGroup, disciplineB: SportsGroup) => 
        disciplineA.order - disciplineB.order
      ),
      tap((disciplines: SportsGroup[]) => 
        setDefaultActiveDiscipline ? 
          this.setDefaultActiveDiscipline(disciplines) 
          : 
          null
      )
    );
  }

  public getScannedDisciplines(): Observable<SportsGroup[]> {
    return this.matchesSharedService.getMatches(this.token)
      .pipe(
        mappedArrayExtractProperty('discipline'),
        scan((accumulatedDisciplines: SportsGroup[], currentDisciplines: SportsGroup[]) => [
          ...accumulatedDisciplines, ...currentDisciplines
        ], []),
        mappedArrayOnlyUniqueByKey('id')
      );
  }

  public getScannedRegionsByLabel(): Observable<SportsGroup[]> {
    return this.matchesSharedService.getMatches(this.token)
      .pipe(
        mappedArrayExtractProperty('region'),
        scan((accumulatedRegions: SportsGroup[], currentRegions: SportsGroup[]) => [
          ...accumulatedRegions, ...currentRegions
        ], []),
        mappedArrayOnlyUniqueByKey('label')
      );
  }

  public getMatches(): Observable<Match[]> {
    return this.matchesSharedService.getMatches(this.token);
  }

  public getMatchesSortedByGroupOrder(): Observable<Match[]> {
    return this.getMatches()
      .pipe(
        map(this.sortMatchesBySportsGroupOrder)
      );
  }

  public getFirstMatch(): Observable<Match> {
    return this.matchesSharedService.getMatches(this.token)
      .pipe(
        filterEmptyArray(),
        map((matches: Match[]) => matches[0])
      );
  }

  public getUniqueDisciplinesWithTop(): Observable<SportsGroup[]> {
    return this.getUniqueDisciplines()
      .pipe(
        map((disciplines: SportsGroup[]) => 
          [{ id: -1 } as SportsGroup, ...disciplines]
        ),
        tap((disciplines: SportsGroup[]) => this.setDefaultActiveDiscipline(disciplines))
      );
  }

  public getUniqueDisciplinesSortedByOrderWithTop(): Observable<SportsGroup[]> {
    return this.getUniqueDisciplinesSortedByOrder()
      .pipe(
        map((disciplines: SportsGroup[]) => 
          [{ id: -1 } as SportsGroup, ...disciplines]
        ),
        tap((disciplines: SportsGroup[]) => this.setDefaultActiveDiscipline(disciplines))
      );
  }

  public getPreMatchesSortedByRank(): Observable<Match[]> {
    return this.getMatches()
      .pipe(
        filterEmptyArray(),
        mappedArrayFilter((match: Match) => match.state.isPre),
        mappedArraySort((matchA: Match, matchB: Match) => matchA.rank - matchB.rank)
      );
  }

  public getLiveMatchesSortedByRank(): Observable<Match[]> {
    return this.getMatches()
      .pipe(
        filterEmptyArray(),
        mappedArrayFilter((match: Match) => match.state.isLive),
        mappedArraySort((matchA: Match, matchB: Match) => matchA.rank - matchB.rank)
      );
  }

  public getPreMatchesSortedByRankOnlyWithParticipants(): Observable<Match[]> {
    return this.getPreMatchesSortedByRank()
      .pipe(
        mappedArrayFilter((match: Match) => Boolean(match.homePlayer))
      );
  }

  private setDefaultActiveDiscipline(disciplines: SportsGroup[]): void {
    this.activeDiscipline$
      .pipe(
        take(1),
        filter((discipline: SportsGroup) => discipline === null)
      )
      .subscribe(() => this.activeDiscipline$.next(disciplines[0]));
  }

  public getPrimaryMarketsGroupByMatch(): Observable<MarketsBelongsToMatch> {
    return this.getMarkets()
      .pipe(   
        switchMap((markets: Market[]) => 
          from(markets)
            .pipe(
              groupBy((market: Market) => market.matchId, (market: Market) => market),
              mergeMap((group$: GroupedObservable<string, Market>) => 
                group$.pipe(
                  reduce((acc: Array<string | Market>, cur: Market) => 
                    [...acc, cur], [group$.key]
                  )
                )  
              ),
              map((arr: Array<string | Market>) => ({ [arr[0] as string]: arr.slice(1) })),
              reduce((acc: any, cur: any) => ({ ...acc, ...cur }), {})
            )
        ),
        startWith({})
      );
  }

  public getMatchesGroupedByDisciplines(): Observable<Match[][]> {
    return this.getMatches()
      .pipe(
        switchMap((matches: Match[]) =>
          from(matches)
            .pipe(
              groupBy((match: Match) => match.discipline.id),
              mergeMap((group$: GroupedObservable<number, Match>) => 
                group$.pipe(
                  reduce((acc: Match[], cur: Match) => [...acc, cur], [])
                )
              ),
              toArray()
            )
        )
      );
  }

  public getMatchesGroupedByDisciplinesAndSortedByGroupOrder(): Observable<Match[][]> {
    return this.getMatchesGroupedByDisciplines()
      .pipe(
        map((matchesGroup: Match[][]) => // sort disciplines by order
          matchesGroup.slice().sort((disciplineGroupA: Match[], disciplineGroupB: Match[]) =>
            disciplineGroupA[0].discipline.order - disciplineGroupB[0].discipline.order
          )
        ),
        map((matchesGroup: Match[][]) => // sort matches in discipline, by match discipline order
          matchesGroup.map(this.sortMatchesBySportsGroupOrder)
        )
      );
  }

  private sortMatchesBySportsGroupOrder(matches: Match[]): Match[]  {
    return matches.slice().sort((matchA: Match, matchB: Match) => {
      if (matchA.discipline.order !== matchB.discipline.order) {
        return matchA.discipline.order - matchB.discipline.order;
      }
      else if (matchA.region.order !== matchB.region.order) {
        return matchA.region.order - matchB.region.order;
      }
      else {
        return matchA.league.order - matchB.league.order;
      }
    });
  }
/*
  public getMatchesGroupedByLeagueAndSortedByDate(): Observable<Match[][][]> {
    return this.getMatchesSortedByDateAsc()
      .pipe(
        switchMap((matches: Match[]) =>
          from(matches)
            .pipe(     
              groupBy((match: Match) => moment(new Date(match.date)).format('YYYY-MM-DD')),
              mergeMap((groupDate$: GroupedObservable<string, MatchApi2>) => 
                groupDate$
                  .pipe(
                    groupBy((match: MatchApi2) => match.sportsGroups[0].id),
                    mergeMap((groupLeague$: GroupedObservable<number, MatchApi2>) =>
                      groupLeague$.pipe(
                        reduce((acc: MatchApi2[], cur: MatchApi2) => [...acc, cur], [])
                      )
                    ),
                    reduce((acc: MatchApi2[][], cur: MatchApi2[]) => [...acc, cur], [])
                  )
              ),
              toArray()
            )
        )
      );
  }

  public getMatchesGroupedByLeagueAndSortedByDateAndSortedByGroupOrder(): Observable<Match[][][]> {
    return this.getMatchesGroupedByLeagueAndSortedByDate()
      .pipe(
        map((matchesDateGroup: MatchApi2[][][]) => 
          matchesDateGroup.map((matchesLeagueGroup: MatchApi2[][]) =>
            matchesLeagueGroup.map(this.sortMatchesBySportsGroupOrder)
          )
        )
      );
  }
*/
  // tslint:disable-next-line:max-line-length
  public selectPrimaryMarketsLengthForGroupedByLeagueAndSortedByDate(): UnaryFunction<Observable<Match[][][]>, Observable<number[][][]>> {
    return pipe(
      switchMap((groupedMatches: Match[][][]) => 
        this.getPrimaryMarkets()
          .pipe(
            map((markets: Market[]) => 
              groupedMatches.map((dateGroup: Match[][]) =>
                dateGroup.map((leagueGroup: Match[]) => 
                  this.extractUniqueMarketsAsColumnArray(leagueGroup, markets)
                )
              )
            )
          )
      )
    );
  }

  public selectPrimaryMarketsLengthForGroupedByDiscipline(): UnaryFunction<Observable<Match[][]>, Observable<number[][]>> {
    return pipe(
      switchMap((groupedMatches: Match[][]) => 
        this.getPrimaryMarkets()
          .pipe(
            map((markets: Market[]) =>
              groupedMatches.map((leagueGroup: Match[]) => 
                this.extractUniqueMarketsAsColumnArray(leagueGroup, markets)
              )
            )
          )
      )
    );
  }

  public selectPrimaryMarketsLengthForMatches(): any {
    return pipe(
      switchMap((matches: Match[]) => 
        this.getPrimaryMarkets()
          .pipe(
            map((markets: Market[]) => 
              this.extractUniqueMarketsAsColumnArray(matches, markets)
            )
          )
      )
    );
  }

  private extractUniqueMarketsAsColumnArray(matches: Match[], markets: Market[]): number[] {
    const marketsForMatches: Market[] = markets.filter((market: Market) => 
      matches.some((match: Match) => market.matchId === match.id)
    );
    const marketsUnique: Market[] = marketsForMatches.filter(filterDuplicate('primaryColumn'));
    return createArrayByLength(marketsUnique.length).slice(0, 3);
  }

  public getMatchesSortedByDateAsc(): Observable<Match[]> {
    return this.getMatches()
      .pipe(
        map((matches: Match[]) =>
          matches.slice().sort((matchA: Match, matchB: Match) => 
            new Date(matchA.date).getTime() - new Date(matchB.date).getTime()
          )
        )
      );
  }

  public getMatchesFromDiscipline(disciplineId: number): Observable<Match[]> {
    return this.getMatches()
      .pipe(
        simpleSortByAsc('rank'),
        mappedArrayFilter((match: Match) => match.discipline.id === disciplineId)
      );
  }

  public getMatchesForActiveDiscipline(): Observable<Match[]> {
    return this.activeDiscipline$
      .pipe(
        filter(Boolean),
        switchMap((discipline: SportsGroup) => this.getMatchesFromDiscipline(discipline.id))
      );
  }

  public getMatchesForActiveDisciplineSortedByRankDesc(): Observable<Match[]> {
    return this.getMatchesForActiveDiscipline()
      .pipe(
        map((matches: Match[]) =>
          matches.sort((matchA: Match, matchB: Match) => matchB.rank - matchA.rank)
        )
      )
  }

  public getMatchesFromTopGroup(): Observable<Match[]> {
    return this.getMatches()
      .pipe(
        simpleSortByAsc('rank'),
        mappedArrayFilter((match: Match) => match.isTopGroup),
      );
  }

  public getMatchesForActiveDisciplineTop(): Observable<Match[]> {
    return this.activeDiscipline$
      .pipe(
        filter(Boolean),
        switchMap((discipline: SportsGroup) => 
          iif(
            () => discipline.id !== -1,
            this.getMatchesFromDiscipline(discipline.id),
            this.getMatchesFromTopGroup()
          )
        )
      );
  }

  public setPrimaryColumn(identifier: string, primaryColumn: number): void {
    this.matchesSharedService.getPrimaryColumnsConfigs[this.token]
      .pipe(take(1))
      .subscribe((configs: PrimaryColumnConfig[]) => {
        this.primaryColumns$.next({
          [identifier]: configs.find((config: PrimaryColumnConfig) => config.id === primaryColumn)
        });
      });
  }

  public getPrimaryColumn(): Observable<ObjectAsId<PrimaryColumnConfig>>  {
    return this.primaryColumns$
      .pipe(
        scan((acc: ObjectAsId<PrimaryColumnConfig>, curr: ObjectAsId<PrimaryColumnConfig>) => ({...acc, ...curr}), {}),
        auditTime(0) // fix to not trigger detectChanges multipletimes when we change disciplineTab and got new defaultprimary column at the same moment
      );
  }

  public selectDefaultPrimaryColumnConfig(): UnaryFunction<Observable<Match[]>, Observable<PrimaryColumnConfig>> {
    return pipe(
      filterEmptyArray(),
      switchMap((matches: Match[]) =>
        this.getPrimaryMarkets()
          .pipe(
            filterEmptyArray(),
            map((primaryMarkets: Market[]) => {
              const foundMarkets: Market[] = primaryMarkets.filter((market: Market) => 
                matches.some((match: Match) => market.matchId === match.id)
              );
              const sortedByPosition: Market[] = foundMarkets.sort((marketA: Market, marketB: Market) => 
                marketA.position - marketB.position
              );
              return sortedByPosition.length > 0 ? sortedByPosition[0].primaryColumn : null;
            })
          )
      ),
      filter(Boolean),
      switchMap((primaryColumn: number) => 
        this.matchesSharedService.getPrimaryColumnsConfigs(this.token)
          .pipe(
            filterEmptyArray(),
            map((configs: PrimaryColumnConfig[]) => 
              configs.find((config: PrimaryColumnConfig) => config.id === primaryColumn)
            ),
            filter(Boolean)
          )
      )
    );
  }

}
