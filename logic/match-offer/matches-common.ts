import { Subject, Observable, BehaviorSubject } from "rxjs";
import { MatchesContainerConfig } from "./interfaces/matches-container-config.interface";
import { Match } from "@logic/shared/models/match-offer/match";
import { Market } from "@logic/shared/models/match-offer/market";
import { ContainerToken } from "./enums/container-token.enum";

export class MatchesCommon {
  private token: ContainerToken;
  private config: MatchesContainerConfig;
  private restart$: Subject<void> = new Subject<void>();

  public newMatch$: Subject<Match> = new Subject<Match>();
  public newMarket$: Subject<Market> = new Subject<Market>();


  public newMarketApi1$: Subject<PrimaryMarket> = new Subject<PrimaryMarket>();
  public updateMarketApi1$: Subject<PrimaryMarket> = new Subject<PrimaryMarket>();
  public newStatApi1$: Subject<any> = new Subject<any>();
  public updateStatApi1$: Subject<any> = new Subject<any>();

  public setConfig(config: MatchesContainerConfig): void {
    this.config = config;
  }

  public setToken(token: ContainerToken): void {
    this.token = token;
  }

  public getToken(): ContainerToken {
    return this.token;
  }

  public getConfig(): MatchesContainerConfig {
    return this.config;
  }

  public restart(): void {
    this.restart$.next(null);
  }

  public onRestart(): Observable<void> {
    return this.restart$.asObservable();
  }
}