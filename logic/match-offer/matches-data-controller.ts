import { Provide } from "@logic/di-resolver/di-resolver";
import { MatchesHttpSourceChooser } from "./matches-http-source-chooser";
import { MatchesContainerConfig } from "./interfaces/matches-container-config.interface";
import { MatchesApi2Loader } from "./loaders/matches-api2-loader";
import { MatchesCommon } from "./matches-common";
import { MarketsApi2Loader } from "./loaders/markets-api2-loader";
import { PrimaryColumnConfigApi2Loader } from "./loaders/primary-column-config-api2-loader";
import { MatchesStatsApi2Loader } from "./loaders/matches-stats-api2-loader";
import { Disposable } from "@logic/shared/interfaces/disposable.interface";
import { MatchesSharedService } from "./matches-shared.service";
import { ContainerToken } from "./enums/container-token.enum";
import { Environment } from "@logic/shared/environment";
import { MatchesApi1Loader } from "./loaders/matches-api1-loader";
import { MatchesApi1HttpSourceChooser } from "./matches-api1-http-source-chooser";
import { MarketsApi1Loader } from "./loaders/markets-api1.loader";
import { MatchesStatsApi1Loader } from "./loaders/matches-stats-api1-loader";

@Provide([
  Environment,
  MatchesSharedService,
  MatchesHttpSourceChooser, 
  MatchesApi2Loader, 
  MarketsApi2Loader, 
  PrimaryColumnConfigApi2Loader, 
  MatchesStatsApi2Loader,
  MatchesApi1HttpSourceChooser,
  MatchesApi1Loader,
  MarketsApi1Loader,
  MatchesStatsApi1Loader
])
export class MatchesDataController implements Disposable {
  private common: MatchesCommon = new MatchesCommon();
  private token: ContainerToken;

  constructor(
    private env: Environment,
    private matchesSharedService: MatchesSharedService,
    private httpSourceChooser: MatchesHttpSourceChooser, 
    private matchesLoader: MatchesApi2Loader,
    private marketsLoader: MarketsApi2Loader,
    private primaryColumnConfigApi2Loader: PrimaryColumnConfigApi2Loader,
    private matchesStatsApi2Loader: MatchesStatsApi2Loader,

    private matchesApi1HttpSourceChooser: MatchesApi1HttpSourceChooser,
    private matches1Loader: MatchesApi1Loader,
    private markets1Loader: MarketsApi1Loader,
    private matchesStats1Loader: MatchesStatsApi1Loader) {
  }

  public init(token: ContainerToken): void {
    this.token = token;
    this.matchesSharedService.cleanContainer(token);
    this.common.setToken(token);


    this.env.api2Enabled ? this.runApi2() : this.runApi1();

  }

  private runApi2(): void {
    this.matchesLoader.setCommon(this.common);
    this.marketsLoader.setCommon(this.common);
    this.matchesStatsApi2Loader.setCommon(this.common);
    this.primaryColumnConfigApi2Loader.setCommon(this.common);
    
    this.matchesStatsApi2Loader.loadMatchStats();
    this.primaryColumnConfigApi2Loader.loadPrimaryColumns();
    this.marketsLoader.loadMarkets();
    this.matchesLoader.loadMatches();
  }

  private runApi1(): void {
    this.matches1Loader.setCommon(this.common);
    this.markets1Loader.setCommon(this.common);
    this.matchesStats1Loader.setCommon(this.common);

    this.matchesStats1Loader.loadMatchesStats();
    this.markets1Loader.loadMarkets();
    this.matches1Loader.loadMatches();
  }

  public loadForConfig(config: MatchesContainerConfig): void {
    this.common.setConfig(config);

    this.env.api2Enabled ?
      this.matchesLoader.setHttpSource(this.httpSourceChooser.chooseHttpMatchesSource(config))
      :
      this.matches1Loader.setHttpSource(this.matchesApi1HttpSourceChooser.chooseHttpMatchesSource(config));
      
    this.common.restart();
  }


  public dispose(): void {
    this.marketsLoader.dispose();
    this.matchesLoader.dispose();
    this.primaryColumnConfigApi2Loader.dispose();
    this.matchesStatsApi2Loader.dispose();
    this.matchesSharedService.cleanContainer(this.token);
  }
}