import { LazySingleton } from "@logic/di-resolver/di-resolver";
import { BehaviorSubject, Observable } from "rxjs";

@LazySingleton([])
export class BetslipSelectionsService {

  private activeSelections$: BehaviorSubject<string[]> = new BehaviorSubject<string[]>([]);

 
  public getActiveSelections(): Observable<string[]> {
    return this.activeSelections$.asObservable();
  }

  public toggleSelectionFromApi2(toggleSelection: any): void {

  }


}