import { TopLiveBloc } from './bloc/top-live/top-live.bloc';
import { AuthBarBloc } from './bloc/auth-bar/auth-bar.bloc';
import { DiResolver } from './di-resolver/di-resolver';
import { EnvironmentContract } from './shared/interfaces/environment-contract.interface';
import { Environment } from './shared/environment';
import { MatchRowBloc } from './bloc/match-row/match-row.bloc';
import { DisciplinesSelectBloc } from './bloc/disciplines-select/disciplines-select.bloc';
import { OddsButtonsBloc } from './bloc/odds-buttons/odds-buttons.bloc';

export function getTopLiveBloc(): TopLiveBloc {
  return DiResolver.resolve(TopLiveBloc);
}

export function getAuthBloc(): AuthBarBloc {
  return DiResolver.resolve(AuthBarBloc);
}

export function getMatchRowBloc(): MatchRowBloc {
  return DiResolver.resolve(MatchRowBloc);
}

export function getDisciplinesSelectBloc(): DisciplinesSelectBloc {
  return DiResolver.resolve(DisciplinesSelectBloc);
}

export function getOddsButtonsBloc(): OddsButtonsBloc {
  return DiResolver.resolve(OddsButtonsBloc);
}

export function registerEnvironment(environment: EnvironmentContract) {
  DiResolver.registerClassAsSingleton(new Environment(environment));
}
