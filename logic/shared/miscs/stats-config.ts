/**
 * maxNormalPeriod - max index of normal time period
 * overTimePeriods - count of over time periods
 * finishPeriod - number of finish period for stats.eventType != period
 * periodFinishPeriod - number of finish period for stats.eventType == period
 * periodOverTimeSecondEndPeriod - number of 2nd over time end period for stats.eventType == period
 */

export interface StatsConfigObject {
  [disciplineLabel: string]: {
    maxNormalPeriod?: number;
    finishPeriod?: number;
    overTimePeriods?: number;
    periodFinishPeriod?: number;
    periodOverTimeSecondEndPeriod?: number;
    sportKind?: number,
  };
}

export const StatsConfig: StatsConfigObject = {
  'football': {
    maxNormalPeriod: 2,
    finishPeriod: 5,
    overTimePeriods: 2,
    periodFinishPeriod: 9,
    periodOverTimeSecondEndPeriod: 7
  },
  'tennis': {
    maxNormalPeriod: 5,
    finishPeriod: 6,
    overTimePeriods: 0,
    periodFinishPeriod: 11
  },
  'volleyball': {
    sportKind: 5,
    maxNormalPeriod: 5,
    finishPeriod: 6,
    overTimePeriods: 0,
    periodFinishPeriod: 11
  },
  'ice-hockey': {
    maxNormalPeriod: 3,
    finishPeriod: 5,
    overTimePeriods: 0,
    periodFinishPeriod: 9,
    periodOverTimeSecondEndPeriod: 7
  },
  'basketball': {
    sportKind: 3,
    maxNormalPeriod: 4,
    finishPeriod: 6,
    overTimePeriods: 0,
    periodFinishPeriod: 11
  },
  'american-football': {
    maxNormalPeriod: 4,
    finishPeriod: 6,
    overTimePeriods: 0,
    periodFinishPeriod: 10
  },
  'badminton': {
    maxNormalPeriod: 3,
    finishPeriod: 4,
    overTimePeriods: 0,
    periodFinishPeriod: 7
  },
  'baseball': {
    maxNormalPeriod: 9,
    finishPeriod: 10,
    overTimePeriods: 0,
    periodFinishPeriod: 19
  },
  'cricket': {
    maxNormalPeriod: 4,
    finishPeriod: 5,
    overTimePeriods: 0,
    periodFinishPeriod: 9
  },
  'beach-volleyball': {
    maxNormalPeriod: 3,
    finishPeriod: 4,
    overTimePeriods: 0,
    periodFinishPeriod: 7
  },
  'darts': {
    maxNormalPeriod: 9,
    finishPeriod: 10,
    overTimePeriods: 0,
    periodFinishPeriod: 19
  },
  'handball': {
    maxNormalPeriod: 2,
    finishPeriod: 4,
    overTimePeriods: 0,
    periodFinishPeriod: 8
  },
  'rugby-league': {
    maxNormalPeriod: 2,
    finishPeriod: 3,
    overTimePeriods: 0,
    periodFinishPeriod: 9
  },
  'rugby-union': {
    maxNormalPeriod: 2,
    finishPeriod: 3,
    overTimePeriods: 0,
    periodFinishPeriod: 9
  },
  'snooker': {
    maxNormalPeriod: 7,
    finishPeriod: 8,
    overTimePeriods: 0,
    periodFinishPeriod: 15
  },
  'table-tennis': {
    maxNormalPeriod: 7,
    finishPeriod: 8,
    overTimePeriods: 0,
    periodFinishPeriod: 15
  },
  'water-polo': {
    maxNormalPeriod: 4,
    finishPeriod: 6,
    overTimePeriods: 0,
    periodFinishPeriod: 11
  },
  'e-football': {
    maxNormalPeriod: 2,
    finishPeriod: 5,
    overTimePeriods: 0,
    periodFinishPeriod: 9
  },
  'e-basketball': {
    maxNormalPeriod: 4,
    finishPeriod: 6,
    overTimePeriods: 0,
    periodFinishPeriod: 11
  },
  'boxing': {
    maxNormalPeriod: 12,
    periodFinishPeriod: 13
  },
  'counter-strike-go': {
    maxNormalPeriod: 5,
    finishPeriod: 7,
    overTimePeriods: 0,
    periodFinishPeriod: 13
  },
  'league-of-legends': {
    maxNormalPeriod: 5,
    finishPeriod: 7,
    overTimePeriods: 0,
    periodFinishPeriod: 13
  },
  'dota-2': {
    maxNormalPeriod: 5,
    finishPeriod: 7,
    overTimePeriods: 0,
    periodFinishPeriod: 13
  },
  'starcraft-2': {
    maxNormalPeriod: 5,
    finishPeriod: 7,
    overTimePeriods: 0,
    periodFinishPeriod: 13
  },
  'heroes-of-the-storm': {
    maxNormalPeriod: 7,
    finishPeriod: 9,
    overTimePeriods: 0,
    periodFinishPeriod: 15
  },
  'overwatch': {
    maxNormalPeriod: 7,
    finishPeriod: 9,
    overTimePeriods: 0,
    periodFinishPeriod: 15
  }
}