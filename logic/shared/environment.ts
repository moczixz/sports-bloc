import { EnvironmentContract } from "./interfaces/environment-contract.interface";

export class Environment implements EnvironmentContract {
  public sportsBookApi1: string;
  public sportsBookWs1: string;
  public api2Enabled: boolean;
  public sportsBookApi2: string;
  public sportsBookWs2: string;
  public production: boolean;

  constructor(data: EnvironmentContract) {
    Object.keys(data).forEach(key => this[key] = data[key]);
  }
}