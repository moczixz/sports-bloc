import { StatsOld } from "./stats-old";
import { Helper } from "@logic/shared/helpers/helper";


export class MatchStats {

  public disciplineLabel: string;

  public extraInfo: string = '';

  constructor(
    public id: number,
    public matchId: string,
    public additionalMinutes: number,
    public cornerScore: string,
    public currentMinute: string,
    public dangerousAttackScore: string,
    public freeKickScore: string,
    public info: string,
    public matchLength: number,
    public penaltyScore: string,
    public period: number,
    public redcardScore: string,
    public score: string,
    public server: number,
    public set1CornerScore: string,
    public set1RedCardScore: string,
    public set1Score: string,
    public set1YellowCardScore: string,
    public set2CornerScore: string,
    public set2RedCardScore: string,
    public set2Score: string,
    public set2YellowCardScore: string,
    public set3Score: string,
    public set4Score: string,
    public set5Score: string,
    public shotOffTargetScore: string,
    public shotOnTargetScore: string,
    public sportKind: number,
    public yellowCardScore: string,
    public eventType: string,
    public remainingTime: string,
    public extraTimeScore: string,
    public gameScore: string,
    public setCount: number
    ) { }

  public static fromJsonApi2(data: any): MatchStats {
    return new MatchStats(
      data['match_statistics_id'],
      data['match_id'],
      data['additional_minutes'],
      data['corner_score'],
      data['current_minute'],
      data['dangerous_attack_score'],
      data['free_kick_score'],
      data['info'],
      data['match_length'],
      data['penalty_score'],
      data['period'],
      data['redcard_score'],
      data['score'],
      data['server'],
      data['set1_corner_score'],
      data['set1_red_card_score'],
      data['set1_score'],
      data['set1_yellow_card_score'],
      data['set2_corner_score'],
      data['set2_red_card_score'],
      data['set2_score'],
      data['set2_yellow_card_score'],
      data['set3_score'],
      data['set4_score'],
      data['set5_score'],
      data['shot_off_target_score'],
      data['shot_on_target_score'],
      data['sport_kind'],
      data['yellowcard_score'],
      data['event_type'],
      data['remaining_time'],
      data['extra_time_score'],
      data['game_score'],
      data['set_count']
    );
  }

  public static fromJsonApi1(data: any): MatchStats {
    return new MatchStats(
      data['id'],
      data['matchId'],
      data['additionalMinutes'],
      data['cornerScore'],
      data['currentMinute'],
      data['dangerousAttackScore'],
      data['freeKickScore'],
      data['info'],
      data['matchLength'],
      data['penaltyScore'],
      data['period'],
      data['redcardScore'],
      data['score'],
      data['server'],
      data['set1CornerScore'],
      data['set1RedCardScore'],
      data['set1Score'],
      data['set1YellowCardScore'],
      data['set2CornerScore'],
      data['set2RedCardScore'],
      data['set2Score'],
      data['set2YellowCardScore'],
      data['set3Score'],
      data['set4Score'],
      data['set5Score'],
      data['shotOffTargetScore'],
      data['shotOnTargetScore'],
      data['sportKind'],
      data['yellowcardScore'],
      data['eventType'],
      data['remainingTime'],
      data['extraTimeScore'],
      data['gameScore'],
      data['setCount']
    )
  }

  public static fromJsonApi2Array(data: any[]): MatchStats[] {
    return data.map(MatchStats.fromJsonApi2);
  }

  public setDisciplineLabel(disciplineLabel: string): void {
    this.disciplineLabel = disciplineLabel;
  }

  public get scoreHome(): string {
    return this.score ? this.score.split(':')[0] : '-';
  }

  public get scoreAway(): string {
    return this.score ? this.score.split(':')[1] : '-';
  }

  public get periodOrderNumeral(): number {
    if (this.disciplineLabel) {
      const periodPackage = StatsOld.createPeriodDependData(
        this.disciplineLabel, 
        this.eventType, 
        this.period, 
        this.score
      );
      this.extraInfo = periodPackage.extraInfo;
      return periodPackage.periodOrderNumeral;
    }
    return null;
  }

  private getField(field: string): string {
    if (!this[field]) {
      console.log('MISSING STATS FIELD!', field);
    }
    return this[field] ? this[field] : '-:-';
  }

  private getAvailableScoresFields(): string[] {
    return ['set1Score', 'set2Score', 'set3Score', 'set4Score', 'set5Score']
      .filter((field: string) => Boolean(this[field]));
  }

  public get footballStatsTableMatrix(): string[][] {
    return [0, 1].map((participantSide: number) => 
      ['yellowCardScore', 'redcardScore', 'cornerScore'].map((field: string) => 
        this.getField(field).split(':')[participantSide]
      )
    );
  }

  public get tennisStatsTableMatrix(): string[][] {
    return [0, 1].map((participantSide: number) => 
      ['gameScore', 'set1Score', 'set2Score', 'set3Score']
      .map((field: string) => 
        this.getField(field).split(':')[participantSide]
      )
    );
  }

  public get volleyballStatsTableMatrix(): string[][] {
    return [0, 1].map((participantSide: number) => 
      ['set1Score', 'set2Score', 'set3Score', 'set4Score', 'set5Score'].map((field: string) => 
        this.getField(field).split(':')[participantSide]
      )
    );
  }

  public get basketballStatsTableMatrix(): string[][] {
    return [0, 1].map((participantSide: number) => 
      ['set1Score', 'set2Score', 'set3Score', 'set4Score'].map((field: string) => 
        this.getField(field).split(':')[participantSide]
      )
    );
  }

  public get setCountStatsTableMatrix(): string[][] {
    if (this.setCount) {
      return [0, 1].map((participantSide: number) =>
        Helper.createfilledArray(this.setCount, 1).map((no: number) => `set${no}Score`).map((field: string) => 
          this.getField(field).split(':')[participantSide]
        )
      );
    }
    return [];
  }

  public get setCountStatsTableHead(): string[] {
    if (this.setCount) {
      return Helper.createfilledArray(this.setCount, 1).map((no: number) => no.toString());
    }
    return [];
  }

  public get availableStatsTableMatrix(): string[][] {
    return [0, 1].map((participantSide: number) =>
      this.getAvailableScoresFields().map((field: string) =>
        this.getField(field).split(':')[participantSide]
      )
    );
  }
  
  public get availableStatsTableHead(): string[] {
    return Helper.createfilledArray(this.getAvailableScoresFields().length, 1).map((no: number) => no.toString());
  }


}
