export class MarketGroup {
  constructor(
    public id: number,
    public expanded: boolean,
    public name: string,
    public showMarketNames: boolean,
    public position: number
  ){ }

  public static fromJsonApi2(data: any): MarketGroup {
    return new MarketGroup(
      data['markets_group_id'], 
      data['expanded'],
      data['name'],
      data['show_market_names'],
      data['position'] ? data['position'] : 999999999
    );
  }

  public static fromJsonApi1(data: any): MarketGroup {
    return new MarketGroup(
      data['id'], 
      data['expanded'],
      data['name'],
      data['showMarketNames'],
      999999999
    ); 
  }
}