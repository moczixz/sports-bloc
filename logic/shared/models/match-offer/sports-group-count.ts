export class SportsGroupCount {

  constructor(public live: number, public prematch: number) {
  }

  public static fromJsonApi2(data: any): SportsGroupCount {
    return data['count'] ? 
      new SportsGroupCount(data['count']['live'], data['count']['prematch'])
      :
      new SportsGroupCount(0, 0)
  }
}