import { Rate } from "./rate";

export class Selection {

  constructor(
    public id: string, 
    public name: string, 
    public order: number, 
    public rate: Rate) {
  }

  public static fromJsonApi2(data: any): Selection {
    return new Selection(
      data['selection_id'],
      data['name'],
      data['order'],
      Rate.fromJsonApi2(data['rate'])
    )
  }

  public static fromJsonApi1(data: any): Selection {
    return new Selection(
      data['id'],
      data['name'],
      data['order'],
      Rate.fromJsonApi2(data['rate'])
    )
  }

  public static fromJsonApi2Array(data: any[]): Selection[] {
    return data.map(Selection.fromJsonApi2);
  }

  public static fromJsonApi1Array(data: any[]): Selection[] {
    return data.map(Selection.fromJsonApi1);
  }
}