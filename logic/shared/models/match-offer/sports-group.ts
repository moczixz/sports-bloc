import { Helper } from "@logic/shared/helpers/helper";
import { SportsGroupCount } from "./sports-group-count";

export class SportsGroup {
  constructor(
    // common
    public id: number,
    public name: string,
    public label: string,
    public count: SportsGroupCount,
    public parentId: number,

    // api2
    public order: number,
  ) { }

  public static fromJsonApi2(data: any): SportsGroup {
    return new SportsGroup(
      data['sports_group_id'],
      data['name'],
      Helper.stringToUrl(data['names']['en']),
      SportsGroupCount.fromJsonApi2(data),
      data['parent_id'],
      data['order'] ? data['order'] : 99999999,
    );
  }

  public static fromJsonApi1(data: any): SportsGroup {
    return new SportsGroup(
      data['id'],
      data['name'],
      data['label'],
      SportsGroupCount.fromJsonApi2(data),
      data['parentId'],
      99999999,
    );
  }

  public static fromJsonApi2Array(data: any[]): SportsGroup[] {
    return data ? data.map(SportsGroup.fromJsonApi2) : [];
  }

  public static fromJsonApi1Array(data: any[]): SportsGroup[] {
    return data ? data.map(SportsGroup.fromJsonApi1) : [];
  }

  public static getDiscipline(sportsGroups: SportsGroup[]): SportsGroup {
    return sportsGroups.slice()[0];
  }

  public static getRegion(sportsGroups: SportsGroup[]): SportsGroup {
    return sportsGroups.slice()[1];
  }

  public static getLeague(sportsGroups: SportsGroup[]): SportsGroup {
    return sportsGroups.slice()[2];
  }

}