export class MatchState {
  constructor(
    public isLive: boolean,
    public isPre: boolean,
    public isVisible: boolean,
    public isSuspended: boolean,
  ) { }

  public static fromJsonApi2(data: any): MatchState {
    return new MatchState(
      data['state']['is_live'],
      data['state']['is_pre'],
      data['state']['status'] !== 'closed', // isVisible
      data['state']['status'] === 'suspended', // isSuspended
    );
  }

  public static fromJsonApi1(data: any): MatchState {
    return new MatchState(
      data['isLive'],
      data['isPre'],
      data['isVisible'],
      data['isSuspended']
    )
  }
}