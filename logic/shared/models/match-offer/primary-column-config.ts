export class PrimaryColumnConfig {
  constructor(
    public id: number,
    public columns: string[],
    public sportsGroupId: number,
    public name: string,
    public hasLine: boolean,
    public position: number
  ) { }

  public static fromJsonApi2(data: any): PrimaryColumnConfig {
    return new PrimaryColumnConfig(
      data['primary_column_id'],
      Object.keys(data['columns']).map((key: string) => data['columns'][key]),
      data['sports_group_id'],
      data['name'],
      data['has_line'],
      data['position'] ? data['position'] : 999999
    );
  }

  public static fromJsonApi1(data: any): PrimaryColumnConfig {
    return new PrimaryColumnConfig(
      data['id'],
      data['columns'],
      null,
      data['name'],
      data['hasLine'],
      999999999
    )
  }

  public static fromJsonApi2Array(data: any[]): PrimaryColumnConfig[] {
    return data.map(PrimaryColumnConfig.fromJsonApi2);
  }

  public static fromJsonApi1Array(data: any[]): PrimaryColumnConfig[] {
    return data.map(PrimaryColumnConfig.fromJsonApi1);
  }
}
