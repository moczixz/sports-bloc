import { SportsGroup } from "./sports-group";
import { MatchState } from "./match-state";

export class Match {
  constructor(
    // common
    public id: string | number,
    public date: string,
    public homePlayer: string,
    public awayPlayer: string,
    public discipline: SportsGroup,
    public region: SportsGroup,
    public league: SportsGroup,
    public sportsGroups: SportsGroup[],
    public marketsCount: number,
    public state: MatchState, 
    public isTopGroup: boolean,
    public rank?: number,
    public livescoreproId?: number,
    public scoreframeId?: number,
    public isVizuOrScoreExist?: boolean,

    // api1
    public rawData?: any,
  ) { }

  public static fromJsonApi2(data: any): Match {
    const sportsGroups: SportsGroup[] = SportsGroup.fromJsonApi2Array(data['sports_groups']);
    return new Match(
      data['match_id'],
      data['date'],
      (data['home'] || []).join(','),
      (data['away'] || []).join(','),
      SportsGroup.getDiscipline(sportsGroups),
      SportsGroup.getRegion(sportsGroups),
      SportsGroup.getLeague(sportsGroups),
      sportsGroups,
      data['markets_count'],
      MatchState.fromJsonApi2(data),
      data['group'] && data['group']['id'] === -1,
      data['rank'],
      data['integrations']['livescorepro_id'],
      data['integrations']['scoreframe_id'],
      Boolean(data['integrations']['livescorepro_id']) || Boolean(data['integrations']['scoreframe_id'])
    );
  }

  public static fromJsonApi1(data: any): Match {
    const sportsGroups: SportsGroup[] = SportsGroup.fromJsonApi1Array(data['sportsGroups']);
    return new Match(
      data['id'],
      data['date'],
      data['participants']['home'],
      data['participants']['away'],
      SportsGroup.getDiscipline(sportsGroups),
      SportsGroup.getRegion(sportsGroups),
      SportsGroup.getLeague(sportsGroups),
      sportsGroups,
      data['marketsCount'],
      MatchState.fromJsonApi1(data),
      data['group'] && data['group']['id'] === -1,
      data['rank'],
      data['livescoreproId'],
      data['scoreframeId'],
      Boolean(data['livescoreproId']) || Boolean(data['scoreframeId']),
      data
    )
  }

  public static fromJsonApi2Array(data: any[]): Match[] {
    return data.map(Match.fromJsonApi2);
  }

  public static fromJsonApi1Array(data: any[]): Match[] {
    return data.map(Match.fromJsonApi1);
  }


}
