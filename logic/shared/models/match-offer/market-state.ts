export class MarketState {
  constructor(
    public isVisible: boolean,
    public isSuspended: boolean,
  ) { }

  public static fromJsonApi2(data: any): MarketState {
    const status: string = data['state']['status'];
    return new MarketState(
      status === 'open' || status === 'suspended', // isVisible
      status === 'suspended' || status === 'closed', // isSuspended
    );
  }

  public static fromJsonApi1(data: any): MarketState {
    return new MarketState(
      data['isVisible'],
      data['isSuspended']
    )
  }
}