import { StatsConfig } from "@logic/shared/miscs/stats-config";

export interface PeriodDependPackage {
  extraInfo: string;
  periodOrderNumeral: number;
}

export class StatsOld {

  public eventId?: number;
  public eventTimeUtc?: string;
  public eventType?: string;
  public eventTypeId?: number;
  public id?: number;
  public info?: string;
  public period?: number;
  public score?: string;
  public periodScore?: string;
  public side?: number;
  public server?: number;
  public sportKind?: number;
  public extraTimeScore?: string;
  public setCount?: number;
  public currentMinute?: number;
  public remainingTime?: number;

  public setScore?: string;

  public set1Score?: string;
  public set2Score?: string;
  public set3Score?: string;
  public set4Score?: string;
  public set5Score?: string;

  public periodOrderNumeral?: number;
  public extraInfo?: string;

  // tennis
  public gameScore?: string;

  //football
  public yellowcardScore?: string;
  public redcardScore?: string;
  public cornerScore?: string;

  public static createPeriodDependData(disciplineLabel: string, eventType: string, period: number, score: string): PeriodDependPackage {
    let periodOrderNumeral: number = null;
    let extraInfo: string = '';
    let isFinished: boolean = false;

    const sportConfig: any = (StatsConfig[disciplineLabel]) ? StatsConfig[disciplineLabel] : {};
    
    if (eventType !== 'period') {
      // event_type != period
      if (period > sportConfig.maxNormalPeriod) {
        // extra time or finish
        if (period !== sportConfig.finishPeriod) {
          // extra time
          extraInfo = 'extraTime';
          if (sportConfig.overTimePeriods) {
            // extra time with periods
            periodOrderNumeral = period - sportConfig.overTimePeriods;
          }
          // TODO add extra time info to stats table
          // this.stats.updateData(data.stats, this.SPORTS_CONFIG[this.sportKind].overTimeLabelShort);
        }
        else {
          // finish
          extraInfo = 'finished';
          isFinished = true;
        }
      }
      else {
        // normal time
        periodOrderNumeral = period;
      }
    }
    else {
      // event_type == period
      if (period > sportConfig.maxNormalPeriod * 2 - 1) {
        // extra time, penalties or finish
        if (period !== sportConfig.periodFinishPeriod) {
          if (period === sportConfig.periodOverTimeSecondEndPeriod && StatsOld.isDraw(score)) {
            // penalties
            extraInfo = 'penalties';
            // TODO add penalties to stats table
            // this.stats.updateData(data.stats, '', true);
          }
          else {
            // extra time
            if (sportConfig.overTimePeriods) {
              // extra time with periods
              periodOrderNumeral = Math.floor((period - sportConfig.overTimePeriods) / 2);
              extraInfo = 'extraTime';
            }
            else {
              // just extra time
              extraInfo = 'extraTime';
            }
            // TODO add extra time info to stats table
            // this.stats.updateData(data.stats, this.SPORTS_CONFIG[this.sportKind].overTimeLabelShort);
          }
        }
        else {
          // finish
          extraInfo = 'finished';
          isFinished = true;
        }
      }
      else {
        // normal time
        periodOrderNumeral = Math.ceil((period + 1) / 2);
      }
      if (!isFinished && period > 0 && period % 2 !== 0) {
        // end of half/prd/set
        extraInfo = 'endOfPeriod';
      }
    }
    return { extraInfo, periodOrderNumeral };
  }

  public getPeriodDependedData(sportType: string): void {
    const periodPackage: PeriodDependPackage = StatsOld.createPeriodDependData(sportType, this.eventType, this.period, this.score);
    this.extraInfo = periodPackage.extraInfo;
    this.periodOrderNumeral = periodPackage.periodOrderNumeral;
  }

  public static isDraw(score: string): boolean {
    const scoreParts: string[] = score.split(':');
    if (scoreParts[0] === scoreParts[1]) {
      return true;
    }
    return false;
  }
}
