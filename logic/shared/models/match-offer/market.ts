import { MarketGroup } from './market-group';
import { MarketState } from './market-state';
import { Selection } from './selection';
// tslint:disable-next-line:max-classes-per-file
export class Market {
  constructor(
    public id: string | number,
    public matchId: string,
    public marketGroup: MarketGroup,
    public name: string,
    public label: string,
    public primaryColumn: number,
    public selections: Selection[],
    public view: string,
    public state: MarketState,
    public line: number,

    // api2
    public position: number,

    // api1
    public order: number,
    public rawData: any
    
    /* BACKWARD COMPAT */
  ) { }

  public static fromJsonApi2(data: any): Market {
    return new Market(
      data['market_id'],
      data['match_id'],
      MarketGroup.fromJsonApi2(data['markets_group']),
      data['name'],
      data['name'],
      data['primary_column'],
      Selection.fromJsonApi2Array(data['selections']), // selections
      data['view'],
      MarketState.fromJsonApi2(data),
      data['line'],
      data['position'] ? data['position'] : 99999999999,
      99999999999,
      null
    );
  }

  public static fromJsonApi1(data: any): Market {
    return new Market(
      data['id'],
      data['matchId'],
      MarketGroup.fromJsonApi1(data['marketGroup']),
      data['name'],
      data['label'],
      data['primaryColumn'],
      Selection.fromJsonApi1Array(data['selections']), // selections
      data['view'],
      MarketState.fromJsonApi1(data),
      data['line'],
      99999999999,
      data['order'] ? data['order'] : 99999999999,
      data
    );
  }

  public static fromJsonApi2Array(data: any[]): Market[] {
    return data.map(Market.fromJsonApi2);
  }

  public static fromJsonApi1Array(data: any[]): Market[] {
    return data.map(Market.fromJsonApi1);
  }
}
