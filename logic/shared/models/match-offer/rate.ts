export class Rate {
  constructor(public decimal: number, public fractional?: string) {}

  public static fromJsonApi2(data: any): Rate {
    return new Rate(
      data['decimal'],
      data['fractional']
    )
  }

  public static fromJsonApi2Array(data: any[]): Rate[] {
    return data.map(Rate.fromJsonApi2);
  }

}