export interface EnvironmentContract {
  api2Enabled: boolean,
  production: boolean;
  sportsBookApi2: string;
  sportsBookWs2: string;
  sportsBookApi1: string;
  sportsBookWs1: string;
}