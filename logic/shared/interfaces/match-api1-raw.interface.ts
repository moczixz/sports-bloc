interface MatchApi1Raw {
  date: string;
  group: Group;
  id: number;
  isLive: boolean;
  isPre: boolean;
  isSuspended: boolean;
  isVisible: boolean;
  livescoreproId: number;
  marketsCount: number;
  participants: Participants;
  primaryMarkets: PrimaryMarket[];
  rank: number;
  scoreframeId: number;
  sportsGroups: SportsGroup[];
  stats: Stats;
}

interface Stats {
  additionalMinutes: number;
  cornerScore: string;
  currentMinute: number;
  dangerousAttackScore: string;
  eventId: number;
  eventTimeUtc: string;
  eventType: string;
  eventTypeId: number;
  freeKickScore: string;
  id: number;
  info: string;
  matchLength: number;
  penaltyScore: string;
  period: number;
  periodCount: number;
  redcardScore: string;
  score: string;
  server: number;
  set1CornerScore: string;
  set1RedCardScore: string;
  set1Score: string;
  set1YellowCardScore: string;
  set2CornerScore: string;
  set2RedCardScore: string;
  set2Score: string;
  set2YellowCardScore: string;
  set3Score: string;
  set4Score: string;
  set5Score: string;
  shotOffTargetScore: string;
  shotOnTargetScore: string;
  side: number;
  sportKind: number;
  yellowcardScore: string;
}

interface SportsGroup {
  count: Count;
  id: number;
  label: string;
  name: string;
  parentId: number;
  primaryMarketTypes?: PrimaryMarketType[];
}

interface PrimaryMarketType {
  columns: string[];
  hasLine: boolean;
  id: number;
  label: string;
  name: string;
}

interface Count {
  live: number;
  prematch: number;
}

interface PrimaryMarket {
  class?: any;
  id: number;
  isSuspended: boolean;
  isVisible: boolean;
  label: string;
  line: number;
  marketGroup: MarketGroup;
  marketTypeId: number;
  matchId: number;
  name: string;
  order: number;
  primaryColumn: number;
  selections: Selection[];
  view: string;
}

interface Selection {
  id: number;
  isSuspended: boolean;
  name: string;
  order: number;
  rate: Rate;
}

interface Rate {
  decimal: number;
  fractional: string;
}

interface MarketGroup {
  expanded: boolean;
  id: number;
  name: string;
  showMarketNames: boolean;
}

interface Participants {
  away: string;
  home: string;
}

interface Group {
  id: number;
  label: string;
  name: string;
}interface RootObject {
  date: string;
  group: Group;
  id: number;
  isLive: boolean;
  isPre: boolean;
  isSuspended: boolean;
  isVisible: boolean;
  livescoreproId: number;
  marketsCount: number;
  participants: Participants;
  primaryMarkets: PrimaryMarket[];
  rank: number;
  scoreframeId: number;
  sportsGroups: SportsGroup[];
  stats: Stats;
}

interface Stats {
  additionalMinutes: number;
  cornerScore: string;
  currentMinute: number;
  dangerousAttackScore: string;
  eventId: number;
  eventTimeUtc: string;
  eventType: string;
  eventTypeId: number;
  freeKickScore: string;
  id: number;
  info: string;
  matchLength: number;
  penaltyScore: string;
  period: number;
  periodCount: number;
  redcardScore: string;
  score: string;
  server: number;
  set1CornerScore: string;
  set1RedCardScore: string;
  set1Score: string;
  set1YellowCardScore: string;
  set2CornerScore: string;
  set2RedCardScore: string;
  set2Score: string;
  set2YellowCardScore: string;
  set3Score: string;
  set4Score: string;
  set5Score: string;
  shotOffTargetScore: string;
  shotOnTargetScore: string;
  side: number;
  sportKind: number;
  yellowcardScore: string;
}

interface SportsGroup {
  count: Count;
  id: number;
  label: string;
  name: string;
  parentId: number;
  primaryMarketTypes?: PrimaryMarketType[];
}

interface PrimaryMarketType {
  columns: string[];
  hasLine: boolean;
  id: number;
  label: string;
  name: string;
}

interface Count {
  live: number;
  prematch: number;
}

interface PrimaryMarket {
  class?: any;
  id: number;
  isSuspended: boolean;
  isVisible: boolean;
  label: string;
  line: number;
  marketGroup: MarketGroup;
  marketTypeId: number;
  matchId: number;
  name: string;
  order: number;
  primaryColumn: number;
  selections: Selection[];
  view: string;
}

interface Selection {
  id: number;
  isSuspended: boolean;
  name: string;
  order: number;
  rate: Rate;
}

interface Rate {
  decimal: number;
  fractional: string;
}

interface MarketGroup {
  expanded: boolean;
  id: number;
  name: string;
  showMarketNames: boolean;
}

interface Participants {
  away: string;
  home: string;
}

interface Group {
  id: number;
  label: string;
  name: string;
}