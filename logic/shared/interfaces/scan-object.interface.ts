export interface ScanObject<T> {
  [key: string]: T;
}