export interface ObjectAsId<T> {
  [objectId: string]: T;
}