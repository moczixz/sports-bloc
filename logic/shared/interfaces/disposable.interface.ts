import { Subject } from "rxjs";

export interface Disposable {

  //private onDispose$: Subject<void>;
  dispose: () => void;
}