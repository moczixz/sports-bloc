import { ObjectAsId } from "../interfaces/object-as-id.interface";

export function intersectSimpleSimetric<T>(arrayA: T[], arrayB: T[]): T[] {
  // simetrict intersect, return the diffrence from both sides simple values like array of ids
  return arrayA
    .filter((x: T) => !arrayB.includes(x))
    .concat(arrayB.filter((x: T) => !arrayA.includes(x)));
}

export function pluck<T, V>(array: T[], key: string, undefinedReturnEmpty: boolean = true): V[] {
  if (Array.isArray(array)) {
    return array.map<V>((item: T) => item[key]);
  }
  if (undefinedReturnEmpty) {
    return [] as V[];
  }
}

export function extract<T, V>(array: T[], key: string, undefinedReturnEmpty: boolean = true): V[] {
  return pluck(array, key, undefinedReturnEmpty);
}

export function pluckDefined<T, V>(array: T[], key: string, undefinedReturnEmpty: boolean = true): V[] {
  return pluck<T, V>(array, key, undefinedReturnEmpty)
    .filter((item: V) => Boolean(item));
}

export function propertyExist<T>(key: string): (currentItem: T, index: number, array: T[]) => boolean {
  // tslint:disable-next-line:typedef
  return (currentItem: T, index: number, array: T[]) => Boolean(currentItem) && Boolean(currentItem[key]);
}

export function filterDuplicate<T>(key: string): (currentItem: T, index: number, array: T[]) => boolean {
  // tslint:disable-next-line:typedef
  return (currentItem: T, index: number, array: T[]) => {
    return array.findIndex((item: T) =>
      Boolean(item) && Boolean(currentItem) && item[key] === currentItem[key]) === index;
  };
}

export function filterDuplicatePrimitive<T>(currentItem: T, index: number, array: T[]): boolean {
  return array.findIndex((item: T) => item === currentItem) === index;
}

export function chunk<T>(array: T[], chunkSize: number): any {
  return array.reduce((array: T[], item: T, index: number) => { 
    const chunkIndex: number = Math.floor(index / chunkSize);
  
    if(!array[chunkIndex]) {
      array[chunkIndex] = [] as any; // start a new chunk
    }
  
    (array[chunkIndex] as any).push(item);
  
    return array;
  }, []);
}

export function mapToArray<T, V>(map: Map<T, V>): V[] {
  return Array.from(map).map((data: [T, V]) => data[1]);
}

export function takeFirst<T, V>(array: T): V {
  return Array.isArray(array) && array.length > 0 ? array[0] : undefined;
}

export function takeLast<T, V>(array: T): V {
  return Array.isArray(array) && array.length > 0 ? array[array.length - 1] : undefined;
}

export function groupBy<T, V>(array: T[], keyGetter: ((item: T) => V)): T[][] {
  const groupMap: Map<V, T[]> = new Map<V, T[]>();
  array.forEach((item: T) => {
    const key: V = keyGetter(item);
    const group: T[] = findOrCreateGroup(key, groupMap);
    group.push(item);
  });
  return mapToArray(groupMap);
}

export function findOrCreateGroup<T, V>(key: T, groups: Map<T, V>): V {
  if (!groups.has(key)) {
    groups.set(key, [] as any);
  }
  return groups.get(key);
}

export function addOrRemoveExistPrimitive<T>(acc: T[], insert: T): T[] {
  const foundIndex: number = acc.indexOf(insert);
  if (foundIndex === -1) {
    acc.push(insert);
  }
  else {
    acc.splice(foundIndex, 1);
  }
  return acc;
}

export function createArrayByLength(length: number): number[] {
  return Array(length).fill(null).map((x: number, index: number) => index);
}

export function transformToKeyObject<T>(key: string, object: T): ObjectAsId<T> {
  return { [object[key]]: object };
}

export function transformObjectToArray<T, V>(object: T): V[] {
  return Object.keys(object).map((key: string) => object[key]);
}