import { filter, map, scan, publishReplay, refCount, take, catchError, mergeMap } from 'rxjs/operators';
import { MonoTypeOperatorFunction, OperatorFunction, UnaryFunction, Observable, pipe, of, throwError } from 'rxjs';
import { pluck, filterDuplicate, groupBy, addOrRemoveExistPrimitive } from './pure-utils';
import { ScanObject } from '../interfaces/scan-object.interface';
import { AjaxError } from 'rxjs/ajax';

export function filterIsDefined(): MonoTypeOperatorFunction<any> {
  return filter(isDefined);
}

export function isDefined(value: any): boolean {
  return Boolean(value);
}

export function toNumber<T>(): OperatorFunction<T, number> {
  return map((data: T) => Number(data));
}

export function mappedArrayExtractProperty<T, V>(property: string): OperatorFunction<T[], V[]> {
  return map((data: T[]) => pluck(data, property));
}

export function mapFlatArray<T>(): OperatorFunction<T[][], T[]> {
  return map((data: T[][]) => 
    data.reduce((acc: T[], curr: T[]) => acc.concat(curr), [])
  );
}

export function mappedArrayOnlyUniqueByKey<T>(property: string): MonoTypeOperatorFunction<T[]> {
  return map((data: T[]) => data.filter(filterDuplicate(property)));
}

export function mappedArrayGroupBy<T, V>(keyGetter: (item: T) => V): OperatorFunction<T[], T[][]> {
  return map((data: T[]) => groupBy(data, keyGetter));
}

export function mappedArrayFilter<T>(filterFn: (item: T) => boolean): OperatorFunction<T[], T[]> {
  return map((data: T[]) => data.filter(filterFn));
}

export function mappedArraySort<T>(sortFn: (itemA: T, itemB: T) => number): OperatorFunction<T[], T[]> {
  return map((data: T[]) => data.sort(sortFn));
}

export function mappedArrayLimit<T>(arrayLimit: number): OperatorFunction<T[], T[]> {
  return map((data: T[]) => data.slice(0, arrayLimit));
}

export function mappedArrayLimitFrom<T>(arrayLimitFrom: number): OperatorFunction<T[], T[]> {
  return map((data: T[]) => data.slice(arrayLimitFrom));
}

export function mappedArrayLimitFromTo<T>(from: number, to: number): OperatorFunction<T[], T[]> {
  return map((data: T[]) => data.slice(from, to));
}

export function scanUniquePrimitivesArray<T>(): OperatorFunction<T, T[]> {
  return scan((accumulator: T[], current: T) => addOrRemoveExistPrimitive(accumulator, current), []);
}

/*
  example:
  input: [{name: 'some1', data: [1,2,3,4]}, {name: 'some2', data: [3,4,3,2]}]
  call: pipe(
    mappedArrayTransformToObjectByKey('name')
  )
  output: {
    some1: {name: 'some1', data: [1,2,3,4]},
    some2: {name: 'some2', data: [3,4,3,2]}
  }

  note: afer update Typescript version to minimum 3.2 update this function by replace ANY for V in acc
  data.reduce((acc: V, curr: T) => ({...acc, ...{[curr[key]]: curr}}), {} as V)
*/
export function mappedArrayTransformToObjectByKey<T, V>(key: string): OperatorFunction<T[], V> {
  return map((data: T[]) => 
    data.reduce((acc: any, curr: T) => ({...acc, ...{[curr[key]]: curr}}), {} as any)
  );
}

export function scanObjectOverTimeByKey<T>(key: string): OperatorFunction<T, ScanObject<T>> {
  return scan((accumulator: ScanObject<T>, current: T) => {
    return {
      ...accumulator,
      ...{
        [current[key]] : current
      }
    };
  }, {});
}

export function filterEmptyArray<T>(): MonoTypeOperatorFunction<T[]> {
  return filter((data: T[]) => Boolean(data) && data.length > 0);
}

export function simpleSortByAsc<T>(property: string): OperatorFunction<T[], T[]> {
  return map((data: T[]) => 
    data.sort((itemA: T, itemB: T) => itemA[property] - itemB[property])
  );
}

export function cacheTime<T>(timeout: number): UnaryFunction<Observable<T>, Observable<T>> {
  return pipe(
    catchError((err: AjaxError ) => of(err)),
    publishReplay<T>(1, timeout),
    refCount<T>(),
    take<T>(1),
    mergeMap((data: T) => data instanceof AjaxError ? throwError(data) : of(data))
  );
}

export function greaterThan(greaterThan: number): OperatorFunction<number, boolean> {
  return map((compareNumber: number) => compareNumber > greaterThan);
}

export function cloneAndModifyObjectProperty<T, V>(modifyFn: (object: V, key: string) => V): OperatorFunction<T, T> {
  return map((data: T) => 
    Object.keys(data)
      .reduce((acc: any, key: string) => {
        return {
          ...acc,
          [key]: modifyFn(data[key], key)
        };
      }, {} as T)
  );
}

export function isEqual(value: string|number): OperatorFunction<string | number, boolean> {
  return map((inputValue: string|number) => inputValue === value);
}

export function isEqualOr(value: string|number, orValue: string|number): OperatorFunction<string | number, boolean> {
  return map((inputValue: string|number) => inputValue === value || inputValue === orValue);
}

export function filterIsEqual(value: string|number): MonoTypeOperatorFunction<string | number> {
  return filter((inputValue: string|number) => inputValue === value);
}

export function dontCompleteStreamOnError(): UnaryFunction<Observable<any>, Observable<any>> {
  return pipe(
    catchError(() => of(null)),
    filter(Boolean)
  )
}