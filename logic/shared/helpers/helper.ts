import { characterMap } from "../miscs/character-map";

export class Helper {

  public static allAccents: RegExp = new RegExp(Object.keys(characterMap).join('|'), 'g');

  public static stringToUrl(text: string): string {
    text = text.replace(/\(|\)/g, '');
    const result = text.replace(Helper.allAccents, match => {
      return characterMap[match];
    })
      .toLowerCase();
    return result
      .replace(/\-\-+/g, '-')
      .replace(/\:\-+/g, '-');
  }

  public static generateRandomInteger(min: number, max: number): number {
    return Math.floor(min + Math.random() * (max + 1 - min));
  }

  public static randomString(len: number, charSet: string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'): string {
    let randomString: string = '';
    for (let i: number = 0; i < len; i++) {
      const randomPoz: number = Math.floor(Math.random() * charSet.length);
      randomString += charSet.substring(randomPoz, randomPoz + 1);
    }
    return randomString;
  }

  public static shuffleArray<T>(array: T[]): T[] {
    let currentIndex: number = array.length;
    let temporaryValue: T;
    let randomIndex: number;
    while (currentIndex !== 0) {
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
    return array;
  }

  public static createfilledArray(length: number, begin: number = 0): number[] {
    return Array.from(Array(length), (x: number, index: number) => index + begin);
  }

}