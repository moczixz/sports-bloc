import { Provide } from '@logic/di-resolver/di-resolver';
import { MatchesContainerConfig } from '@logic/match-offer/interfaces/matches-container-config.interface';
import { MatchesDataController } from '@logic/match-offer/matches-data-controller';
import { BehaviorSubject, Subject, of, Observable } from 'rxjs';
import { Disposable } from '@logic/shared/interfaces/disposable.interface';
import { takeUntil, tap } from 'rxjs/operators';
import { ContainerToken } from '@logic/match-offer/enums/container-token.enum';
import { MatchesDataSelector } from '@logic/match-offer/matches-data-selectors';

@Provide([MatchesDataController, MatchesDataSelector])
export class TopLiveBloc implements Disposable {

  private defaultConfig: MatchesContainerConfig = {
    isLive: true,
    isTopFive: true,
    limit: 20
  };
  // INPUT
  public config$: BehaviorSubject<MatchesContainerConfig> = new BehaviorSubject<MatchesContainerConfig>(this.defaultConfig);
  public primaryColumnChange$: Subject<{identifier: string, primaryColumn: number}> = new Subject();

  private onDispose$: Subject<void> = new Subject<void>();

  // OUTPUT
  /*
  public token$: Observable<ContainerToken> = of(ContainerToken.topLive);
  public disciplines$: Observable<SportsGroup[]> = this.dataSelectors.getUniqueDisciplinesSortedByOrder(true);
  public activeDiscipline$: Subject<SportsGroup> = this.dataSelectors.activeDiscipline$;
  public matchesInActiveDiscipline$: Observable<Match[]> = this.dataSelectors.getMatchesForActiveDiscipline();
  public primaryMarkets$: Observable<Market[]> = this.dataSelectors.getPrimaryMarkets();
  public selectedPrimaryColumns$: Observable<ObjectAsId<PrimaryColumnConfig>> = this.dataSelectors.getPrimaryColumn();
  public primaryMarketsGroupedByMatch$: Observable<MarketsBelongsToMatch> = this.dataSelectors.getPrimaryMarketsGroupByMatch();
  public defaultPrimaryColumnConfig$: Observable<PrimaryColumnConfig> = this.dataSelectors.getMatchesForActiveDiscipline()
    .pipe(this.dataSelectors.selectDefaultPrimaryColumnConfig()); 
*/
  
  

  constructor(private controller: MatchesDataController, private dataSelectors: MatchesDataSelector) {
    controller.init(ContainerToken.topLive);
    dataSelectors.setToken(ContainerToken.topLive);
    this.init();
  }

  private init(): void {
    this.config$
      .pipe(takeUntil(this.onDispose$))
      .subscribe((config: MatchesContainerConfig) => this.controller.loadForConfig(config))
  }


  public getToken = () => of(ContainerToken.topLive);
  public isDataLoaded = () => this.dataSelectors.isDataLoaded();
  public getDisciplines = () => this.dataSelectors.getUniqueDisciplinesSortedByOrder(true);
  public getActiveDiscipline = () => this.dataSelectors.activeDiscipline$;
  public getMatchesInActiveDiscipline = () => this.dataSelectors.getMatchesForActiveDisciplineSortedByRankDesc();
  public getPrimaryMarkets = () => this.dataSelectors.getPrimaryMarkets();
  public getSelectedPrimaryColumn = () => this.dataSelectors.getPrimaryColumn();
  public getPrimaryMarketsGroupedByMatch = () => this.dataSelectors.getPrimaryMarketsGroupByMatch();
  public getDefaultPrimaryColumnConfig = () => this.dataSelectors.getMatchesForActiveDiscipline()
    .pipe(this.dataSelectors.selectDefaultPrimaryColumnConfig());


  public dispose(): void {
    this.controller.dispose();
    this.onDispose$.next(null);
    this.onDispose$.complete();
  }

}
