import { Provide } from "@logic/di-resolver/di-resolver";
import { ContainerToken } from "@logic/match-offer/enums/container-token.enum";
import { Subject, Observable, BehaviorSubject } from "rxjs";
import { takeUntil, filter, } from "rxjs/operators";
import { Disposable } from "@logic/shared/interfaces/disposable.interface";
import { MatchesDataSelector } from "@logic/match-offer/matches-data-selectors";
import { Match } from "@logic/shared/models/match-offer/match";

@Provide([MatchesDataSelector])
export class MatchRowBloc implements Disposable {

  // INPUT
  public token$: BehaviorSubject<ContainerToken> = new BehaviorSubject<ContainerToken>(null);


  private destroy$: Subject<void> = new Subject<void>();

  constructor(private dataSelectors: MatchesDataSelector) {
    this.init();
  }

  private init(): void {
    this.token$
      .pipe(
        filter(Boolean),
        takeUntil(this.destroy$)
      )
      .subscribe((token: ContainerToken) => this.dataSelectors.setToken(token));
  }

  public getMatchSocket = (match: Match) => this.dataSelectors.getMatchSocket(match);
  public getMatchStatsSocket = (match: Match) => this.dataSelectors.getMatchStatsSocket(match);
  public getMatchLink = (match: Match) => this.dataSelectors.getMatchLink(match);

  public dispose(): void {
    this.destroy$.next(null);
    this.destroy$.complete();
  }
}