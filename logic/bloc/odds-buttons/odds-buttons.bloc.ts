import { Provide } from "@logic/di-resolver/di-resolver";
import { Disposable } from "@logic/shared/interfaces/disposable.interface";
import { Subject, of, Observable, BehaviorSubject } from "rxjs";
import { ContainerToken } from "@logic/match-offer/enums/container-token.enum";
import { filter, takeUntil, tap } from "rxjs/operators";
import { Match } from "@logic/shared/models/match-offer/match";
import { Market } from "@logic/shared/models/match-offer/market";
import { OddsButtonsSettings } from "@logic/match-offer/interfaces/odds-buttons-settings.interface";
import { PrimaryColumnConfig } from "@logic/shared/models/match-offer/primary-column-config";
import { OddsButtonsWorker } from "./odds-buttons-worker";
import { ToggleSelection } from "./interface/toggle-selection.interface";

@Provide([OddsButtonsWorker])
export class OddsButtonsBloc implements Disposable {
  // INPUT
  public token$: BehaviorSubject<ContainerToken> = this.worker.token$;
  public matchSocket$: BehaviorSubject<Match> = this.worker.matchSocket$;
  public primaryMarkets$: BehaviorSubject<Market[]> = this.worker.primaryMarkets$;
  public market$: BehaviorSubject<Market> = this.worker.market$;
  public settings$: BehaviorSubject<OddsButtonsSettings> = this.worker.settings$;
  public primaryColumnConfig$: BehaviorSubject<PrimaryColumnConfig> = this.worker.primaryColumnConfig$;
  public toggleSelection$: Subject<ToggleSelection> = this.worker.toggleSelection$;

  public destroy$: Subject<void> = new Subject<void>();

  constructor(private worker: OddsButtonsWorker) {
  }

  public getMatchLink = (match: Match) => of('');  
  public getMarket = () => this.worker.getMarket();
  public getRateChanges = () => this.worker.getSelectionRateChanges();
  public getIsSuspended = () => this.worker.getIsSuspended();
  public getActiveSelections = () => this.worker.getActiveSelections();
  public getNotEmptyColumns = () => this.worker.getPrimaryColumnsWithoutEmpty();



  public dispose(): void {
    this.destroy$.next(null);
    this.destroy$.complete();
  }
}