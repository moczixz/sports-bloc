import { OddsButtonsBloc } from "./odds-buttons.bloc";
import { BehaviorSubject, Observable, combineLatest, zip, iif, of, merge, from, Subject } from "rxjs";
import { Market } from "@logic/shared/models/match-offer/market";
import { filter, map, startWith, switchMap, pairwise, take, tap, mergeMap, mergeScan, takeUntil } from "rxjs/operators";
import { PrimaryColumnConfig } from "@logic/shared/models/match-offer/primary-column-config";
import { Rate } from "@logic/shared/models/match-offer/rate";
import { Match } from "@logic/shared/models/match-offer/match";
import { Selection } from '@logic/shared/models/match-offer/selection';
import { MatchStats } from "@logic/shared/models/match-offer/match-stats";
import { OddsButtonsSettings } from "@logic/match-offer/interfaces/odds-buttons-settings.interface";
import { OddsButtonsType } from "@logic/match-offer/enums/odds-buttons-type.enum";
import { ToggleSelection } from "./interface/toggle-selection.interface";
import { RateKeeper } from "./interface/rate-keeper.interface";
import { Provide } from "@logic/di-resolver/di-resolver";
import { MatchesDataSelector } from "@logic/match-offer/matches-data-selectors";
import { BetslipSelectionsService } from "@logic/betslip/service/betslip-selections.service";
import { ContainerToken } from "@logic/match-offer/enums/container-token.enum";
import { Disposable } from "@logic/shared/interfaces/disposable.interface";

@Provide([MatchesDataSelector, BetslipSelectionsService])
export class OddsButtonsWorker implements Disposable {

  public token$: BehaviorSubject<ContainerToken> = new BehaviorSubject<ContainerToken>(null);
  public matchSocket$: BehaviorSubject<Match> = new BehaviorSubject<Match>(null);
  public primaryMarkets$: BehaviorSubject<Market[]> = new BehaviorSubject<Market[]>(null);
  public market$: BehaviorSubject<Market> = new BehaviorSubject<Market>(null);
  public settings$: BehaviorSubject<OddsButtonsSettings> = new BehaviorSubject<OddsButtonsSettings>(null);
  public primaryColumnConfig$: BehaviorSubject<PrimaryColumnConfig> = new BehaviorSubject<PrimaryColumnConfig>(null);
  public toggleSelection$: Subject<ToggleSelection> = new Subject<ToggleSelection>();

  private selectedMarket$: BehaviorSubject<Market> = new BehaviorSubject<Market>(null);
  private rateChanges$: BehaviorSubject<RateKeeper> = new BehaviorSubject<RateKeeper>(null);

  private destroy$: Subject<void> = new Subject<void>();

  constructor(private dataSelectors: MatchesDataSelector, private betslipSelectionsService: BetslipSelectionsService) {
    this.init();
  }

  private init(): void {
    this.token$
      .pipe(
        filter(Boolean),
        takeUntil(this.destroy$),
      )
      .subscribe((token: ContainerToken) => this.dataSelectors.setToken(token));

    this.toggleSelection$
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: ToggleSelection) => this.toggleSelection(data))

    this.watchOverSelection();
  }


  public getSelectedMarket(): Observable<Market> {
    return this.selectedMarket$.asObservable();
  }

  private getPrimaryMarketsSortedByPosition(): Observable<Market[]> {
    return this.primaryMarkets$
      .pipe(
        filter(Boolean),
        map((markets: Market[]) =>
          markets.sort((marketA: Market, marketB: Market) => marketA.position - marketB.position)
        )
      )
  }

  public getIsSuspended(): Observable<boolean> {
    return combineLatest(
      this.matchSocket$.pipe(filter(Boolean)), 
      this.selectedMarket$.pipe(filter(Boolean))
        .pipe(
          switchMap((market: Market) => this.dataSelectors.getMarketSocket(market))
        )
    )
      .pipe(
        map(([match, market]: [Match, Market]) => 
          match.state.isSuspended || market.state.isSuspended
        )
      );
  }

  public getActiveSelections(): Observable<string[]> {
    return combineLatest(
      this.selectedMarket$,
      this.betslipSelectionsService.getActiveSelections()
        .pipe(startWith([]), pairwise())
    )
    .pipe(
      filter((data: [Market, [string[], string[]]]) => Boolean(data[0]) && Boolean(data[1][0]) && Boolean(data[1][1])),
      filter((data: [Market, [string[], string[]]]) => {
        const market: Market = data[0];
        const previousSelectionIds: string[] = data[1][0];
        const currentSelectionIds: string[] = data[1][1];
        return market.selections.some((selection: Selection) => previousSelectionIds.includes(selection.id))
          || market.selections.some((selection: Selection) => currentSelectionIds.includes(selection.id));
      }),
      map((data: [Market, [string[], string[]]]) => data[1][1])
    );
  }
  
  public toggleSelection(data: ToggleSelection): void {
    zip(
      this.selectedMarket$.pipe(take(1)),
      this.matchSocket$.pipe(filter(Boolean), take(1)),
      this.selectedMarket$
        .pipe(
          switchMap((market: Market) => this.dataSelectors.getMarketSocket(market)),
          take(1)
        ),
      iif(
        () => data.match.state.isLive,
        this.dataSelectors.getMatchStatsSocket(data.match),
        of(null)
      ).pipe(take(1))
    )
    .subscribe(([market, matchSocket, marketSocket, matchStats]: [Market, Match, Market, MatchStats]) => 
      this.betslipSelectionsService.toggleSelectionFromApi2({
        selection: data.selection, 
        rate: data.rate, 
        market, 
        match: data.match, 
        matchSocket, 
        marketSocket, 
        matchStats
      })
    );
  }

  public getMarket(): Observable<Market> {
    return this.settings$
      .pipe(
        filter(Boolean),
        switchMap((settings: OddsButtonsSettings) =>
          this.chooseMarketSource(settings)
        )
      )
  }

  private chooseMarketSource(settings:OddsButtonsSettings): Observable<Market> {
    if (settings.type === OddsButtonsType.market) {
      return this.getSelectedMarket();
    }
    if (settings.selectFirstMarket) {
      return this.getFirstMarket();
    }
    // default market source for standard odds-buttons
    return this.getMarketForPrimaryColumnConfig();
  }

  public getMarketForPrimaryColumnConfig(): Observable<Market> {
    return combineLatest(
      this.getPrimaryMarketsSortedByPosition(),
      this.primaryColumnConfig$.pipe(filter(Boolean))
    )
    .pipe(
      map(([primaryMarkets, config]: [Market[], PrimaryColumnConfig]) => 
        primaryMarkets.filter((market: Market) => market.primaryColumn === config.id)
      ),
      filter((primaryMarkets: Market[]) => primaryMarkets.length > 0),
      map((primaryMarkets: Market[]) => primaryMarkets[0]),
      tap((market: Market) => this.selectedMarket$.next(market))
    );
  }

  public getFirstMarket(): Observable<Market> {
    return this.getPrimaryMarketsSortedByPosition()
      .pipe(
        filter((primaryMarkets: Market[]) => primaryMarkets.length > 0),
        map((primaryMarkets: Market[]) => primaryMarkets[0]),
        tap((market: Market) => this.selectedMarket$.next(market))
      )
  }

  private checkIfRateChanged(acc: RateKeeper, selection: Selection): Observable<RateKeeper> {
    if (acc[selection.id]) {
      const currentRate: Rate = acc[selection.id].current;
      if (currentRate.decimal !== selection.rate.decimal) {
        return of({...acc, [selection.id]: { previous: currentRate, current: selection.rate }});
      }
      else {
        return of(null);
      }
    }
    else {
      return of({[selection.id]: {previous: selection.rate, current: selection.rate }});
    }
  }

  private watchOverSelection(): void {
    this.selectedMarket$
      .pipe(
        filter(Boolean),
        switchMap((market: Market) => merge(of(market), this.dataSelectors.getMarketSocket(market))),
        mergeMap((market: Market) => from(market.selections)),
        mergeScan((acc: RateKeeper, selection: Selection) => 
          this.checkIfRateChanged(acc, selection)
            .pipe(
              filter(Boolean),
              map((item: RateKeeper) => {
                return {...acc, ...item};
              })
            )
        , {}, 1),
        takeUntil(this.destroy$)
      )
      .subscribe((rates: RateKeeper) => this.rateChanges$.next(rates));
  }

  public getSelectionRateChanges(): Observable<RateKeeper> {
    return this.rateChanges$.pipe(filter(Boolean));
  }

  public getPrimaryColumnsWithoutEmpty(): Observable<string[]> {
    return this.primaryColumnConfig$
      .pipe(
        filter(Boolean),
        map((config: PrimaryColumnConfig) => 
          config.columns.filter((column: string) => column.length > 0)
        ),
        startWith([])
      )
  }

  public dispose(): void {
    this.destroy$.next(null);
    this.destroy$.complete();
  }

}
