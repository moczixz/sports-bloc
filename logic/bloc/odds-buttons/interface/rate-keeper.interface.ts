import { Rate } from "@logic/shared/models/match-offer/rate";

export interface RateKeeper {
  [selectionId: string ]: {
    previous: Rate,
    current: Rate
  };
} 