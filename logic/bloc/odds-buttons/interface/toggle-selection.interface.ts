import { Match } from "@logic/shared/models/match-offer/match";
import { Rate } from "@logic/shared/models/match-offer/rate";

export interface ToggleSelection {
  selection: Selection, 
  rate: Rate, 
  match: Match
}