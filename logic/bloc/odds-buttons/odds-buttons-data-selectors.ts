/*
import { Injectable } from '@angular/core';
import { MatchApi2 } from '@models/match-api2';
import { Observable, of } from 'rxjs';
import { OpenMatchDetails } from '@modules/matches-list/interfaces/open-match-details.interface';
import { SingleMatchViewService } from '@modules/single-match-view/services/single-match-view.service';

@Injectable()
export class OddsButtonsDataSelectors {

  constructor(private singleMatchViewService: SingleMatchViewService) {
  }

  public getMatchLink(match: MatchApi2): Observable<string> {
    const openMatchDetails: OpenMatchDetails = this.singleMatchViewService.getOpenMatchApi2Details(match);
    return of(
      this.singleMatchViewService.getLinkToMatch(
        match.state.isLive,
        openMatchDetails.ids,
        openMatchDetails.names,
        match.participants
      )
    );
  }
  
}
*/