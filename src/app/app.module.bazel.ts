import { NgModule } from "@angular/core";
import { AppModuleDeclarations, providers } from "./app.module";
import { AppComponent } from "./app.component";
import { AppRoutingModuleBazel } from "./app-routing.module.bazel";

@NgModule({
  declarations: [],
  imports: [AppModuleDeclarations, AppRoutingModuleBazel],
  providers: providers,
  bootstrap: [AppComponent]
})
export class AppModuleBazel {}