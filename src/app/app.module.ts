import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CommonPipesModule } from '@ngCommon/pipe/common-pipes.module';
import { TopPreComponent } from './top-pre/top-pre.component';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [
    AppComponent,
    TopPreComponent,
  ],
  imports: [
    BrowserModule,
    CommonPipesModule,
    RouterModule
  ]
})
export class AppModuleDeclarations {}


export const providers = []

@NgModule({
  declarations: [],
  imports: [AppModuleDeclarations, AppRoutingModule],
  providers: providers,
  bootstrap: [AppComponent]
})
export class AppModule {}