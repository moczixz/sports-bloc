import { Component, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { interval } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import { TopLiveBloc } from '@logic/bloc/top-live/top-live.bloc';
import { getTopLiveBloc } from '@logic/bloc-logic';



@Component({
  selector: 'app-top-live',
  templateUrl: './top-live.component.html',
  styleUrls: ['./top-live.component.css']
})
export class TopLiveComponent implements AfterViewInit {
  private topLiveBloc: TopLiveBloc = getTopLiveBloc();

  constructor() { 
    console.log(this.topLiveBloc);
  }

  ngAfterViewInit() {
    //this.changeDetector.detach();
  }


}
