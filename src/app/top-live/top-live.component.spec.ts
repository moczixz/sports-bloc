import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopLiveComponent } from './top-live.component';

describe('TopLiveComponent', () => {
  let component: TopLiveComponent;
  let fixture: ComponentFixture<TopLiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopLiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopLiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
