import {enableProdMode} from '@angular/core';
import {platformBrowser} from '@angular/platform-browser';
// @ts-ignore
import {AppModuleBazelNgFactory} from './app/app.module.bazel.ngfactory';

enableProdMode();
platformBrowser().bootstrapModuleFactory(AppModuleBazelNgFactory);
