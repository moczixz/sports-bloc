import {platformBrowser} from '@angular/platform-browser';
// @ts-ignore
import {AppModuleBazelNgFactory} from './app/app.module.bazel.ngfactory';

platformBrowser().bootstrapModuleFactory(AppModuleBazelNgFactory);
