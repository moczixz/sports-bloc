import { NgModule } from '@angular/core';
import { OddsButtonsComponent } from './odds-buttons.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { CommonDirectivesModule } from '@ngCommon/directive/common-directives.module';

@NgModule({
  declarations: [
    OddsButtonsComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    CommonDirectivesModule
  ],
  exports: [
    OddsButtonsComponent
  ]
})
export class OddsButtonsModule {
}
