import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { OddsButtonsPublicAudience } from '@ngCommon/common-ui-handlers/odds-buttons/odds-buttons-public-audience';
import { OddsButtonsTemplateHandler } from '@ngCommon/common-ui-handlers/odds-buttons/odds-buttons-template-handler';
import { Match } from '@logic/shared/models/match-offer/match';
import { ContainerToken } from '@logic/match-offer/enums/container-token.enum';
import { Market } from '@logic/shared/models/match-offer/market';
import { OddsButtonsSettings } from '@logic/match-offer/interfaces/odds-buttons-settings.interface';
import { PrimaryColumnConfig } from '@logic/shared/models/match-offer/primary-column-config';
import { OddsButtonsBloc } from '@logic/bloc/odds-buttons/odds-buttons.bloc';
import { getOddsButtonsBloc } from '@logic/bloc-logic';
import { Observable, defer, Subject } from 'rxjs';

import { ToggleSelection } from '@logic/bloc/odds-buttons/interface/toggle-selection.interface';
import { RateKeeper } from '@logic/bloc/odds-buttons/interface/rate-keeper.interface';

@Component({
  selector: 'odds-buttons',
  templateUrl: 'odds-buttons.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OddsButtonsComponent {
  private oddsButtonsBloc: OddsButtonsBloc = getOddsButtonsBloc();
  
  public publicAudience: OddsButtonsPublicAudience = new OddsButtonsPublicAudience();
  public templateHandler: OddsButtonsTemplateHandler = new OddsButtonsTemplateHandler();
  @Input() set token(token: ContainerToken) { this.oddsButtonsBloc.token$.next(token); };
  @Input() match: Match;
  @Input() set matchSocket(match: Match) { this.oddsButtonsBloc.matchSocket$.next(match); }
  @Input() set primaryMarkets(markets: Market[]) { this.oddsButtonsBloc.primaryMarkets$.next(markets); }
  @Input() set market(market: Market) { this.oddsButtonsBloc.market$.next(market); }

  private _settings: OddsButtonsSettings;
  @Input() set settings(settings: OddsButtonsSettings) {
    this._settings = settings;
    this.oddsButtonsBloc.settings$.next(settings);
  }
  get settings(): OddsButtonsSettings {
    return this._settings;
  }

  private _primaryColumnConfig: PrimaryColumnConfig;
  @Input() set primaryColumnConfig(config: PrimaryColumnConfig) { 
    this._primaryColumnConfig = config;
    this.templateHandler.setPrimaryColumnConfig(config);
    this.oddsButtonsBloc.primaryColumnConfig$.next(config);
  }
  get primaryColumnConfig(): PrimaryColumnConfig {
    return this._primaryColumnConfig;
  }

  public toggleSelection$: Subject<ToggleSelection> = this.oddsButtonsBloc.toggleSelection$;


  public market$: Observable<Market> = defer(() => this.oddsButtonsBloc.getMarket().pipe(this.publicAudience.tapMarket()));
  public rateChanges$: Observable<RateKeeper> = this.oddsButtonsBloc.getRateChanges();
  public isSuspended$: Observable<boolean> = this.oddsButtonsBloc.getIsSuspended();
  public activeSelections$: Observable<string[]> = this.oddsButtonsBloc.getActiveSelections();
  public matchLink$: Observable<string> = defer(() => this.oddsButtonsBloc.getMatchLink(this.match));

  public notEmptyColumns$: Observable<string[]> = this.oddsButtonsBloc.getNotEmptyColumns()
    .pipe(this.templateHandler.tapAndUpdateColumns());


}
