import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'match-period-type',
  templateUrl: 'match-period-type.component.html'
})
export class MatchPeriodTypeComponent implements OnInit {
  @Input() sportType: string;

  constructor() {
  }

  ngOnInit() {
  }
}
