import { NgModule } from "@angular/core";
import { MatchPeriodTypeComponent } from "./match-period-type.component";
import { CommonModule } from "@angular/common";

@NgModule({
  declarations: [
    MatchPeriodTypeComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    MatchPeriodTypeComponent
  ]
})
export class MatchCommonModule {

}