import { DisciplinesSelectComponent } from './disciplines-select.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { SbControlsModule } from '@ngCommon/sb-controls/sb-controls.module';

@NgModule({
  declarations: [
    DisciplinesSelectComponent
  ],
  imports: [
    CommonModule,
    SbControlsModule,
    FormsModule
  ],
  exports: [
    DisciplinesSelectComponent
  ]
})
export class DisciplinesSelectModule {
  
}
