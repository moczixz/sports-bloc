import { ChangeDetectionStrategy, Component, Input, Output, EventEmitter, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { SportsGroup } from '@logic/shared/models/match-offer/sports-group';
import { DisciplinesSelectUiHandler } from '@ngCommon/common-ui-handlers/disciplines-select/disciplines-select-ui-handler';

@Component({
  selector: 'disciplines-select',
  templateUrl: 'disciplines-select.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DisciplinesSelectUiHandler]
})
export class DisciplinesSelectComponent implements AfterViewInit {
  @ViewChild('topTenRef', {static: false}) topTenRef: ElementRef;
  @Input() set activeDiscipline(discipline: SportsGroup) {
    this.uiHandler.setActiveDisciplineId(discipline);
  }

  @Input() set disciplines(disciplines: SportsGroup[]) {
    this.uiHandler.disciplines = disciplines;
  }

  @Output() activeDisciplineChange: EventEmitter<SportsGroup> = this.uiHandler.activeDisciplineChange;

  constructor(public uiHandler: DisciplinesSelectUiHandler) {
  }

  public ngAfterViewInit(): void {
    this.uiHandler.setTopTenRef(this.topTenRef);
  }

}
