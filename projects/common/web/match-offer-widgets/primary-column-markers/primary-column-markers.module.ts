import { NgModule } from '@angular/core';
import { PrimaryColumnMarkersComponent } from './primary-column-markers.component';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [PrimaryColumnMarkersComponent],
  imports: [CommonModule],
  exports: [PrimaryColumnMarkersComponent]
})
export class PrimaryColumnMarkersModule {

}
