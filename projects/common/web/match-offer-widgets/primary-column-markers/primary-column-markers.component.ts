import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { LoopTracker } from '@ngCommon/miscs/loop-tracker';
import { PrimaryColumnConfig } from '@logic/shared/models/match-offer/primary-column-config';


@Component({
  selector: 'primary-column-markers',
  templateUrl: 'primary-column-markers.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PrimaryColumnMarkersComponent{

  public tracker: LoopTracker = new LoopTracker();
  @Input() primaryColumnConfig: PrimaryColumnConfig; 
}
