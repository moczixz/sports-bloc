import { NgModule } from '@angular/core';
import { MatchRowComponent } from './match-row.component';
import { CommonModule } from '@angular/common';
import { OddsButtonsModule } from '../match-offer-widgets/odds-buttons/odds-buttons.module';
import { PrimaryColumnMarkersModule } from '../match-offer-widgets/primary-column-markers/primary-column-markers.module';
import { CommonDirectivesModule } from '@ngCommon/directive/common-directives.module';
import { SbTooltipModule } from '@ngCommon/sb-tooltip/sb-tooltip.module';
import { MatchCommonModule } from '../match-offer-widgets/match-common/match-common.module';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    MatchRowComponent
  ],
  imports: [
    CommonModule,
    OddsButtonsModule,
    PrimaryColumnMarkersModule,
    CommonDirectivesModule,
    SbTooltipModule,
    MatchCommonModule,
    RouterModule
  ],
  exports: [
    MatchRowComponent
  ]
})
export class MatchRowModule {

}