import { ChangeDetectionStrategy, Input, Component } from "@angular/core";
import { Match } from "@logic/shared/models/match-offer/match";
import { Market } from "@logic/shared/models/match-offer/market";
import { PrimaryColumnConfig } from "@logic/shared/models/match-offer/primary-column-config";
import { Observable, defer } from "rxjs";
import { MatchStats } from "@logic/shared/models/match-offer/match-stats";
import { ContainerToken } from "@logic/match-offer/enums/container-token.enum";
import { MatchRowSettings } from "@logic/match-offer/interfaces/match-row-settings.interface";
import { MatchRowBloc } from "@logic/bloc/match-row/match-row.bloc";
import { getMatchRowBloc } from "@logic/bloc-logic";

@Component({
  selector: 'match-row',
  templateUrl: 'match-row.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MatchRowComponent {
  private matchRowBloc: MatchRowBloc = getMatchRowBloc();
  @Input() set token(token: ContainerToken) { this.matchRowBloc.token$.next(token); };

  @Input() match: Match;

  @Input() primaryMarkets: Market[];
  @Input() col1PrimaryColumnConfig: PrimaryColumnConfig;
  @Input() col2PrimaryColumnConfig: PrimaryColumnConfig;
  @Input() col3PrimaryColumnConfig: PrimaryColumnConfig;

  @Input() settings: MatchRowSettings;

  public token$: Observable<ContainerToken> = this.matchRowBloc.token$;
  public matchSocket$: Observable<Match> = defer(() => this.matchRowBloc.getMatchSocket(this.match));
  public matchStatsSocket$: Observable<MatchStats> = defer(() => this.matchRowBloc.getMatchStatsSocket(this.match));
  public matchLink$: Observable<string> = defer(() => this.matchRowBloc.getMatchLink(this.match));


  public get colPrimaryColumnConfigs(): PrimaryColumnConfig[] {
    return [
      this.col1PrimaryColumnConfig, this.col2PrimaryColumnConfig, this.col3PrimaryColumnConfig
    ].filter(Boolean);
  }

}
